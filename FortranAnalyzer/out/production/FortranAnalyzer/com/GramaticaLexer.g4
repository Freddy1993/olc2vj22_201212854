lexer grammar GramaticaLexer;

channels {
  WHITESPACE_CHANNEL,
  COMMENTS_CHANNEL,
  CONTROL_CYCLE
}

EQUAL    : '=';
TWOPOINT : ':';
COMMA    : ',';
EXCLAMATION : '!';
SINGLE_QUOTE:'\'';
DOUBLE_QUOTE:'"';
LP       : '(';
RP       : ')';
TWOPOINTS: '::';
PROGRAM  : 'program';
IMPLICIT : 'implicit';
NONE: 'none';
CORCHL: '[';
CORCHR: ']';
END      : 'end';
THEN     : 'then';
EXIT     : 'exit' -> channel(CONTROL_CYCLE);
CYCLE    : 'cycle' -> channel(CONTROL_CYCLE);
PRINTLN  : 'print';
INTEGER  : 'integer';
REAL     : 'real';
COMPLEX  : 'complex';
CHARACTER: 'character';
LOGICAL  : 'logical';
SUM: '+';
SUBSTACT: '-';
MULTIPLICATION: '*';
DIVISION: '/';
POWER: '**';
EQUALS: '.eq.';
EQUALS_ALTERNATIVE: '==';
INEQUALITY: '.ne.';
INEQUALITY_ALTERNATIVE: '/=';
GREATER_THAN: '.gt.';
GREATER_THAN_ALTERNATIVE: '>';
LESS_THAN: '.lt.';
LESS_THAN_ALTERNATIVE: '<';
GREATER_EQUAL: '.ge.';
GREATER_EQUAL_ALTERNATIVE: '>=';
LESS_EQUAL: '.le.';
LESS_EQUAL_ALTERNATIVE: '<=';
AND: '.and.';
OR: '.or.';
NOT: '.not.';
DIMENSION: 'dimension';
SIZE: 'size';
ALLOCATABLE: 'allocatable';
ALLOCATE: 'allocate';
DEALLOCATE: 'deallocate';
IF: 'if';
ELSE: 'else';
DO: 'do';
WHILE: 'while';
INTENT: 'intent';
IN: 'in';
OUT: 'out';
INOUT: 'inout';
CALL: 'call';
SUBROUTINE: 'subroutine';
FUNCTION: 'function';
RESULT: 'result';
NUMBER   : [0-9]+;
FLOAT    : [0-9]+[.][0-9]+;
STRING   : '"'~["]*'"';
STRING2   : '\''~[']*'\'';
TRUE     : '.true.';
FALSE    : '.false.';
ID: [a-zA-Z_] [a-zA-Z0-9_]*;
WHITESPACE: [ \r\n\t]+ -> skip;
COMMENT : '!' ~[\n]+  -> channel(COMMENTS_CHANNEL);

fragment
ESC_SEQ
    :   '\\' ('\\'|'@'|'['|']'|'.'|'#'|'+'|'-'|'!'|':'|' ')
    ;