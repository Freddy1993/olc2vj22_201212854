parser grammar Gramatica;

options {
  tokenVocab = GramaticaLexer;
}

@header {
     import java.util.HashMap;
        import Tree.Arbol;
        import Tree.PrintInstruction;
        import Tree.NodeInst;
        import Tree.Operations;
        import Tree.Primitives;
        import Tree.Symbol.Tipo;
        import Tree.Operations.Tipo_Operations;
        import Tree.Assignment;
        import Tree.Declaracion;
        import Tree.Identifier;
        import Tree.For;
        import Tree.DoWhile;
        import Tree.If;
        import java.util.LinkedList;
        import Tree.AccessArray;
        import Tree.AsignArray;
        import Tree.DeclarationArray;
        import Tree.Size;
        import Tree.ParamsFunction;
        import Tree.Function;
        import Tree.CallFunction;
        import Tree.Return;
        import Tree.Allocate;
        import Tree.Deallocate;
}

@members{
    HashMap memory = new HashMap();
    LinkedList<NodeInst> a = new LinkedList<>();
}

initStart returns [Arbol ast]
    : inst = body
        {
            $ast = new Arbol($inst.lista);
        }
;

body returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> concatList = new LinkedList<>(); $lista = concatList; }
    : ins=listFunctions* inst=program
    {
        if ( $ctx.ins!=null ) $lista.addAll($ins.lista);
        $lista.addAll($inst.lista);
    }
;

listFunctions returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
    : sublista=listFunctions instru=instruction
    {
        $sublista.lista.add($instru.inst);
        $lista = $sublista.lista;
    }
    | instru=instruction
    {
        $lista.add($instru.inst);
    }
;

instruction returns[NodeInst inst]
    : funct=function
    {
        $inst = $funct.inst;
    }
;

function returns[NodeInst inst]
    : FUNCTION idIni=ID LP params=list_paramaters RP RESULT LP idResult=ID RP IMPLICIT NONE instr=list_cont_instruction END FUNCTION idFin=ID
        {
            $inst = new Function($idIni.text, $params.lista, $instr.lista, new Return(new Identifier($idResult.text, $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine()), $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine()), $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine());
        }
    | SUBROUTINE idIni=ID LP params=list_paramaters RP IMPLICIT NONE instr=list_cont_instruction END SUBROUTINE idFin=ID
        {
          $inst = new Function($idIni.text, $params.lista, $instr.lista, null, $SUBROUTINE.getLine(), $SUBROUTINE.getCharPositionInLine());
        }
    | FUNCTION idIni=ID LP RP RESULT LP idResult=ID RP IMPLICIT NONE instr=list_cont_instruction END FUNCTION idFin=ID
        {
            $inst = new Function($idIni.text, new LinkedList<>(), $instr.lista, new Return(new Identifier($idResult.text, $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine()), $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine()), $FUNCTION.getLine(), $FUNCTION.getCharPositionInLine());
        }
    | SUBROUTINE idIni=ID LP RP IMPLICIT NONE instr=list_cont_instruction END SUBROUTINE idFin=ID
        {
          $inst = new Function($idIni.text, new LinkedList<>(), $instr.lista, null, $SUBROUTINE.getLine(), $SUBROUTINE.getCharPositionInLine());
        }
;


list_paramaters returns[LinkedList<NodeInst> lista]
        @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
        : sublista=list_variable COMMA var=variable
        {
            $sublista.lista.add($var.inst);
            $lista = $sublista.lista;
        }
        | var=variable
        {
            $lista.add($var.inst);
        }
;

call_function returns[NodeInst inst]
    : ID LP params=dimension_array RP
        {
            $inst = new CallFunction($ID.text, $params.lista, $ID.getLine(), $ID.getCharPositionInLine());
        }
    | CALL ID LP params=dimension_array RP
        {
            $inst = new CallFunction($ID.text, $params.lista, $ID.getLine(), $ID.getCharPositionInLine());
        }
    | ID LP RP
      {
          $inst = new CallFunction($ID.text, new LinkedList<>(), $ID.getLine(), $ID.getCharPositionInLine());
      }
    | CALL ID LP RP
      {
          $inst = new CallFunction($ID.text, new LinkedList<>(), $ID.getLine(), $ID.getCharPositionInLine());
      }
;

program returns[LinkedList<NodeInst> lista]
    : PROGRAM ID IMPLICIT NONE cont=list_cont_instruction END PROGRAM ID
    {
       $lista = $cont.lista;
    }
;

list_cont_instruction returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
    : sublista=list_cont_instruction cont=content_inst
        {
            $sublista.lista.add($cont.inst);
            $lista = $sublista.lista;
        }
    | cont=content_inst
        {
            $lista.add($cont.inst);
        }
;

content_inst returns[NodeInst inst]
    : PRINTLN MULTIPLICATION COMMA lista=list_expression
        {
            $inst = new PrintInstruction($lista.lista, $PRINTLN.getLine(), $PRINTLN.getCharPositionInLine());;
        }
    | var=variables
         {
            $inst = $var.inst;
         }
    | varParams=declareVars
         {
            $inst = $varParams.inst;
         }
    | asig=asignation
        {
            $inst = $asig.inst;
        }
    | senteIf=sentence_if
        {
            $inst = $senteIf.inst;
        }
    | sentedo=sentence_do
        {
            $inst = $sentedo.inst;
        }
    | senteDoWhile=sentence_do_while
        {
            $inst = $senteDoWhile.inst;
        }
    | senteArray=sentence_array
        {
            $inst = $senteArray.inst;
        }
    | senteAsignArray=sentence_asignacion_array
        {
            $inst = $senteAsignArray.inst;
        }
    | senteAsignArrayInitial=sentence_asig_array_inicial
        {
            $inst = $senteAsignArrayInitial.inst;
        }
    | func=call_function
        {
            $inst = $func.inst;
        }
    | allow=sentence_allocate
        {
            $inst = $allow.inst;
        }
    | deallow=sentence_deallocate
        {
            $inst = $deallow.inst;
        }
;


asignation returns[NodeInst inst]
    : ID EQUAL expr=expression
      {
          $inst = new Assignment($ID.text, $expr.inst, $ID.getLine(), $ID.getCharPositionInLine());
      }
;

variables returns[NodeInst inst]
    : type=type_var TWOPOINTS lista=list_variable
        {
            $inst = new Declaracion($lista.lista, $type.tipo, $TWOPOINTS.getLine(), $TWOPOINTS.getCharPositionInLine());;
        }
;

declareVars returns[NodeInst inst]
    : type=type_var COMMA INTENT LP type_in=type_declare_var RP TWOPOINTS lista=list_variable
        {
            $inst = new ParamsFunction($lista.lista, $type.tipo, $type_in.tipo, $INTENT.getLine(), $INTENT.getCharPositionInLine());
        }
;

type_declare_var returns[Tipo tipo]
    : IN
        {
            $tipo = Tipo.IN;
        }
    | OUT
        {
            $tipo = Tipo.OUT;
        }
    | INOUT
        {
            $tipo = Tipo.INOUT;
        }
;

type_var returns[Tipo tipo]
        : INTEGER
            {
                $tipo = Tipo.ENTERO;
            }
        | REAL
            {
                $tipo = Tipo.REAL;
            }
        | COMPLEX
            {
                $tipo = Tipo.COMPLEX;
            }
        | CHARACTER
            {
                $tipo = Tipo.CHARACTER;
            }
        | LOGICAL
            {
                $tipo = Tipo.LOGICAL;
            }
;

list_variable returns[LinkedList<NodeInst> lista]
        @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
        : sublista=list_variable COMMA var=variable
        {
            $sublista.lista.add($var.inst);
            $lista = $sublista.lista;
        }
        | var=variable
        {
            $lista.add($var.inst);
        }
;

variable returns[NodeInst inst]
        : ID
            {
                $inst = new Identifier($ID.text, $ID.getLine(), $ID.getCharPositionInLine());
            }
        | ID EQUAL expr=expression
            {
                $inst = new Assignment($ID.text, $expr.inst, $ID.getLine(), $ID.getCharPositionInLine());
            }
;

sentence_if returns [NodeInst inst]
    : IF LP expr=expression RP THEN list=list_cont_instruction  END IF
    {
        $inst = new If($expr.inst, $list.lista, null, $IF.getLine(), $IF.getCharPositionInLine());
    }
    | IF LP expr=expression RP THEN  END IF
    | IF LP expr=expression RP THEN list=list_cont_instruction ELSE listelse=list_cont_instruction END IF
    {
        $inst = new If($expr.inst, $list.lista, $listelse.lista, $IF.getLine(), $IF.getCharPositionInLine());
    }
    | IF LP expr=expression RP THEN list=list_cont_instruction ELSE sIf=list_If
    {
                $inst = new If($expr.inst, $list.lista, $sIf.lista, $IF.getLine(), $IF.getCharPositionInLine());
    }
    | IF LP expr=expression RP THEN  ELSE listelse=list_cont_instruction END IF
    {
            $inst = new If($expr.inst, null, $listelse.lista, $IF.getLine(), $IF.getCharPositionInLine());
    }
    ;

list_If returns[LinkedList<NodeInst> lista]
        @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
        : sublista=list_If  sIf=sentence_if
        {
            $sublista.lista.add($sIf.inst);
            $lista = $sublista.lista;
        }
        | sIf=sentence_if
        {
            $lista.add($sIf.inst);
        }
;

sentence_do returns[NodeInst inst]
        : DO var=ID EQUAL initialDo=expression COMMA endDo=expression COMMA stepDo=expression list=list_cont_instruction END DO
            {
                $inst = new For(new Assignment($var.text, $initialDo.inst, $DO.getLine(), $DO.getCharPositionInLine()),
                                       new Operations(
                                       new Primitives($var.text, Tipo.IDENTIFICADOR),$endDo.inst,"<=", $DO.getLine(), $DO.getCharPositionInLine()),
                                       new Assignment(
                                               $var.text,
                                               new Operations(
                                                   new Primitives($var.text, Tipo.IDENTIFICADOR),$stepDo.inst,"+", $DO.getLine(), $DO.getCharPositionInLine()
                                               )
                                               , $DO.getLine(), $DO.getCharPositionInLine()
                                           )
                                   , $list.lista, $DO.getLine(), $DO.getCharPositionInLine()
                                   );
            }
        | DO var=ID EQUAL initialDo=expression COMMA endDo=expression list=list_cont_instruction END DO
                  {
                      $inst = new For(new Assignment($var.text, $initialDo.inst, $DO.getLine(), $DO.getCharPositionInLine()),
                                                             new Operations(
                                                             new Primitives($var.text, Tipo.IDENTIFICADOR),$endDo.inst,"<=", $DO.getLine(), $DO.getCharPositionInLine()),
                                                             new Assignment(
                                                                     $var.text,
                                                                     new Operations(
                                                                         new Primitives($var.text, Tipo.IDENTIFICADOR), new Primitives("1", Tipo.ENTERO),"+", $DO.getLine(), $DO.getCharPositionInLine()
                                                                     )
                                                                     , $DO.getLine(), $DO.getCharPositionInLine()
                                                                 )
                                                         , $list.lista, $DO.getLine(), $DO.getCharPositionInLine()
                                                         );
                  }
;

sentence_do_while returns[NodeInst inst]
        : DO WHILE instr=expression list=list_cont_instruction END DO
            {
                $inst = new DoWhile($instr.inst, $list.lista, $DO.getLine(), $DO.getCharPositionInLine());
            }
;

sentence_array returns[NodeInst inst]
    : type=type_var COMMA DIMENSION LP dimension=dimension_array RP TWOPOINTS ID
    {
        $inst = new DeclarationArray($ID.text, $type.tipo, $dimension.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
    | type=type_var COMMA ALLOCATABLE TWOPOINTS ID LP dinamycDimension=dimension_array_dinamyc RP
    {
        $inst = new DeclarationArray($ID.text, $type.tipo, $dinamycDimension.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
;

sentence_asignacion_array returns[NodeInst inst]
    : ID CORCHL dimension=dimension_array CORCHR EQUAL exp=expression
    {
        $inst = new AsignArray($ID.text, $exp.inst, $dimension.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
;

sentence_asig_array_inicial returns[NodeInst inst]
    : ID EQUAL LP DIVISION dimension=dimension_array DIVISION RP
    {
        $inst = new AsignArray($ID.text, $dimension.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
;

sentence_allocate returns[NodeInst inst]
    : ALLOCATE LP ID LP dimension=dimension_array RP RP
    {
        $inst = new Allocate($ID.text, $dimension.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
;

sentence_deallocate returns[NodeInst inst]
    : DEALLOCATE LP ID RP
    {
        $inst = new Deallocate($ID.text, $ID.getLine(), $ID.getCharPositionInLine());
    }
;

dimension_array returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
    :sublista=dimension_array COMMA expr=expression
    {
        $sublista.lista.add($expr.inst);
        $lista = $sublista.lista;
    }
    | expr=expression
    {
        $lista.add($expr.inst);
    }
;

dimension_array_dinamyc returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
    : sublista=dimension_array_dinamyc COMMA TWOPOINT
    {
        $sublista.lista.add(new Primitives(0, Tipo.ENTERO));
        $lista = $sublista.lista;
    }
    | TWOPOINT
    {
        $lista.add(new Primitives(0, Tipo.ENTERO));
    }
;

access_array returns[NodeInst inst]
    : ID CORCHL dim=dimension_array CORCHR
    {
        $inst = new AccessArray($ID.text, $dim.lista, $ID.getLine(), $ID.getCharPositionInLine());
    }
;


list_expression returns[LinkedList<NodeInst> lista]
    @init{ LinkedList<NodeInst> a = new LinkedList<>(); $lista = a; }
    : sublista=list_expression COMMA expr=expression
        {
            $sublista.lista.add($expr.inst);
            $lista = $sublista.lista;
        }
    | expr=expression
        {
            $lista.add($expr.inst);
        }
;


expression returns[NodeInst inst]
    : exp=expression_arithmetic
        {
            $inst = $exp.inst;
        }
    | exp_rel=expression_relational
        {
            $inst = $exp_rel.inst;
        }
    | exp_asig=asignation
        {
            $inst = $exp_asig.inst;
        }
    | accArray=access_array
        {
            $inst = $accArray.inst;
        }
;

expression_arithmetic returns[NodeInst inst]
    : minus='-' opU = expression_arithmetic
        {
            $inst = new Operations($opU.inst, Tipo_Operations.NEGATIVO, $minus.getLine(), $minus.getCharPositionInLine());
        }
    | opIz = expression_arithmetic op=POWER opDe = expression_arithmetic
        {
            $inst = new Operations($opIz.inst,$opDe.inst,$op.text, $op.getLine(), $op.getCharPositionInLine());
        }
    | opIz = expression_arithmetic op=(MULTIPLICATION|DIVISION) opDe = expression_arithmetic
        {
            $inst = new Operations($opIz.inst,$opDe.inst,$op.text, $op.getLine(), $op.getCharPositionInLine());
        }
    | opIz = expression_arithmetic op=(SUM|SUBSTACT) opDe = expression_arithmetic
        {
            $inst = new Operations($opIz.inst,$opDe.inst,$op.text, $op.getLine(), $op.getCharPositionInLine());
        }
    | expr_valor = expression_value
        {
            $inst = $expr_valor.inst;
        }
    | LP exp=expression RP
        {
            $inst = $exp.inst;
        }
;

expression_relational returns[NodeInst inst]
    : opIz= expression_relational op=(EQUALS_ALTERNATIVE |EQUALS| INEQUALITY_ALTERNATIVE | INEQUALITY | GREATER_EQUAL_ALTERNATIVE | GREATER_EQUAL | LESS_EQUAL_ALTERNATIVE | LESS_EQUAL | LESS_THAN_ALTERNATIVE | LESS_THAN | GREATER_THAN_ALTERNATIVE | GREATER_THAN) opDe=expression_relational
        {
            $inst = new Operations($opIz.inst,$opDe.inst,$op.text, $op.getLine(), $op.getCharPositionInLine());
        }
    | opIzC= expression_relational op=(AND | OR) opDeC=expression_relational
          {
              $inst = new Operations($opIzC.inst,$opDeC.inst,$op.text, $op.getLine(), $op.getCharPositionInLine());
          }
     | NOT opDe=expression_relational
         {
             $inst = new Operations($opDe.inst,null,$NOT.text, $NOT.getLine(), $NOT.getCharPositionInLine());
         }
    | expr = expression_arithmetic
        {
            $inst = $expr.inst;
        }
;

expression_value returns[NodeInst inst]
    : primitivo
        {
            $inst = $primitivo.inst;
        }
    | accArray=access_array
        {
            $inst = $accArray.inst;
        }
     | cFunction=call_function
        {
                 $inst = $cFunction.inst;
        }
     | senSize=sent_size
        {
                 $inst = $senSize.inst;
        }
;

sent_size returns[NodeInst inst]
    : SIZE LP ID RP
    {
        $inst = new Size($ID.text, $SIZE.getLine(), $SIZE.getCharPositionInLine());
    }
;

primitivo returns[NodeInst inst]
    : NUMBER { $inst = new Primitives($NUMBER.text, Tipo.ENTERO, $NUMBER.getLine(), $NUMBER.getCharPositionInLine()); }
    | FLOAT  { $inst = new Primitives($FLOAT.text, Tipo.REAL, $FLOAT.getLine(), $FLOAT.getCharPositionInLine()); }
    | TRUE   { $inst = new Primitives($TRUE.text, Tipo.LOGICAL, $TRUE.getLine(), $TRUE.getCharPositionInLine()); }
    | FALSE  { $inst = new Primitives($FALSE.text, Tipo.LOGICAL, $FALSE.getLine(), $FALSE.getCharPositionInLine()); }
    | ID     { $inst = new Primitives($ID.text, Tipo.IDENTIFICADOR, $ID.getLine(), $ID.getCharPositionInLine()); }
    | STRING2  { $inst = new Primitives($STRING2.text, Tipo.CHARACTER, $STRING2.getLine(), $STRING2.getCharPositionInLine()); }
    | STRING { $inst = new Primitives($STRING.text, Tipo.CHARACTER, $STRING.getLine(), $STRING.getCharPositionInLine()); }
    | EXIT { $inst = new Primitives($EXIT.text, Tipo.EXIT, $EXIT.getLine(), $EXIT.getCharPositionInLine()); }
    | CYCLE { $inst = new Primitives($CYCLE.text, Tipo.CYCLE, $CYCLE.getLine(), $CYCLE.getCharPositionInLine()); }
;