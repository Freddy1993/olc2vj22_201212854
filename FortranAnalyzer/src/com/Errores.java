package com;

import Gramatica.SyntaxError;
import Tree.Excepcion;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Errores extends JFrame implements ActionListener {

    private JPanel panel1;
    private JTable table1;
    private JButton salirButton;

    public Errores() {
        Errores.this.setContentPane(Errores.this.panel1);
        Errores.this.setBounds(100,20,900,700);
        Errores.this.setVisible(true);


        String data[][] = new String[FortranAnalyzer.syntaxlistener.syntaxErrors.size()+FortranAnalyzer.sematicListener.size()][5];
        String col[] = {"No.","Linea","Columna","Descripción", "Tipo"};


        int indice = 0;
        if (FortranAnalyzer.syntaxlistener.syntaxErrors.size() != 0) {
            for (int i = 0; i < FortranAnalyzer.syntaxlistener.syntaxErrors.size(); i++) {
                SyntaxError obj = FortranAnalyzer.syntaxlistener.syntaxErrors.get(i);
                data[i][0] = String.valueOf(i+1);
                data[i][1] = String.valueOf(obj.getLine());
                data[i][2] = String.valueOf(obj.getCharPositionInLine());
                data[i][3] = obj.getMessage();
                data[i][4] = obj.getTipoError();
                indice++;
            }
        }
        if (FortranAnalyzer.sematicListener.size() != 0) {
            for (int i = 0; i < FortranAnalyzer.sematicListener.size(); i++) {
                Excepcion obj = FortranAnalyzer.sematicListener.get(i);

                data[indice][0] = String.valueOf(i+1);
                data[indice][1] = String.valueOf(obj.getLine());
                data[indice][2] = String.valueOf(obj.getColumn());
                data[indice][3] = obj.getDescription();
                data[indice][4] = obj.getTipo();
            }
        }


        DefaultTableModel model = new DefaultTableModel(data,col);
        table1.setModel(model);

    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }
}
