package com;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class JFileChooserConfirmOverwrite extends JFileChooser {
    public JFileChooserConfirmOverwrite() {
        this.setMultiSelectionEnabled(false);
    }

    public void approveSelection() {
        File selectedFile = this.getSelectedFile();
        if (selectedFile.exists()) {
            int answer = JOptionPane.showConfirmDialog(this, "Overwrite existing file?", "Overwrite?", 0);
            if (answer != 0) {
                return;
            }
        }

        super.approveSelection();
    }
}