package com;

import Gramatica.Gramatica;
import Gramatica.GramaticaLexer;
import Gramatica.SyntaxErrorListener;
import Tree.Arbol;
import Tree.Excepcion;
import Tree.TableSymbol;

import Gramatica.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import java.util.List;


import static org.antlr.v4.runtime.CharStreams.fromString;
import Utils.*;


public class FortranAnalyzer extends JFrame implements ActionListener {

    private JButton archivo;
    private JPanel panel;
    private JButton Analizar;
    private JTextArea textArea1;
    private JTextArea textArea2;
    private JTextArea textArea3;
    private JComboBox comboBox1;
    private JScrollPane jscrollpane;

    static ArrayList<String> LineNum = new ArrayList<String>();
    static ArrayList<String> Type = new ArrayList<String>();
    static ArrayList<String> Content = new ArrayList<String>();


    public static SyntaxErrorListener syntaxlistener = new SyntaxErrorListener();
    public static LinkedList<Excepcion> sematicListener = new LinkedList();

    public static TableSymbol tSymbol = new TableSymbol();

    NumeroLinea numeroLinea;
    public FortranAnalyzer() {
        numeroLinea = new NumeroLinea(textArea1);
        jscrollpane.setRowHeaderView(numeroLinea);

        archivo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser chooser = new JFileChooser(new File("C:/Users/BDGSA/Desktop/ProyectosUsac/Vacaciones Junio/compi2/Proyecto1/olc2vj22_201212854/FortranAnalyzer/src/ArchivosEntrada/test.f90"));
                int checkInput = chooser.showOpenDialog(null);
                textArea1.setText("");

                if (checkInput == JFileChooser.APPROVE_OPTION) {
                    File openedFile = chooser.getSelectedFile();
                    try {
                        BufferedReader in;
                        in = new BufferedReader(new FileReader(openedFile));
                        String line = in.readLine();
                        while (line != null) {
                            textArea1.setText(textArea1.getText() + "\n" + line);
                            line = in.readLine();
                        }
                    } catch (FileNotFoundException ex) {
                        System.out.println(ex.toString());
                    } catch (IOException ex) {
                        System.out.println(ex.toString());
                    }
                }
            }
        });
        Analizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    textArea2.setText("");
                    textArea3.setText("");
                    syntaxlistener.syntaxErrors.clear();
                    sematicListener.clear();
                    Utils.initParamas3D();

                    CharStream cs = fromString(textArea1.getText());
                    GramaticaLexer lexer = new GramaticaLexer(cs);

                    lexer.addErrorListener(syntaxlistener);

                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    Gramatica parser = new Gramatica(tokens);
                    parser.addErrorListener(syntaxlistener);

                    ParseTree tree = parser.initStart(); // parse
                    FormatTreeListener listener = new FormatTreeListener();

                    ParseTreeWalker.DEFAULT.walk(listener, tree);
                    Arbol ast = listener.ast;
                    TableSymbol tabla = new TableSymbol();
                    ast.execute(tabla, ast);

                    sematicListener = ast.excepciones;
                    if (sematicListener.size() == 0) {
                        ast.code3D(new TableSymbol(), ast);
                    }
                    if (ast.tablaDeSimbolosGlobal != null) {
                        tSymbol = ast.tablaDeSimbolosGlobal;
                    }
                    if (ast.console.size() != 0) {
                        for (Object objeto: ast.console) {
                            textArea2.append(objeto.toString() + "\n");
                        }
                    }

                    if (Utils.consola.size() != 0) {
                        for (Object objeto: Utils.consola) {
                            textArea3.append(objeto.toString() + "\n");
                        }
                    }

                    if(syntaxlistener.syntaxErrors.size() == 0) {
                        FormatTreeListener listeners = new FormatTreeListener();
                        ParseTreeWalker.DEFAULT.walk(listeners, tree);
                        Arbol astError = listeners.ast;
                        TableSymbol tablaError = new TableSymbol();
                        ast.execute(tablaError, astError);
                    } else {
                        for (int i = 0; i < syntaxlistener.syntaxErrors.size(); i++) {
                            SyntaxError obj = syntaxlistener.syntaxErrors.get(i);
                        }
                    }

                } catch (Exception e) {
                    System.out.println("Error en " + e);
                    throw new RuntimeException(e);
                }
            }
        });
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (comboBox1.getSelectedItem().equals("Arbol CST")) {
                    CharStream cs = fromString(textArea1.getText());
                    GramaticaLexer lexer = new GramaticaLexer(cs);

                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    Gramatica parser = new Gramatica(tokens);

                    //ParseTree tree = parser.stat();
                    /*List<String> rulesName = Arrays.asList(parser.getRuleNames());
                    TreeViewer viewer = new TreeViewer(rulesName, parser.stat());
                    viewer.open();*/

                    SimpleTree tree = new SimpleTree.Builder()
                            .withParser(parser)
                            .withParseTree(parser.initStart())
                            .withDisplaySymbolicName(false)
                            .build();

                    DotOptions options = new DotOptions.Builder()
                            .withParameters("  labelloc=\"t\";\n  label=\"Expression Tree\";\n\n")
                            .withLexerRuleShape("circle")
                            .build();

                    escribirArchivo( new DotTreeRepresentation().display(tree, options), "CST");
                } else if (comboBox1.getSelectedItem().equals("Arbol AST")) {
                    CharStream cs = fromString(textArea1.getText());
                    GramaticaLexer lexer = new GramaticaLexer(cs);

                    CommonTokenStream tokens = new CommonTokenStream(lexer);
                    Gramatica parser = new Gramatica(tokens);

                    ParserRuleContext ctx = parser.initStart();
                    generateAST(ctx,false,0);


                    escribirArchivo(printDOT(), "AST");


                } else if (comboBox1.getSelectedItem().equals("Errores")) {

                        Errores form = new Errores();
                        form.show();
                } else if (comboBox1.getSelectedItem().equals("Tabla de Simbolos")) {
                    Simbolos form = new Simbolos();
                    form.show();
                }
            }
        });
    }


    public void escribirArchivo(String cadena, String filename)
    {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter(filename + ".dot");
            pw = new PrintWriter(fichero);

            pw.println(cadena);

            dibujar(filename + ".dot",filename+ ".png");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para
                // asegurarnos que se cierra el fichero.
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
    public void dibujar( String direccionDot, String direccionPng ){
        try
        {
            ProcessBuilder pbuilder;

            /*
             * Realiza la construccion del comando
             * en la linea de comandos esto es:
             * dot -Tpng -o archivo.png archivo.dot
             */
            pbuilder = new ProcessBuilder( "dot", "-Tpng", "-o", direccionPng, direccionDot );
            pbuilder.redirectErrorStream( true );
            //Ejecuta el proceso
            pbuilder.start();

            openImage(direccionPng);


        } catch (Exception e) { e.printStackTrace(); }
    }

    public void openImage(String direction) {
        File file = new File(direction);

        //first check if Desktop is supported by Platform or not
        if(!Desktop.isDesktopSupported()){
            System.out.println("Desktop is not supported");
            return;
        }

        Desktop desktop = Desktop.getDesktop();
        if(file.exists()) {
            try {
                desktop.open(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void generateAST(RuleContext ctx, boolean verbose, int indentation) {
        boolean toBeIgnored = !verbose && ctx.getChildCount() == 1 && ctx.getChild(0) instanceof ParserRuleContext;

        if (!toBeIgnored) {
            String ruleName = Gramatica.ruleNames[ctx.getRuleIndex()];
            LineNum.add(Integer.toString(indentation));
            Type.add(ruleName);
            Content.add(ctx.getText());
        }
        for (int i = 0; i < ctx.getChildCount(); i++) {
            ParseTree element = ctx.getChild(i);
            if (element instanceof RuleContext) {
                generateAST((RuleContext) element, verbose, indentation + (toBeIgnored ? 0 : 1));
            }
        }
    }


    private String printDOT(){
        String dot = "graph tree {";

        for(int i =0; i<LineNum.size(); i++){
            dot += " " + LineNum.get(i)+i+"[label=\""+Type.get(i)+"  "+Content.get(i)+" \"]" + "\n";
        }

        int pos = 0;
        for(int i=1; i<LineNum.size();i++){
            pos=getPos(Integer.parseInt(LineNum.get(i))-1, i);
            dot += (Integer.parseInt(LineNum.get(i))-1)+Integer.toString(pos)+"--"+LineNum.get(i)+i+ "\n";
        }

        dot += "}";

        return  dot;
    }


    private static int getPos(int n, int limit){
        int pos = 0;
        for(int i=0; i<limit;i++){
            if(Integer.parseInt(LineNum.get(i))==n){
                pos = i;
            }
        }
        return pos;
    }


    public static void main(String[] args) {
        FortranAnalyzer form = new FortranAnalyzer();
        form.setContentPane(form.panel);
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setBounds(100,20,900,700);
        form.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }
}

class DotTreeRepresentation {

    public String display(SimpleTree tree) {
        return display(tree, DotOptions.DEFAULT);
    }

    public String display(SimpleTree tree, DotOptions options) {
        return display(new InOrderTraversal().traverse(tree), options);
    }

    public String display(List<SimpleTree.Node> nodes, DotOptions options) {

        StringBuilder builder = new StringBuilder("graph tree {\n\n");
        Map<SimpleTree.Node, String> nodeNameMap = new HashMap<>();
        int nodeCount = 0;

        if (options.parameters != null) {
            builder.append(options.parameters);
        }

        for (SimpleTree.Node node : nodes) {

            nodeCount++;
            String nodeName = String.format("node_%s", nodeCount);
            nodeNameMap.put(node, nodeName);

            builder.append(String.format("  %s [label=\"%s\", shape=%s];\n",
                    nodeName,
                    node.getLabel().replace("\"", "\\\""),
                    node.isTokenNode() ? options.lexerRuleShape : options.parserRuleShape));
        }

        builder.append("\n");

        for (SimpleTree.Node node : nodes) {

            String name = nodeNameMap.get(node);

            for (SimpleTree.Node child : node.getChildren()) {
                String childName = nodeNameMap.get(child);
                builder.append("  ").append(name).append(" -- ").append(childName).append("\n");
            }
        }

        return builder.append("}\n").toString();
    }
}

class InOrderTraversal {

    public List<SimpleTree.Node> traverse(SimpleTree tree) {

        if (tree == null)
            throw new IllegalArgumentException("tree == null");

        List<SimpleTree.Node> nodes = new ArrayList<>();

        traverse(tree.root, nodes);

        return nodes;
    }

    private void traverse(SimpleTree.Node node, List<SimpleTree.Node> nodes) {

        if (node.hasChildren()) {
            traverse(node.getChildren().get(0), nodes);
        }

        nodes.add(node);

        for (int i = 1; i < node.getChildCount(); i++) {
            traverse(node.getChild(i), nodes);
        }
    }
}

class DotOptions {

    public static final DotOptions DEFAULT = new DotOptions.Builder().build();

    public static final String DEFAULT_PARAMETERS = null;
    public static final String DEFAULT_LEXER_RULE_SHAPE = "box";
    public static final String DEFAULT_PARSER_RULE_SHAPE = "ellipse";

    public static class Builder {

        private String parameters = DEFAULT_PARAMETERS;
        private String lexerRuleShape = DEFAULT_LEXER_RULE_SHAPE;
        private String parserRuleShape = DEFAULT_PARSER_RULE_SHAPE;

        public DotOptions.Builder withParameters(String parameters) {
            this.parameters = parameters;
            return this;
        }

        public DotOptions.Builder withLexerRuleShape(String lexerRuleShape) {
            this.lexerRuleShape = lexerRuleShape;
            return this;
        }

        public DotOptions.Builder withParserRuleShape(String parserRuleShape) {
            this.parserRuleShape = parserRuleShape;
            return this;
        }

        public DotOptions build() {

            if (lexerRuleShape == null)
                throw new IllegalStateException("lexerRuleShape == null");

            if (parserRuleShape == null)
                throw new IllegalStateException("parserRuleShape == null");

            return new DotOptions(parameters, lexerRuleShape, parserRuleShape);
        }
    }

    public final String parameters;
    public final String lexerRuleShape;
    public final String parserRuleShape;

    private DotOptions(String parameters, String lexerRuleShape, String parserRuleShape) {
        this.parameters = parameters;
        this.lexerRuleShape = lexerRuleShape;
        this.parserRuleShape = parserRuleShape;
    }
}

class SimpleTree {

    public static class Builder {

        private Parser parser = null;
        private ParseTree parseTree = null;
        private Set<Integer> ignoredTokenTypes = new HashSet<>();
        private boolean displaySymbolicName = true;

        public SimpleTree build() {

            if (parser == null) {
                throw new  IllegalStateException("parser == null");
            }

            if (parseTree == null) {
                throw new  IllegalStateException("parseTree == null");
            }

            return new SimpleTree(parser, parseTree, ignoredTokenTypes, displaySymbolicName);
        }

        public SimpleTree.Builder withParser(Parser parser) {
            this.parser = parser;
            return this;
        }

        public SimpleTree.Builder withParseTree(ParseTree parseTree) {
            this.parseTree = parseTree;
            return this;
        }

        public SimpleTree.Builder withIgnoredTokenTypes(Integer... ignoredTokenTypes) {
            this.ignoredTokenTypes = new HashSet<>(Arrays.asList(ignoredTokenTypes));
            return this;
        }

        public SimpleTree.Builder withDisplaySymbolicName(boolean displaySymbolicName) {
            this.displaySymbolicName = displaySymbolicName;
            return this;
        }
    }

    public final SimpleTree.Node root;

    private SimpleTree(Parser parser, ParseTree parseTree, Set<Integer> ignoredTokenTypes, boolean displaySymbolicName) {
        this.root = new SimpleTree.Node(parser, parseTree, ignoredTokenTypes, displaySymbolicName);
    }

    public SimpleTree(SimpleTree.Node root) {

        if (root == null)
            throw new IllegalArgumentException("root == null");

        this.root = root;
    }

    public SimpleTree copy() {
        return new SimpleTree(root.copy());
    }

    public String toLispTree() {

        StringBuilder builder = new StringBuilder();

        toLispTree(this.root, builder);

        return builder.toString().trim();
    }

    private void toLispTree(SimpleTree.Node node, StringBuilder builder) {

        if (node.isLeaf()) {
            builder.append(node.getLabel()).append(" ");
        }
        else {
            builder.append("(").append(node.label).append(" ");

            for (SimpleTree.Node child : node.children) {
                toLispTree(child, builder);
            }

            builder.append(") ");
        }
    }

    @Override
    public String toString() {
        return String.format("%s", this.root);
    }

    public static class Node {

        protected String label;
        protected int level;
        protected boolean isTokenNode;
        protected List<SimpleTree.Node> children;

        Node(Parser parser, ParseTree parseTree, Set<Integer> ignoredTokenTypes, boolean displaySymbolicName) {
            this(parser.getRuleNames()[((RuleContext)parseTree).getRuleIndex()], 0, false);
            traverse(parseTree, this, parser, ignoredTokenTypes, displaySymbolicName);
        }

        public Node(String label, int level, boolean isTokenNode) {
            this.label = label;
            this.level = level;
            this.isTokenNode = isTokenNode;
            this.children = new ArrayList<>();
        }

        public void replaceWith(SimpleTree.Node node) {
            this.label = node.label;
            this.level = node.level;
            this.isTokenNode = node.isTokenNode;
            this.children.remove(node);
            this.children.addAll(node.children);
        }

        public SimpleTree.Node copy() {

            SimpleTree.Node copy = new SimpleTree.Node(this.label, this.level, this.isTokenNode);

            for (SimpleTree.Node child : this.children) {
                copy.children.add(child.copy());
            }

            return copy;
        }

        public void normalizeLevels(int level) {

            this.level = level;

            for (SimpleTree.Node child : children) {
                child.normalizeLevels(level + 1);
            }
        }

        public boolean hasChildren() {
            return !children.isEmpty();
        }

        public boolean isLeaf() {
            return !hasChildren();
        }

        public int getChildCount() {
            return children.size();
        }

        public SimpleTree.Node getChild(int index) {
            return children.get(index);
        }

        public int getLevel() {
            return level;
        }

        public String getLabel() {
            return label;
        }

        public boolean isTokenNode() {
            return isTokenNode;
        }

        public List<SimpleTree.Node> getChildren() {
            return new ArrayList<>(children);
        }

        private void traverse(ParseTree parseTree, SimpleTree.Node parent, Parser parser, Set<Integer> ignoredTokenTypes, boolean displaySymbolicName) {

            List<SimpleTree.ParseTreeParent> todo = new ArrayList<>();

            for (int i = 0; i < parseTree.getChildCount(); i++) {

                ParseTree child = parseTree.getChild(i);

                if (child.getPayload() instanceof CommonToken) {

                    CommonToken token = (CommonToken) child.getPayload();

                    if (!ignoredTokenTypes.contains(token.getType())) {

                        String tempText = displaySymbolicName ?
                                String.format("%s: '%s'",
                                        parser.getVocabulary().getSymbolicName(token.getType()),
                                        token.getText()
                                                .replace("\r", "\\r")
                                                .replace("\n", "\\n")
                                                .replace("\t", "\\t")
                                                .replace("'", "\\'")) :
                                String.format("%s",
                                        token.getText()
                                                .replace("\r", "\\r")
                                                .replace("\n", "\\n")
                                                .replace("\t", "\\t"));

                        if (parent.label == null) {
                            parent.label = tempText;
                        }
                        else {
                            parent.children.add(new SimpleTree.Node(tempText, parent.level + 1, true));
                        }
                    }
                }
                else {
                    SimpleTree.Node node = new SimpleTree.Node(parser.getRuleNames()[((RuleContext)child).getRuleIndex()], parent.level + 1, false);
                    parent.children.add(node);
                    todo.add(new SimpleTree.ParseTreeParent(child, node));
                }
            }

            for (SimpleTree.ParseTreeParent wrapper : todo) {
                traverse(wrapper.parseTree, wrapper.parent, parser, ignoredTokenTypes, displaySymbolicName);
            }
        }

        @Override
        public String toString() {
            return String.format("{label=%s, level=%s, isTokenNode=%s, children=%s}", label, level, isTokenNode, children);
        }
    }

    private static class ParseTreeParent {

        final ParseTree parseTree;
        final SimpleTree.Node parent;

        private ParseTreeParent(ParseTree parseTree, SimpleTree.Node parent) {
            this.parseTree = parseTree;
            this.parent = parent;
        }
    }
}