package com;

import org.antlr.v4.runtime.tree.Tree;

public interface TreeTextProvider {
    String getText(Tree var1);
}
