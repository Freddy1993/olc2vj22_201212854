package com;

import Tree.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class Simbolos extends JFrame implements ActionListener {

    private JPanel panel1;
    private JButton salirButton;
    private JTable table1;

    public Simbolos() {
        Simbolos.this.setContentPane(Simbolos.this.panel1);
        Simbolos.this.setBounds(100,20,900,700);
        Simbolos.this.setVisible(true);

        String data[][] = new String[FortranAnalyzer.tSymbol.size()][6];
        String col[] = {"No.","Tipo","ID","Valor", "Dimensiones", "Ambito"};

        if (FortranAnalyzer.tSymbol.size() != 0) {

            for (int i = 0; i < FortranAnalyzer.tSymbol.size(); i++) {
                LinkedList<String> valores = new LinkedList<>();
                String dataArray = "";
                Symbol obj = FortranAnalyzer.tSymbol.get(i);
                data[i][0] = String.valueOf(i+1);
                data[i][1] = String.valueOf(obj.getTipo());
                data[i][2] = String.valueOf(obj.getId());
                data[i][5] = String.valueOf(obj.getAmbito());

                if (obj.getTamaniosDimensiones() != null) {
                    if (obj.getTamaniosDimensiones().size() > 0) {

                        ArrayNode nodInicial = (ArrayNode) obj.getValor();
                        mostrarArregloDinamico(nodInicial.getCeldasVecinas(), valores);
                        String valoresArreglo = valores.toString();
                        if(obj.getTamaniosDimensiones().size() == 1) {
                            valoresArreglo = valoresArreglo.substring(1,valoresArreglo.toString().length() - 1);
                        }
                        dataArray += valoresArreglo;
                        data[i][3] = dataArray;
                        data[i][4] = String.valueOf(obj.getTamaniosDimensiones().size());
                    }
                } else {
                    if(obj.getValor() ==  null) {
                        data[i][3] = String.valueOf(getValorDefault(obj.getTipo()));
                    } else {
                        data[i][3] = String.valueOf(obj.getValor());
                    }

                    if(obj.getTamaniosDimensiones() == null) {
                        data[i][4] = String.valueOf("-");
                    } else {
                        data[i][4] = String.valueOf(obj.getTamaniosDimensiones());
                    }
                }


            }
        }

        DefaultTableModel model = new DefaultTableModel(data,col);
        table1.setModel(model);


    }

    public Object getValorDefault(Symbol.Tipo tipo) {
        if (tipo == Symbol.Tipo.ENTERO) {
            return 0;
        } else if (tipo == Symbol.Tipo.REAL) {
            return 0.00000000;
        } else if (tipo == Symbol.Tipo.COMPLEX) {
            return 0.00000000;
        } else if (tipo == Symbol.Tipo.CHARACTER) {
            return "";
        } else if (tipo == Symbol.Tipo.LOGICAL) {
            return false;
        }

        return "";
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    public void mostrarArregloDinamico(LinkedList<ArrayNode> obj, LinkedList<String> valores) {
        String aux = "";
        for (int j = 0; j < obj.size(); j++) {
            if(obj.get(j).getCeldasVecinas().size() > 0) {
                mostrarArregloDinamico(obj.get(j).getCeldasVecinas(), valores);
            } else {
                Object valor = obj.get(j).getValorCeldas();
                if(!(valor instanceof Nulo)){
                    aux = aux + " " + obj.get(j).getValorCeldas().toString();
                }

            }
        }
        if(!aux.isEmpty()) {
            valores.add("[" + aux + "]");
        }
    }
}
