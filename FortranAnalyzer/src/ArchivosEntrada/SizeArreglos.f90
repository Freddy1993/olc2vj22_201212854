program compi
    implicit none

    integer :: i , j
    integer, dimension( 10 ) :: array
    array = (/ 2 , 5 , 7 , 6 , 8 , 1 , 9 , 6 , 8 , 3 /)

    do i = 1, size ( array ) - 1 , 1
        do j = i + 1, size ( array ) - 1, 1
            if (array[ i ] > array [ j ] ) then
                integer :: aux = array [ i ]
                array [ i ] = array [ j ]
                array [ j ] = aux
            end if
        end do
    end do

    print *, array

end program compi