
subroutine multiplicacion(c, d)
  implicit none

  integer, intent(in) :: c
  integer, intent(in) :: d

  print*, c*d

end subroutine multiplicacion

subroutine suma(a, b)
  implicit none

  integer, intent(in) :: a
  integer, intent(in) :: b

  if(b < a) then
    call multiplicacion(a, b)
  else
    print*, a+b
  end if

end subroutine suma


program myProgram
  implicit none
  integer :: i, j

  call suma(100, 2)

end program myProgram