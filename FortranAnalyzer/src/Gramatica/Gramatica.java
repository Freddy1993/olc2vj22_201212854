// Generated from C:/Users/BDGSA/Desktop/ProyectosUsac/Vacaciones Junio/compi2/Proyecto1/olc2vj22_201212854/FortranAnalyzer/src/com\Gramatica.g4 by ANTLR 4.10.1
package Gramatica;

     import java.util.HashMap;
        import Tree.Arbol;
        import Tree.PrintInstruction;
        import Tree.NodeInst;
        import Tree.Operations;
        import Tree.Primitives;
        import Tree.Symbol.Tipo;
        import Tree.Operations.Tipo_Operations;
        import Tree.Assignment;
        import Tree.Declaracion;
        import Tree.Identifier;
        import Tree.For;
        import Tree.DoWhile;
        import Tree.If;
        import java.util.LinkedList;
        import Tree.AccessArray;
        import Tree.AsignArray;
        import Tree.DeclarationArray;
        import Tree.Size;
        import Tree.ParamsFunction;
        import Tree.Function;
        import Tree.CallFunction;
        import Tree.Return;
        import Tree.Allocate;
        import Tree.Deallocate;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class Gramatica extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		EQUAL=1, TWOPOINT=2, COMMA=3, EXCLAMATION=4, SINGLE_QUOTE=5, DOUBLE_QUOTE=6, 
		LP=7, RP=8, TWOPOINTS=9, PROGRAM=10, IMPLICIT=11, NONE=12, CORCHL=13, 
		CORCHR=14, END=15, THEN=16, EXIT=17, CYCLE=18, PRINTLN=19, INTEGER=20, 
		REAL=21, COMPLEX=22, CHARACTER=23, LOGICAL=24, SUM=25, SUBSTACT=26, MULTIPLICATION=27, 
		DIVISION=28, POWER=29, EQUALS=30, EQUALS_ALTERNATIVE=31, INEQUALITY=32, 
		INEQUALITY_ALTERNATIVE=33, GREATER_THAN=34, GREATER_THAN_ALTERNATIVE=35, 
		LESS_THAN=36, LESS_THAN_ALTERNATIVE=37, GREATER_EQUAL=38, GREATER_EQUAL_ALTERNATIVE=39, 
		LESS_EQUAL=40, LESS_EQUAL_ALTERNATIVE=41, AND=42, OR=43, NOT=44, DIMENSION=45, 
		SIZE=46, ALLOCATABLE=47, ALLOCATE=48, DEALLOCATE=49, IF=50, ELSE=51, DO=52, 
		WHILE=53, INTENT=54, IN=55, OUT=56, INOUT=57, CALL=58, SUBROUTINE=59, 
		FUNCTION=60, RESULT=61, NUMBER=62, FLOAT=63, STRING=64, STRING2=65, TRUE=66, 
		FALSE=67, ID=68, WHITESPACE=69, COMMENT=70;
	public static final int
		RULE_initStart = 0, RULE_body = 1, RULE_listFunctions = 2, RULE_instruction = 3, 
		RULE_function = 4, RULE_list_paramaters = 5, RULE_call_function = 6, RULE_program = 7, 
		RULE_list_cont_instruction = 8, RULE_content_inst = 9, RULE_asignation = 10, 
		RULE_variables = 11, RULE_declareVars = 12, RULE_type_declare_var = 13, 
		RULE_type_var = 14, RULE_list_variable = 15, RULE_variable = 16, RULE_sentence_if = 17, 
		RULE_list_If = 18, RULE_sentence_do = 19, RULE_sentence_do_while = 20, 
		RULE_sentence_array = 21, RULE_sentence_asignacion_array = 22, RULE_sentence_asig_array_inicial = 23, 
		RULE_sentence_allocate = 24, RULE_sentence_deallocate = 25, RULE_dimension_array = 26, 
		RULE_dimension_array_dinamyc = 27, RULE_access_array = 28, RULE_list_expression = 29, 
		RULE_expression = 30, RULE_expression_arithmetic = 31, RULE_expression_relational = 32, 
		RULE_expression_value = 33, RULE_sent_size = 34, RULE_primitivo = 35;
	private static String[] makeRuleNames() {
		return new String[] {
			"initStart", "body", "listFunctions", "instruction", "function", "list_paramaters", 
			"call_function", "program", "list_cont_instruction", "content_inst", 
			"asignation", "variables", "declareVars", "type_declare_var", "type_var", 
			"list_variable", "variable", "sentence_if", "list_If", "sentence_do", 
			"sentence_do_while", "sentence_array", "sentence_asignacion_array", "sentence_asig_array_inicial", 
			"sentence_allocate", "sentence_deallocate", "dimension_array", "dimension_array_dinamyc", 
			"access_array", "list_expression", "expression", "expression_arithmetic", 
			"expression_relational", "expression_value", "sent_size", "primitivo"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'='", "':'", "','", "'!'", "'''", "'\"'", "'('", "')'", "'::'", 
			"'program'", "'implicit'", "'none'", "'['", "']'", "'end'", "'then'", 
			"'exit'", "'cycle'", "'print'", "'integer'", "'real'", "'complex'", "'character'", 
			"'logical'", "'+'", "'-'", "'*'", "'/'", "'**'", "'.eq.'", "'=='", "'.ne.'", 
			"'/='", "'.gt.'", "'>'", "'.lt.'", "'<'", "'.ge.'", "'>='", "'.le.'", 
			"'<='", "'.and.'", "'.or.'", "'.not.'", "'dimension'", "'size'", "'allocatable'", 
			"'allocate'", "'deallocate'", "'if'", "'else'", "'do'", "'while'", "'intent'", 
			"'in'", "'out'", "'inout'", "'call'", "'subroutine'", "'function'", "'result'", 
			null, null, null, null, "'.true.'", "'.false.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "EQUAL", "TWOPOINT", "COMMA", "EXCLAMATION", "SINGLE_QUOTE", "DOUBLE_QUOTE", 
			"LP", "RP", "TWOPOINTS", "PROGRAM", "IMPLICIT", "NONE", "CORCHL", "CORCHR", 
			"END", "THEN", "EXIT", "CYCLE", "PRINTLN", "INTEGER", "REAL", "COMPLEX", 
			"CHARACTER", "LOGICAL", "SUM", "SUBSTACT", "MULTIPLICATION", "DIVISION", 
			"POWER", "EQUALS", "EQUALS_ALTERNATIVE", "INEQUALITY", "INEQUALITY_ALTERNATIVE", 
			"GREATER_THAN", "GREATER_THAN_ALTERNATIVE", "LESS_THAN", "LESS_THAN_ALTERNATIVE", 
			"GREATER_EQUAL", "GREATER_EQUAL_ALTERNATIVE", "LESS_EQUAL", "LESS_EQUAL_ALTERNATIVE", 
			"AND", "OR", "NOT", "DIMENSION", "SIZE", "ALLOCATABLE", "ALLOCATE", "DEALLOCATE", 
			"IF", "ELSE", "DO", "WHILE", "INTENT", "IN", "OUT", "INOUT", "CALL", 
			"SUBROUTINE", "FUNCTION", "RESULT", "NUMBER", "FLOAT", "STRING", "STRING2", 
			"TRUE", "FALSE", "ID", "WHITESPACE", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Gramatica.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	    HashMap memory = new HashMap();
	    LinkedList<NodeInst> a = new LinkedList<>();

	public Gramatica(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class InitStartContext extends ParserRuleContext {
		public Arbol ast;
		public BodyContext inst;
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public InitStartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initStart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterInitStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitInitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitInitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitStartContext initStart() throws RecognitionException {
		InitStartContext _localctx = new InitStartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_initStart);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			((InitStartContext)_localctx).inst = body();

			            ((InitStartContext)_localctx).ast =  new Arbol(((InitStartContext)_localctx).inst.lista);
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public ListFunctionsContext ins;
		public ProgramContext inst;
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public List<ListFunctionsContext> listFunctions() {
			return getRuleContexts(ListFunctionsContext.class);
		}
		public ListFunctionsContext listFunctions(int i) {
			return getRuleContext(ListFunctionsContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_body);
		 LinkedList<NodeInst> concatList = new LinkedList<>(); ((BodyContext)_localctx).lista =  concatList; 
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SUBROUTINE || _la==FUNCTION) {
				{
				{
				setState(75);
				((BodyContext)_localctx).ins = listFunctions(0);
				}
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(81);
			((BodyContext)_localctx).inst = program();

			        if ( _localctx.ins!=null ) _localctx.lista.addAll(((BodyContext)_localctx).ins.lista);
			        _localctx.lista.addAll(((BodyContext)_localctx).inst.lista);
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListFunctionsContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public ListFunctionsContext sublista;
		public InstructionContext instru;
		public InstructionContext instruction() {
			return getRuleContext(InstructionContext.class,0);
		}
		public ListFunctionsContext listFunctions() {
			return getRuleContext(ListFunctionsContext.class,0);
		}
		public ListFunctionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listFunctions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterListFunctions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitListFunctions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitListFunctions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ListFunctionsContext listFunctions() throws RecognitionException {
		return listFunctions(0);
	}

	private ListFunctionsContext listFunctions(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ListFunctionsContext _localctx = new ListFunctionsContext(_ctx, _parentState);
		ListFunctionsContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_listFunctions, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((ListFunctionsContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(85);
			((ListFunctionsContext)_localctx).instru = instruction();

			        _localctx.lista.add(((ListFunctionsContext)_localctx).instru.inst);
			    
			}
			_ctx.stop = _input.LT(-1);
			setState(94);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ListFunctionsContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_listFunctions);
					setState(88);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(89);
					((ListFunctionsContext)_localctx).instru = instruction();

					                  ((ListFunctionsContext)_localctx).sublista.lista.add(((ListFunctionsContext)_localctx).instru.inst);
					                  ((ListFunctionsContext)_localctx).lista =  ((ListFunctionsContext)_localctx).sublista.lista;
					              
					}
					} 
				}
				setState(96);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class InstructionContext extends ParserRuleContext {
		public NodeInst inst;
		public FunctionContext funct;
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public InstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterInstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitInstruction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitInstruction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstructionContext instruction() throws RecognitionException {
		InstructionContext _localctx = new InstructionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_instruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			((InstructionContext)_localctx).funct = function();

			        ((InstructionContext)_localctx).inst =  ((InstructionContext)_localctx).funct.inst;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public NodeInst inst;
		public Token FUNCTION;
		public Token idIni;
		public List_paramatersContext params;
		public Token idResult;
		public List_cont_instructionContext instr;
		public Token idFin;
		public Token SUBROUTINE;
		public List<TerminalNode> FUNCTION() { return getTokens(Gramatica.FUNCTION); }
		public TerminalNode FUNCTION(int i) {
			return getToken(Gramatica.FUNCTION, i);
		}
		public List<TerminalNode> LP() { return getTokens(Gramatica.LP); }
		public TerminalNode LP(int i) {
			return getToken(Gramatica.LP, i);
		}
		public List<TerminalNode> RP() { return getTokens(Gramatica.RP); }
		public TerminalNode RP(int i) {
			return getToken(Gramatica.RP, i);
		}
		public TerminalNode RESULT() { return getToken(Gramatica.RESULT, 0); }
		public TerminalNode IMPLICIT() { return getToken(Gramatica.IMPLICIT, 0); }
		public TerminalNode NONE() { return getToken(Gramatica.NONE, 0); }
		public TerminalNode END() { return getToken(Gramatica.END, 0); }
		public List<TerminalNode> ID() { return getTokens(Gramatica.ID); }
		public TerminalNode ID(int i) {
			return getToken(Gramatica.ID, i);
		}
		public List_paramatersContext list_paramaters() {
			return getRuleContext(List_paramatersContext.class,0);
		}
		public List_cont_instructionContext list_cont_instruction() {
			return getRuleContext(List_cont_instructionContext.class,0);
		}
		public List<TerminalNode> SUBROUTINE() { return getTokens(Gramatica.SUBROUTINE); }
		public TerminalNode SUBROUTINE(int i) {
			return getToken(Gramatica.SUBROUTINE, i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_function);
		try {
			setState(158);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(100);
				((FunctionContext)_localctx).FUNCTION = match(FUNCTION);
				setState(101);
				((FunctionContext)_localctx).idIni = match(ID);
				setState(102);
				match(LP);
				setState(103);
				((FunctionContext)_localctx).params = list_paramaters();
				setState(104);
				match(RP);
				setState(105);
				match(RESULT);
				setState(106);
				match(LP);
				setState(107);
				((FunctionContext)_localctx).idResult = match(ID);
				setState(108);
				match(RP);
				setState(109);
				match(IMPLICIT);
				setState(110);
				match(NONE);
				setState(111);
				((FunctionContext)_localctx).instr = list_cont_instruction(0);
				setState(112);
				match(END);
				setState(113);
				((FunctionContext)_localctx).FUNCTION = match(FUNCTION);
				setState(114);
				((FunctionContext)_localctx).idFin = match(ID);

				            ((FunctionContext)_localctx).inst =  new Function((((FunctionContext)_localctx).idIni!=null?((FunctionContext)_localctx).idIni.getText():null), ((FunctionContext)_localctx).params.lista, ((FunctionContext)_localctx).instr.lista, new Return(new Identifier((((FunctionContext)_localctx).idResult!=null?((FunctionContext)_localctx).idResult.getText():null), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine()), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine()), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine());
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(117);
				((FunctionContext)_localctx).SUBROUTINE = match(SUBROUTINE);
				setState(118);
				((FunctionContext)_localctx).idIni = match(ID);
				setState(119);
				match(LP);
				setState(120);
				((FunctionContext)_localctx).params = list_paramaters();
				setState(121);
				match(RP);
				setState(122);
				match(IMPLICIT);
				setState(123);
				match(NONE);
				setState(124);
				((FunctionContext)_localctx).instr = list_cont_instruction(0);
				setState(125);
				match(END);
				setState(126);
				((FunctionContext)_localctx).SUBROUTINE = match(SUBROUTINE);
				setState(127);
				((FunctionContext)_localctx).idFin = match(ID);

				          ((FunctionContext)_localctx).inst =  new Function((((FunctionContext)_localctx).idIni!=null?((FunctionContext)_localctx).idIni.getText():null), ((FunctionContext)_localctx).params.lista, ((FunctionContext)_localctx).instr.lista, null, ((FunctionContext)_localctx).SUBROUTINE.getLine(), ((FunctionContext)_localctx).SUBROUTINE.getCharPositionInLine());
				        
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(130);
				((FunctionContext)_localctx).FUNCTION = match(FUNCTION);
				setState(131);
				((FunctionContext)_localctx).idIni = match(ID);
				setState(132);
				match(LP);
				setState(133);
				match(RP);
				setState(134);
				match(RESULT);
				setState(135);
				match(LP);
				setState(136);
				((FunctionContext)_localctx).idResult = match(ID);
				setState(137);
				match(RP);
				setState(138);
				match(IMPLICIT);
				setState(139);
				match(NONE);
				setState(140);
				((FunctionContext)_localctx).instr = list_cont_instruction(0);
				setState(141);
				match(END);
				setState(142);
				((FunctionContext)_localctx).FUNCTION = match(FUNCTION);
				setState(143);
				((FunctionContext)_localctx).idFin = match(ID);

				            ((FunctionContext)_localctx).inst =  new Function((((FunctionContext)_localctx).idIni!=null?((FunctionContext)_localctx).idIni.getText():null), new LinkedList<>(), ((FunctionContext)_localctx).instr.lista, new Return(new Identifier((((FunctionContext)_localctx).idResult!=null?((FunctionContext)_localctx).idResult.getText():null), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine()), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine()), ((FunctionContext)_localctx).FUNCTION.getLine(), ((FunctionContext)_localctx).FUNCTION.getCharPositionInLine());
				        
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(146);
				((FunctionContext)_localctx).SUBROUTINE = match(SUBROUTINE);
				setState(147);
				((FunctionContext)_localctx).idIni = match(ID);
				setState(148);
				match(LP);
				setState(149);
				match(RP);
				setState(150);
				match(IMPLICIT);
				setState(151);
				match(NONE);
				setState(152);
				((FunctionContext)_localctx).instr = list_cont_instruction(0);
				setState(153);
				match(END);
				setState(154);
				((FunctionContext)_localctx).SUBROUTINE = match(SUBROUTINE);
				setState(155);
				((FunctionContext)_localctx).idFin = match(ID);

				          ((FunctionContext)_localctx).inst =  new Function((((FunctionContext)_localctx).idIni!=null?((FunctionContext)_localctx).idIni.getText():null), new LinkedList<>(), ((FunctionContext)_localctx).instr.lista, null, ((FunctionContext)_localctx).SUBROUTINE.getLine(), ((FunctionContext)_localctx).SUBROUTINE.getCharPositionInLine());
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_paramatersContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_variableContext sublista;
		public VariableContext var;
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public List_variableContext list_variable() {
			return getRuleContext(List_variableContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public List_paramatersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_paramaters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_paramaters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_paramaters(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_paramaters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_paramatersContext list_paramaters() throws RecognitionException {
		List_paramatersContext _localctx = new List_paramatersContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_list_paramaters);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((List_paramatersContext)_localctx).lista =  a; 
		try {
			setState(168);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(160);
				((List_paramatersContext)_localctx).sublista = list_variable(0);
				setState(161);
				match(COMMA);
				setState(162);
				((List_paramatersContext)_localctx).var = variable();

				            ((List_paramatersContext)_localctx).sublista.lista.add(((List_paramatersContext)_localctx).var.inst);
				            ((List_paramatersContext)_localctx).lista =  ((List_paramatersContext)_localctx).sublista.lista;
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(165);
				((List_paramatersContext)_localctx).var = variable();

				            _localctx.lista.add(((List_paramatersContext)_localctx).var.inst);
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Call_functionContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public Dimension_arrayContext params;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public TerminalNode CALL() { return getToken(Gramatica.CALL, 0); }
		public Call_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCall_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCall_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCall_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Call_functionContext call_function() throws RecognitionException {
		Call_functionContext _localctx = new Call_functionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_call_function);
		try {
			setState(192);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(170);
				((Call_functionContext)_localctx).ID = match(ID);
				setState(171);
				match(LP);
				setState(172);
				((Call_functionContext)_localctx).params = dimension_array(0);
				setState(173);
				match(RP);

				            ((Call_functionContext)_localctx).inst =  new CallFunction((((Call_functionContext)_localctx).ID!=null?((Call_functionContext)_localctx).ID.getText():null), ((Call_functionContext)_localctx).params.lista, ((Call_functionContext)_localctx).ID.getLine(), ((Call_functionContext)_localctx).ID.getCharPositionInLine());
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(176);
				match(CALL);
				setState(177);
				((Call_functionContext)_localctx).ID = match(ID);
				setState(178);
				match(LP);
				setState(179);
				((Call_functionContext)_localctx).params = dimension_array(0);
				setState(180);
				match(RP);

				            ((Call_functionContext)_localctx).inst =  new CallFunction((((Call_functionContext)_localctx).ID!=null?((Call_functionContext)_localctx).ID.getText():null), ((Call_functionContext)_localctx).params.lista, ((Call_functionContext)_localctx).ID.getLine(), ((Call_functionContext)_localctx).ID.getCharPositionInLine());
				        
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(183);
				((Call_functionContext)_localctx).ID = match(ID);
				setState(184);
				match(LP);
				setState(185);
				match(RP);

				          ((Call_functionContext)_localctx).inst =  new CallFunction((((Call_functionContext)_localctx).ID!=null?((Call_functionContext)_localctx).ID.getText():null), new LinkedList<>(), ((Call_functionContext)_localctx).ID.getLine(), ((Call_functionContext)_localctx).ID.getCharPositionInLine());
				      
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(187);
				match(CALL);
				setState(188);
				((Call_functionContext)_localctx).ID = match(ID);
				setState(189);
				match(LP);
				setState(190);
				match(RP);

				          ((Call_functionContext)_localctx).inst =  new CallFunction((((Call_functionContext)_localctx).ID!=null?((Call_functionContext)_localctx).ID.getText():null), new LinkedList<>(), ((Call_functionContext)_localctx).ID.getLine(), ((Call_functionContext)_localctx).ID.getCharPositionInLine());
				      
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_cont_instructionContext cont;
		public List<TerminalNode> PROGRAM() { return getTokens(Gramatica.PROGRAM); }
		public TerminalNode PROGRAM(int i) {
			return getToken(Gramatica.PROGRAM, i);
		}
		public List<TerminalNode> ID() { return getTokens(Gramatica.ID); }
		public TerminalNode ID(int i) {
			return getToken(Gramatica.ID, i);
		}
		public TerminalNode IMPLICIT() { return getToken(Gramatica.IMPLICIT, 0); }
		public TerminalNode NONE() { return getToken(Gramatica.NONE, 0); }
		public TerminalNode END() { return getToken(Gramatica.END, 0); }
		public List_cont_instructionContext list_cont_instruction() {
			return getRuleContext(List_cont_instructionContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(PROGRAM);
			setState(195);
			match(ID);
			setState(196);
			match(IMPLICIT);
			setState(197);
			match(NONE);
			setState(198);
			((ProgramContext)_localctx).cont = list_cont_instruction(0);
			setState(199);
			match(END);
			setState(200);
			match(PROGRAM);
			setState(201);
			match(ID);

			       ((ProgramContext)_localctx).lista =  ((ProgramContext)_localctx).cont.lista;
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_cont_instructionContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_cont_instructionContext sublista;
		public Content_instContext cont;
		public Content_instContext content_inst() {
			return getRuleContext(Content_instContext.class,0);
		}
		public List_cont_instructionContext list_cont_instruction() {
			return getRuleContext(List_cont_instructionContext.class,0);
		}
		public List_cont_instructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_cont_instruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_cont_instruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_cont_instruction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_cont_instruction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_cont_instructionContext list_cont_instruction() throws RecognitionException {
		return list_cont_instruction(0);
	}

	private List_cont_instructionContext list_cont_instruction(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		List_cont_instructionContext _localctx = new List_cont_instructionContext(_ctx, _parentState);
		List_cont_instructionContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_list_cont_instruction, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((List_cont_instructionContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(205);
			((List_cont_instructionContext)_localctx).cont = content_inst();

			            _localctx.lista.add(((List_cont_instructionContext)_localctx).cont.inst);
			        
			}
			_ctx.stop = _input.LT(-1);
			setState(214);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new List_cont_instructionContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_list_cont_instruction);
					setState(208);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(209);
					((List_cont_instructionContext)_localctx).cont = content_inst();

					                      ((List_cont_instructionContext)_localctx).sublista.lista.add(((List_cont_instructionContext)_localctx).cont.inst);
					                      ((List_cont_instructionContext)_localctx).lista =  ((List_cont_instructionContext)_localctx).sublista.lista;
					                  
					}
					} 
				}
				setState(216);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Content_instContext extends ParserRuleContext {
		public NodeInst inst;
		public Token PRINTLN;
		public List_expressionContext lista;
		public VariablesContext var;
		public DeclareVarsContext varParams;
		public AsignationContext asig;
		public Sentence_ifContext senteIf;
		public Sentence_doContext sentedo;
		public Sentence_do_whileContext senteDoWhile;
		public Sentence_arrayContext senteArray;
		public Sentence_asignacion_arrayContext senteAsignArray;
		public Sentence_asig_array_inicialContext senteAsignArrayInitial;
		public Call_functionContext func;
		public Sentence_allocateContext allow;
		public Sentence_deallocateContext deallow;
		public TerminalNode PRINTLN() { return getToken(Gramatica.PRINTLN, 0); }
		public TerminalNode MULTIPLICATION() { return getToken(Gramatica.MULTIPLICATION, 0); }
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public List_expressionContext list_expression() {
			return getRuleContext(List_expressionContext.class,0);
		}
		public VariablesContext variables() {
			return getRuleContext(VariablesContext.class,0);
		}
		public DeclareVarsContext declareVars() {
			return getRuleContext(DeclareVarsContext.class,0);
		}
		public AsignationContext asignation() {
			return getRuleContext(AsignationContext.class,0);
		}
		public Sentence_ifContext sentence_if() {
			return getRuleContext(Sentence_ifContext.class,0);
		}
		public Sentence_doContext sentence_do() {
			return getRuleContext(Sentence_doContext.class,0);
		}
		public Sentence_do_whileContext sentence_do_while() {
			return getRuleContext(Sentence_do_whileContext.class,0);
		}
		public Sentence_arrayContext sentence_array() {
			return getRuleContext(Sentence_arrayContext.class,0);
		}
		public Sentence_asignacion_arrayContext sentence_asignacion_array() {
			return getRuleContext(Sentence_asignacion_arrayContext.class,0);
		}
		public Sentence_asig_array_inicialContext sentence_asig_array_inicial() {
			return getRuleContext(Sentence_asig_array_inicialContext.class,0);
		}
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public Sentence_allocateContext sentence_allocate() {
			return getRuleContext(Sentence_allocateContext.class,0);
		}
		public Sentence_deallocateContext sentence_deallocate() {
			return getRuleContext(Sentence_deallocateContext.class,0);
		}
		public Content_instContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_content_inst; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterContent_inst(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitContent_inst(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitContent_inst(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Content_instContext content_inst() throws RecognitionException {
		Content_instContext _localctx = new Content_instContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_content_inst);
		try {
			setState(259);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(217);
				((Content_instContext)_localctx).PRINTLN = match(PRINTLN);
				setState(218);
				match(MULTIPLICATION);
				setState(219);
				match(COMMA);
				setState(220);
				((Content_instContext)_localctx).lista = list_expression(0);

				            ((Content_instContext)_localctx).inst =  new PrintInstruction(((Content_instContext)_localctx).lista.lista, ((Content_instContext)_localctx).PRINTLN.getLine(), ((Content_instContext)_localctx).PRINTLN.getCharPositionInLine());;
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(223);
				((Content_instContext)_localctx).var = variables();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).var.inst;
				         
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(226);
				((Content_instContext)_localctx).varParams = declareVars();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).varParams.inst;
				         
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(229);
				((Content_instContext)_localctx).asig = asignation();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).asig.inst;
				        
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(232);
				((Content_instContext)_localctx).senteIf = sentence_if();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).senteIf.inst;
				        
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(235);
				((Content_instContext)_localctx).sentedo = sentence_do();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).sentedo.inst;
				        
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(238);
				((Content_instContext)_localctx).senteDoWhile = sentence_do_while();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).senteDoWhile.inst;
				        
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(241);
				((Content_instContext)_localctx).senteArray = sentence_array();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).senteArray.inst;
				        
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(244);
				((Content_instContext)_localctx).senteAsignArray = sentence_asignacion_array();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).senteAsignArray.inst;
				        
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(247);
				((Content_instContext)_localctx).senteAsignArrayInitial = sentence_asig_array_inicial();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).senteAsignArrayInitial.inst;
				        
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(250);
				((Content_instContext)_localctx).func = call_function();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).func.inst;
				        
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(253);
				((Content_instContext)_localctx).allow = sentence_allocate();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).allow.inst;
				        
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(256);
				((Content_instContext)_localctx).deallow = sentence_deallocate();

				            ((Content_instContext)_localctx).inst =  ((Content_instContext)_localctx).deallow.inst;
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignationContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public ExpressionContext expr;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode EQUAL() { return getToken(Gramatica.EQUAL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AsignationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAsignation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAsignation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAsignation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsignationContext asignation() throws RecognitionException {
		AsignationContext _localctx = new AsignationContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_asignation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			((AsignationContext)_localctx).ID = match(ID);
			setState(262);
			match(EQUAL);
			setState(263);
			((AsignationContext)_localctx).expr = expression();

			          ((AsignationContext)_localctx).inst =  new Assignment((((AsignationContext)_localctx).ID!=null?((AsignationContext)_localctx).ID.getText():null), ((AsignationContext)_localctx).expr.inst, ((AsignationContext)_localctx).ID.getLine(), ((AsignationContext)_localctx).ID.getCharPositionInLine());
			      
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariablesContext extends ParserRuleContext {
		public NodeInst inst;
		public Type_varContext type;
		public Token TWOPOINTS;
		public List_variableContext lista;
		public TerminalNode TWOPOINTS() { return getToken(Gramatica.TWOPOINTS, 0); }
		public Type_varContext type_var() {
			return getRuleContext(Type_varContext.class,0);
		}
		public List_variableContext list_variable() {
			return getRuleContext(List_variableContext.class,0);
		}
		public VariablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variables; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterVariables(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitVariables(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitVariables(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariablesContext variables() throws RecognitionException {
		VariablesContext _localctx = new VariablesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_variables);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266);
			((VariablesContext)_localctx).type = type_var();
			setState(267);
			((VariablesContext)_localctx).TWOPOINTS = match(TWOPOINTS);
			setState(268);
			((VariablesContext)_localctx).lista = list_variable(0);

			            ((VariablesContext)_localctx).inst =  new Declaracion(((VariablesContext)_localctx).lista.lista, ((VariablesContext)_localctx).type.tipo, ((VariablesContext)_localctx).TWOPOINTS.getLine(), ((VariablesContext)_localctx).TWOPOINTS.getCharPositionInLine());;
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclareVarsContext extends ParserRuleContext {
		public NodeInst inst;
		public Type_varContext type;
		public Token INTENT;
		public Type_declare_varContext type_in;
		public List_variableContext lista;
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public TerminalNode INTENT() { return getToken(Gramatica.INTENT, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public TerminalNode TWOPOINTS() { return getToken(Gramatica.TWOPOINTS, 0); }
		public Type_varContext type_var() {
			return getRuleContext(Type_varContext.class,0);
		}
		public Type_declare_varContext type_declare_var() {
			return getRuleContext(Type_declare_varContext.class,0);
		}
		public List_variableContext list_variable() {
			return getRuleContext(List_variableContext.class,0);
		}
		public DeclareVarsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declareVars; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclareVars(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclareVars(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclareVars(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareVarsContext declareVars() throws RecognitionException {
		DeclareVarsContext _localctx = new DeclareVarsContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_declareVars);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(271);
			((DeclareVarsContext)_localctx).type = type_var();
			setState(272);
			match(COMMA);
			setState(273);
			((DeclareVarsContext)_localctx).INTENT = match(INTENT);
			setState(274);
			match(LP);
			setState(275);
			((DeclareVarsContext)_localctx).type_in = type_declare_var();
			setState(276);
			match(RP);
			setState(277);
			match(TWOPOINTS);
			setState(278);
			((DeclareVarsContext)_localctx).lista = list_variable(0);

			            ((DeclareVarsContext)_localctx).inst =  new ParamsFunction(((DeclareVarsContext)_localctx).lista.lista, ((DeclareVarsContext)_localctx).type.tipo, ((DeclareVarsContext)_localctx).type_in.tipo, ((DeclareVarsContext)_localctx).INTENT.getLine(), ((DeclareVarsContext)_localctx).INTENT.getCharPositionInLine());
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_declare_varContext extends ParserRuleContext {
		public Tipo tipo;
		public TerminalNode IN() { return getToken(Gramatica.IN, 0); }
		public TerminalNode OUT() { return getToken(Gramatica.OUT, 0); }
		public TerminalNode INOUT() { return getToken(Gramatica.INOUT, 0); }
		public Type_declare_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_declare_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterType_declare_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitType_declare_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitType_declare_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_declare_varContext type_declare_var() throws RecognitionException {
		Type_declare_varContext _localctx = new Type_declare_varContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_type_declare_var);
		try {
			setState(287);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IN:
				enterOuterAlt(_localctx, 1);
				{
				setState(281);
				match(IN);

				            ((Type_declare_varContext)_localctx).tipo =  Tipo.IN;
				        
				}
				break;
			case OUT:
				enterOuterAlt(_localctx, 2);
				{
				setState(283);
				match(OUT);

				            ((Type_declare_varContext)_localctx).tipo =  Tipo.OUT;
				        
				}
				break;
			case INOUT:
				enterOuterAlt(_localctx, 3);
				{
				setState(285);
				match(INOUT);

				            ((Type_declare_varContext)_localctx).tipo =  Tipo.INOUT;
				        
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_varContext extends ParserRuleContext {
		public Tipo tipo;
		public TerminalNode INTEGER() { return getToken(Gramatica.INTEGER, 0); }
		public TerminalNode REAL() { return getToken(Gramatica.REAL, 0); }
		public TerminalNode COMPLEX() { return getToken(Gramatica.COMPLEX, 0); }
		public TerminalNode CHARACTER() { return getToken(Gramatica.CHARACTER, 0); }
		public TerminalNode LOGICAL() { return getToken(Gramatica.LOGICAL, 0); }
		public Type_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterType_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitType_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitType_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_varContext type_var() throws RecognitionException {
		Type_varContext _localctx = new Type_varContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_type_var);
		try {
			setState(299);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
				enterOuterAlt(_localctx, 1);
				{
				setState(289);
				match(INTEGER);

				                ((Type_varContext)_localctx).tipo =  Tipo.ENTERO;
				            
				}
				break;
			case REAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(291);
				match(REAL);

				                ((Type_varContext)_localctx).tipo =  Tipo.REAL;
				            
				}
				break;
			case COMPLEX:
				enterOuterAlt(_localctx, 3);
				{
				setState(293);
				match(COMPLEX);

				                ((Type_varContext)_localctx).tipo =  Tipo.COMPLEX;
				            
				}
				break;
			case CHARACTER:
				enterOuterAlt(_localctx, 4);
				{
				setState(295);
				match(CHARACTER);

				                ((Type_varContext)_localctx).tipo =  Tipo.CHARACTER;
				            
				}
				break;
			case LOGICAL:
				enterOuterAlt(_localctx, 5);
				{
				setState(297);
				match(LOGICAL);

				                ((Type_varContext)_localctx).tipo =  Tipo.LOGICAL;
				            
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_variableContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_variableContext sublista;
		public VariableContext var;
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public List_variableContext list_variable() {
			return getRuleContext(List_variableContext.class,0);
		}
		public List_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_variableContext list_variable() throws RecognitionException {
		return list_variable(0);
	}

	private List_variableContext list_variable(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		List_variableContext _localctx = new List_variableContext(_ctx, _parentState);
		List_variableContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_list_variable, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((List_variableContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(302);
			((List_variableContext)_localctx).var = variable();

			            _localctx.lista.add(((List_variableContext)_localctx).var.inst);
			        
			}
			_ctx.stop = _input.LT(-1);
			setState(312);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new List_variableContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_list_variable);
					setState(305);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(306);
					match(COMMA);
					setState(307);
					((List_variableContext)_localctx).var = variable();

					                      ((List_variableContext)_localctx).sublista.lista.add(((List_variableContext)_localctx).var.inst);
					                      ((List_variableContext)_localctx).lista =  ((List_variableContext)_localctx).sublista.lista;
					                  
					}
					} 
				}
				setState(314);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public ExpressionContext expr;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode EQUAL() { return getToken(Gramatica.EQUAL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_variable);
		try {
			setState(322);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(315);
				((VariableContext)_localctx).ID = match(ID);

				                ((VariableContext)_localctx).inst =  new Identifier((((VariableContext)_localctx).ID!=null?((VariableContext)_localctx).ID.getText():null), ((VariableContext)_localctx).ID.getLine(), ((VariableContext)_localctx).ID.getCharPositionInLine());
				            
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(317);
				((VariableContext)_localctx).ID = match(ID);
				setState(318);
				match(EQUAL);
				setState(319);
				((VariableContext)_localctx).expr = expression();

				                ((VariableContext)_localctx).inst =  new Assignment((((VariableContext)_localctx).ID!=null?((VariableContext)_localctx).ID.getText():null), ((VariableContext)_localctx).expr.inst, ((VariableContext)_localctx).ID.getLine(), ((VariableContext)_localctx).ID.getCharPositionInLine());
				            
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_ifContext extends ParserRuleContext {
		public NodeInst inst;
		public Token IF;
		public ExpressionContext expr;
		public List_cont_instructionContext list;
		public List_cont_instructionContext listelse;
		public List_IfContext sIf;
		public List<TerminalNode> IF() { return getTokens(Gramatica.IF); }
		public TerminalNode IF(int i) {
			return getToken(Gramatica.IF, i);
		}
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public TerminalNode THEN() { return getToken(Gramatica.THEN, 0); }
		public TerminalNode END() { return getToken(Gramatica.END, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<List_cont_instructionContext> list_cont_instruction() {
			return getRuleContexts(List_cont_instructionContext.class);
		}
		public List_cont_instructionContext list_cont_instruction(int i) {
			return getRuleContext(List_cont_instructionContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(Gramatica.ELSE, 0); }
		public List_IfContext list_If() {
			return getRuleContext(List_IfContext.class,0);
		}
		public Sentence_ifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_if; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_if(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_if(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_if(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_ifContext sentence_if() throws RecognitionException {
		Sentence_ifContext _localctx = new Sentence_ifContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_sentence_if);
		try {
			setState(375);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(324);
				((Sentence_ifContext)_localctx).IF = match(IF);
				setState(325);
				match(LP);
				setState(326);
				((Sentence_ifContext)_localctx).expr = expression();
				setState(327);
				match(RP);
				setState(328);
				match(THEN);
				setState(329);
				((Sentence_ifContext)_localctx).list = list_cont_instruction(0);
				setState(330);
				match(END);
				setState(331);
				((Sentence_ifContext)_localctx).IF = match(IF);

				        ((Sentence_ifContext)_localctx).inst =  new If(((Sentence_ifContext)_localctx).expr.inst, ((Sentence_ifContext)_localctx).list.lista, null, ((Sentence_ifContext)_localctx).IF.getLine(), ((Sentence_ifContext)_localctx).IF.getCharPositionInLine());
				    
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(334);
				match(IF);
				setState(335);
				match(LP);
				setState(336);
				((Sentence_ifContext)_localctx).expr = expression();
				setState(337);
				match(RP);
				setState(338);
				match(THEN);
				setState(339);
				match(END);
				setState(340);
				match(IF);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(342);
				((Sentence_ifContext)_localctx).IF = match(IF);
				setState(343);
				match(LP);
				setState(344);
				((Sentence_ifContext)_localctx).expr = expression();
				setState(345);
				match(RP);
				setState(346);
				match(THEN);
				setState(347);
				((Sentence_ifContext)_localctx).list = list_cont_instruction(0);
				setState(348);
				match(ELSE);
				setState(349);
				((Sentence_ifContext)_localctx).listelse = list_cont_instruction(0);
				setState(350);
				match(END);
				setState(351);
				((Sentence_ifContext)_localctx).IF = match(IF);

				        ((Sentence_ifContext)_localctx).inst =  new If(((Sentence_ifContext)_localctx).expr.inst, ((Sentence_ifContext)_localctx).list.lista, ((Sentence_ifContext)_localctx).listelse.lista, ((Sentence_ifContext)_localctx).IF.getLine(), ((Sentence_ifContext)_localctx).IF.getCharPositionInLine());
				    
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(354);
				((Sentence_ifContext)_localctx).IF = match(IF);
				setState(355);
				match(LP);
				setState(356);
				((Sentence_ifContext)_localctx).expr = expression();
				setState(357);
				match(RP);
				setState(358);
				match(THEN);
				setState(359);
				((Sentence_ifContext)_localctx).list = list_cont_instruction(0);
				setState(360);
				match(ELSE);
				setState(361);
				((Sentence_ifContext)_localctx).sIf = list_If(0);

				                ((Sentence_ifContext)_localctx).inst =  new If(((Sentence_ifContext)_localctx).expr.inst, ((Sentence_ifContext)_localctx).list.lista, ((Sentence_ifContext)_localctx).sIf.lista, ((Sentence_ifContext)_localctx).IF.getLine(), ((Sentence_ifContext)_localctx).IF.getCharPositionInLine());
				    
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(364);
				((Sentence_ifContext)_localctx).IF = match(IF);
				setState(365);
				match(LP);
				setState(366);
				((Sentence_ifContext)_localctx).expr = expression();
				setState(367);
				match(RP);
				setState(368);
				match(THEN);
				setState(369);
				match(ELSE);
				setState(370);
				((Sentence_ifContext)_localctx).listelse = list_cont_instruction(0);
				setState(371);
				match(END);
				setState(372);
				((Sentence_ifContext)_localctx).IF = match(IF);

				            ((Sentence_ifContext)_localctx).inst =  new If(((Sentence_ifContext)_localctx).expr.inst, null, ((Sentence_ifContext)_localctx).listelse.lista, ((Sentence_ifContext)_localctx).IF.getLine(), ((Sentence_ifContext)_localctx).IF.getCharPositionInLine());
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_IfContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_IfContext sublista;
		public Sentence_ifContext sIf;
		public Sentence_ifContext sentence_if() {
			return getRuleContext(Sentence_ifContext.class,0);
		}
		public List_IfContext list_If() {
			return getRuleContext(List_IfContext.class,0);
		}
		public List_IfContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_If; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_If(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_If(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_If(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_IfContext list_If() throws RecognitionException {
		return list_If(0);
	}

	private List_IfContext list_If(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		List_IfContext _localctx = new List_IfContext(_ctx, _parentState);
		List_IfContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_list_If, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((List_IfContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(378);
			((List_IfContext)_localctx).sIf = sentence_if();

			            _localctx.lista.add(((List_IfContext)_localctx).sIf.inst);
			        
			}
			_ctx.stop = _input.LT(-1);
			setState(387);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new List_IfContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_list_If);
					setState(381);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(382);
					((List_IfContext)_localctx).sIf = sentence_if();

					                      ((List_IfContext)_localctx).sublista.lista.add(((List_IfContext)_localctx).sIf.inst);
					                      ((List_IfContext)_localctx).lista =  ((List_IfContext)_localctx).sublista.lista;
					                  
					}
					} 
				}
				setState(389);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Sentence_doContext extends ParserRuleContext {
		public NodeInst inst;
		public Token DO;
		public Token var;
		public ExpressionContext initialDo;
		public ExpressionContext endDo;
		public ExpressionContext stepDo;
		public List_cont_instructionContext list;
		public List<TerminalNode> DO() { return getTokens(Gramatica.DO); }
		public TerminalNode DO(int i) {
			return getToken(Gramatica.DO, i);
		}
		public TerminalNode EQUAL() { return getToken(Gramatica.EQUAL, 0); }
		public List<TerminalNode> COMMA() { return getTokens(Gramatica.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(Gramatica.COMMA, i);
		}
		public TerminalNode END() { return getToken(Gramatica.END, 0); }
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List_cont_instructionContext list_cont_instruction() {
			return getRuleContext(List_cont_instructionContext.class,0);
		}
		public Sentence_doContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_do; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_do(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_do(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_do(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_doContext sentence_do() throws RecognitionException {
		Sentence_doContext _localctx = new Sentence_doContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_sentence_do);
		try {
			setState(414);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(390);
				((Sentence_doContext)_localctx).DO = match(DO);
				setState(391);
				((Sentence_doContext)_localctx).var = match(ID);
				setState(392);
				match(EQUAL);
				setState(393);
				((Sentence_doContext)_localctx).initialDo = expression();
				setState(394);
				match(COMMA);
				setState(395);
				((Sentence_doContext)_localctx).endDo = expression();
				setState(396);
				match(COMMA);
				setState(397);
				((Sentence_doContext)_localctx).stepDo = expression();
				setState(398);
				((Sentence_doContext)_localctx).list = list_cont_instruction(0);
				setState(399);
				match(END);
				setState(400);
				((Sentence_doContext)_localctx).DO = match(DO);

				                ((Sentence_doContext)_localctx).inst =  new For(new Assignment((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), ((Sentence_doContext)_localctx).initialDo.inst, ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()),
				                                       new Operations(
				                                       new Primitives((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), Tipo.IDENTIFICADOR),((Sentence_doContext)_localctx).endDo.inst,"<=", ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()),
				                                       new Assignment(
				                                               (((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null),
				                                               new Operations(
				                                                   new Primitives((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), Tipo.IDENTIFICADOR),((Sentence_doContext)_localctx).stepDo.inst,"+", ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                               )
				                                               , ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                           )
				                                   , ((Sentence_doContext)_localctx).list.lista, ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                   );
				            
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(403);
				((Sentence_doContext)_localctx).DO = match(DO);
				setState(404);
				((Sentence_doContext)_localctx).var = match(ID);
				setState(405);
				match(EQUAL);
				setState(406);
				((Sentence_doContext)_localctx).initialDo = expression();
				setState(407);
				match(COMMA);
				setState(408);
				((Sentence_doContext)_localctx).endDo = expression();
				setState(409);
				((Sentence_doContext)_localctx).list = list_cont_instruction(0);
				setState(410);
				match(END);
				setState(411);
				((Sentence_doContext)_localctx).DO = match(DO);

				                      ((Sentence_doContext)_localctx).inst =  new For(new Assignment((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), ((Sentence_doContext)_localctx).initialDo.inst, ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()),
				                                                             new Operations(
				                                                             new Primitives((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), Tipo.IDENTIFICADOR),((Sentence_doContext)_localctx).endDo.inst,"<=", ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()),
				                                                             new Assignment(
				                                                                     (((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null),
				                                                                     new Operations(
				                                                                         new Primitives((((Sentence_doContext)_localctx).var!=null?((Sentence_doContext)_localctx).var.getText():null), Tipo.IDENTIFICADOR), new Primitives("1", Tipo.ENTERO),"+", ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                                                     )
				                                                                     , ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                                                 )
				                                                         , ((Sentence_doContext)_localctx).list.lista, ((Sentence_doContext)_localctx).DO.getLine(), ((Sentence_doContext)_localctx).DO.getCharPositionInLine()
				                                                         );
				                  
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_do_whileContext extends ParserRuleContext {
		public NodeInst inst;
		public Token DO;
		public ExpressionContext instr;
		public List_cont_instructionContext list;
		public List<TerminalNode> DO() { return getTokens(Gramatica.DO); }
		public TerminalNode DO(int i) {
			return getToken(Gramatica.DO, i);
		}
		public TerminalNode WHILE() { return getToken(Gramatica.WHILE, 0); }
		public TerminalNode END() { return getToken(Gramatica.END, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List_cont_instructionContext list_cont_instruction() {
			return getRuleContext(List_cont_instructionContext.class,0);
		}
		public Sentence_do_whileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_do_while; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_do_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_do_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_do_while(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_do_whileContext sentence_do_while() throws RecognitionException {
		Sentence_do_whileContext _localctx = new Sentence_do_whileContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_sentence_do_while);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(416);
			((Sentence_do_whileContext)_localctx).DO = match(DO);
			setState(417);
			match(WHILE);
			setState(418);
			((Sentence_do_whileContext)_localctx).instr = expression();
			setState(419);
			((Sentence_do_whileContext)_localctx).list = list_cont_instruction(0);
			setState(420);
			match(END);
			setState(421);
			((Sentence_do_whileContext)_localctx).DO = match(DO);

			                ((Sentence_do_whileContext)_localctx).inst =  new DoWhile(((Sentence_do_whileContext)_localctx).instr.inst, ((Sentence_do_whileContext)_localctx).list.lista, ((Sentence_do_whileContext)_localctx).DO.getLine(), ((Sentence_do_whileContext)_localctx).DO.getCharPositionInLine());
			            
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_arrayContext extends ParserRuleContext {
		public NodeInst inst;
		public Type_varContext type;
		public Dimension_arrayContext dimension;
		public Token ID;
		public Dimension_array_dinamycContext dinamycDimension;
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public TerminalNode DIMENSION() { return getToken(Gramatica.DIMENSION, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public TerminalNode TWOPOINTS() { return getToken(Gramatica.TWOPOINTS, 0); }
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public Type_varContext type_var() {
			return getRuleContext(Type_varContext.class,0);
		}
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public TerminalNode ALLOCATABLE() { return getToken(Gramatica.ALLOCATABLE, 0); }
		public Dimension_array_dinamycContext dimension_array_dinamyc() {
			return getRuleContext(Dimension_array_dinamycContext.class,0);
		}
		public Sentence_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_arrayContext sentence_array() throws RecognitionException {
		Sentence_arrayContext _localctx = new Sentence_arrayContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_sentence_array);
		try {
			setState(444);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(424);
				((Sentence_arrayContext)_localctx).type = type_var();
				setState(425);
				match(COMMA);
				setState(426);
				match(DIMENSION);
				setState(427);
				match(LP);
				setState(428);
				((Sentence_arrayContext)_localctx).dimension = dimension_array(0);
				setState(429);
				match(RP);
				setState(430);
				match(TWOPOINTS);
				setState(431);
				((Sentence_arrayContext)_localctx).ID = match(ID);

				        ((Sentence_arrayContext)_localctx).inst =  new DeclarationArray((((Sentence_arrayContext)_localctx).ID!=null?((Sentence_arrayContext)_localctx).ID.getText():null), ((Sentence_arrayContext)_localctx).type.tipo, ((Sentence_arrayContext)_localctx).dimension.lista, ((Sentence_arrayContext)_localctx).ID.getLine(), ((Sentence_arrayContext)_localctx).ID.getCharPositionInLine());
				    
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(434);
				((Sentence_arrayContext)_localctx).type = type_var();
				setState(435);
				match(COMMA);
				setState(436);
				match(ALLOCATABLE);
				setState(437);
				match(TWOPOINTS);
				setState(438);
				((Sentence_arrayContext)_localctx).ID = match(ID);
				setState(439);
				match(LP);
				setState(440);
				((Sentence_arrayContext)_localctx).dinamycDimension = dimension_array_dinamyc(0);
				setState(441);
				match(RP);

				        ((Sentence_arrayContext)_localctx).inst =  new DeclarationArray((((Sentence_arrayContext)_localctx).ID!=null?((Sentence_arrayContext)_localctx).ID.getText():null), ((Sentence_arrayContext)_localctx).type.tipo, ((Sentence_arrayContext)_localctx).dinamycDimension.lista, ((Sentence_arrayContext)_localctx).ID.getLine(), ((Sentence_arrayContext)_localctx).ID.getCharPositionInLine());
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_asignacion_arrayContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public Dimension_arrayContext dimension;
		public ExpressionContext exp;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode CORCHL() { return getToken(Gramatica.CORCHL, 0); }
		public TerminalNode CORCHR() { return getToken(Gramatica.CORCHR, 0); }
		public TerminalNode EQUAL() { return getToken(Gramatica.EQUAL, 0); }
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Sentence_asignacion_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_asignacion_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_asignacion_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_asignacion_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_asignacion_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_asignacion_arrayContext sentence_asignacion_array() throws RecognitionException {
		Sentence_asignacion_arrayContext _localctx = new Sentence_asignacion_arrayContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_sentence_asignacion_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			((Sentence_asignacion_arrayContext)_localctx).ID = match(ID);
			setState(447);
			match(CORCHL);
			setState(448);
			((Sentence_asignacion_arrayContext)_localctx).dimension = dimension_array(0);
			setState(449);
			match(CORCHR);
			setState(450);
			match(EQUAL);
			setState(451);
			((Sentence_asignacion_arrayContext)_localctx).exp = expression();

			        ((Sentence_asignacion_arrayContext)_localctx).inst =  new AsignArray((((Sentence_asignacion_arrayContext)_localctx).ID!=null?((Sentence_asignacion_arrayContext)_localctx).ID.getText():null), ((Sentence_asignacion_arrayContext)_localctx).exp.inst, ((Sentence_asignacion_arrayContext)_localctx).dimension.lista, ((Sentence_asignacion_arrayContext)_localctx).ID.getLine(), ((Sentence_asignacion_arrayContext)_localctx).ID.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_asig_array_inicialContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public Dimension_arrayContext dimension;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode EQUAL() { return getToken(Gramatica.EQUAL, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public List<TerminalNode> DIVISION() { return getTokens(Gramatica.DIVISION); }
		public TerminalNode DIVISION(int i) {
			return getToken(Gramatica.DIVISION, i);
		}
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public Sentence_asig_array_inicialContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_asig_array_inicial; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_asig_array_inicial(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_asig_array_inicial(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_asig_array_inicial(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_asig_array_inicialContext sentence_asig_array_inicial() throws RecognitionException {
		Sentence_asig_array_inicialContext _localctx = new Sentence_asig_array_inicialContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_sentence_asig_array_inicial);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(454);
			((Sentence_asig_array_inicialContext)_localctx).ID = match(ID);
			setState(455);
			match(EQUAL);
			setState(456);
			match(LP);
			setState(457);
			match(DIVISION);
			setState(458);
			((Sentence_asig_array_inicialContext)_localctx).dimension = dimension_array(0);
			setState(459);
			match(DIVISION);
			setState(460);
			match(RP);

			        ((Sentence_asig_array_inicialContext)_localctx).inst =  new AsignArray((((Sentence_asig_array_inicialContext)_localctx).ID!=null?((Sentence_asig_array_inicialContext)_localctx).ID.getText():null), ((Sentence_asig_array_inicialContext)_localctx).dimension.lista, ((Sentence_asig_array_inicialContext)_localctx).ID.getLine(), ((Sentence_asig_array_inicialContext)_localctx).ID.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_allocateContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public Dimension_arrayContext dimension;
		public TerminalNode ALLOCATE() { return getToken(Gramatica.ALLOCATE, 0); }
		public List<TerminalNode> LP() { return getTokens(Gramatica.LP); }
		public TerminalNode LP(int i) {
			return getToken(Gramatica.LP, i);
		}
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public List<TerminalNode> RP() { return getTokens(Gramatica.RP); }
		public TerminalNode RP(int i) {
			return getToken(Gramatica.RP, i);
		}
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public Sentence_allocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_allocate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_allocate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_allocate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_allocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_allocateContext sentence_allocate() throws RecognitionException {
		Sentence_allocateContext _localctx = new Sentence_allocateContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_sentence_allocate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(463);
			match(ALLOCATE);
			setState(464);
			match(LP);
			setState(465);
			((Sentence_allocateContext)_localctx).ID = match(ID);
			setState(466);
			match(LP);
			setState(467);
			((Sentence_allocateContext)_localctx).dimension = dimension_array(0);
			setState(468);
			match(RP);
			setState(469);
			match(RP);

			        ((Sentence_allocateContext)_localctx).inst =  new Allocate((((Sentence_allocateContext)_localctx).ID!=null?((Sentence_allocateContext)_localctx).ID.getText():null), ((Sentence_allocateContext)_localctx).dimension.lista, ((Sentence_allocateContext)_localctx).ID.getLine(), ((Sentence_allocateContext)_localctx).ID.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_deallocateContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public TerminalNode DEALLOCATE() { return getToken(Gramatica.DEALLOCATE, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public Sentence_deallocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_deallocate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSentence_deallocate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSentence_deallocate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSentence_deallocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sentence_deallocateContext sentence_deallocate() throws RecognitionException {
		Sentence_deallocateContext _localctx = new Sentence_deallocateContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_sentence_deallocate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(472);
			match(DEALLOCATE);
			setState(473);
			match(LP);
			setState(474);
			((Sentence_deallocateContext)_localctx).ID = match(ID);
			setState(475);
			match(RP);

			        ((Sentence_deallocateContext)_localctx).inst =  new Deallocate((((Sentence_deallocateContext)_localctx).ID!=null?((Sentence_deallocateContext)_localctx).ID.getText():null), ((Sentence_deallocateContext)_localctx).ID.getLine(), ((Sentence_deallocateContext)_localctx).ID.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Dimension_arrayContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public Dimension_arrayContext sublista;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public Dimension_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimension_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDimension_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDimension_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDimension_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dimension_arrayContext dimension_array() throws RecognitionException {
		return dimension_array(0);
	}

	private Dimension_arrayContext dimension_array(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Dimension_arrayContext _localctx = new Dimension_arrayContext(_ctx, _parentState);
		Dimension_arrayContext _prevctx = _localctx;
		int _startState = 52;
		enterRecursionRule(_localctx, 52, RULE_dimension_array, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((Dimension_arrayContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(479);
			((Dimension_arrayContext)_localctx).expr = expression();

			        _localctx.lista.add(((Dimension_arrayContext)_localctx).expr.inst);
			    
			}
			_ctx.stop = _input.LT(-1);
			setState(489);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Dimension_arrayContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_dimension_array);
					setState(482);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(483);
					match(COMMA);
					setState(484);
					((Dimension_arrayContext)_localctx).expr = expression();

					                  ((Dimension_arrayContext)_localctx).sublista.lista.add(((Dimension_arrayContext)_localctx).expr.inst);
					                  ((Dimension_arrayContext)_localctx).lista =  ((Dimension_arrayContext)_localctx).sublista.lista;
					              
					}
					} 
				}
				setState(491);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Dimension_array_dinamycContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public Dimension_array_dinamycContext sublista;
		public TerminalNode TWOPOINT() { return getToken(Gramatica.TWOPOINT, 0); }
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public Dimension_array_dinamycContext dimension_array_dinamyc() {
			return getRuleContext(Dimension_array_dinamycContext.class,0);
		}
		public Dimension_array_dinamycContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimension_array_dinamyc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDimension_array_dinamyc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDimension_array_dinamyc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDimension_array_dinamyc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Dimension_array_dinamycContext dimension_array_dinamyc() throws RecognitionException {
		return dimension_array_dinamyc(0);
	}

	private Dimension_array_dinamycContext dimension_array_dinamyc(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Dimension_array_dinamycContext _localctx = new Dimension_array_dinamycContext(_ctx, _parentState);
		Dimension_array_dinamycContext _prevctx = _localctx;
		int _startState = 54;
		enterRecursionRule(_localctx, 54, RULE_dimension_array_dinamyc, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((Dimension_array_dinamycContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(493);
			match(TWOPOINT);

			        _localctx.lista.add(new Primitives(0, Tipo.ENTERO));
			    
			}
			_ctx.stop = _input.LT(-1);
			setState(502);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Dimension_array_dinamycContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_dimension_array_dinamyc);
					setState(496);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(497);
					match(COMMA);
					setState(498);
					match(TWOPOINT);

					                  ((Dimension_array_dinamycContext)_localctx).sublista.lista.add(new Primitives(0, Tipo.ENTERO));
					                  ((Dimension_array_dinamycContext)_localctx).lista =  ((Dimension_array_dinamycContext)_localctx).sublista.lista;
					              
					}
					} 
				}
				setState(504);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Access_arrayContext extends ParserRuleContext {
		public NodeInst inst;
		public Token ID;
		public Dimension_arrayContext dim;
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode CORCHL() { return getToken(Gramatica.CORCHL, 0); }
		public TerminalNode CORCHR() { return getToken(Gramatica.CORCHR, 0); }
		public Dimension_arrayContext dimension_array() {
			return getRuleContext(Dimension_arrayContext.class,0);
		}
		public Access_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_access_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAccess_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAccess_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAccess_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Access_arrayContext access_array() throws RecognitionException {
		Access_arrayContext _localctx = new Access_arrayContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_access_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(505);
			((Access_arrayContext)_localctx).ID = match(ID);
			setState(506);
			match(CORCHL);
			setState(507);
			((Access_arrayContext)_localctx).dim = dimension_array(0);
			setState(508);
			match(CORCHR);

			        ((Access_arrayContext)_localctx).inst =  new AccessArray((((Access_arrayContext)_localctx).ID!=null?((Access_arrayContext)_localctx).ID.getText():null), ((Access_arrayContext)_localctx).dim.lista, ((Access_arrayContext)_localctx).ID.getLine(), ((Access_arrayContext)_localctx).ID.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_expressionContext extends ParserRuleContext {
		public LinkedList<NodeInst> lista;
		public List_expressionContext sublista;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(Gramatica.COMMA, 0); }
		public List_expressionContext list_expression() {
			return getRuleContext(List_expressionContext.class,0);
		}
		public List_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_expressionContext list_expression() throws RecognitionException {
		return list_expression(0);
	}

	private List_expressionContext list_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		List_expressionContext _localctx = new List_expressionContext(_ctx, _parentState);
		List_expressionContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, 58, RULE_list_expression, _p);
		 LinkedList<NodeInst> a = new LinkedList<>(); ((List_expressionContext)_localctx).lista =  a; 
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(512);
			((List_expressionContext)_localctx).expr = expression();

			            _localctx.lista.add(((List_expressionContext)_localctx).expr.inst);
			        
			}
			_ctx.stop = _input.LT(-1);
			setState(522);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new List_expressionContext(_parentctx, _parentState);
					_localctx.sublista = _prevctx;
					_localctx.sublista = _prevctx;
					pushNewRecursionContext(_localctx, _startState, RULE_list_expression);
					setState(515);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(516);
					match(COMMA);
					setState(517);
					((List_expressionContext)_localctx).expr = expression();

					                      ((List_expressionContext)_localctx).sublista.lista.add(((List_expressionContext)_localctx).expr.inst);
					                      ((List_expressionContext)_localctx).lista =  ((List_expressionContext)_localctx).sublista.lista;
					                  
					}
					} 
				}
				setState(524);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public NodeInst inst;
		public Expression_arithmeticContext exp;
		public Expression_relationalContext exp_rel;
		public AsignationContext exp_asig;
		public Access_arrayContext accArray;
		public Expression_arithmeticContext expression_arithmetic() {
			return getRuleContext(Expression_arithmeticContext.class,0);
		}
		public Expression_relationalContext expression_relational() {
			return getRuleContext(Expression_relationalContext.class,0);
		}
		public AsignationContext asignation() {
			return getRuleContext(AsignationContext.class,0);
		}
		public Access_arrayContext access_array() {
			return getRuleContext(Access_arrayContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_expression);
		try {
			setState(537);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(525);
				((ExpressionContext)_localctx).exp = expression_arithmetic(0);

				            ((ExpressionContext)_localctx).inst =  ((ExpressionContext)_localctx).exp.inst;
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(528);
				((ExpressionContext)_localctx).exp_rel = expression_relational(0);

				            ((ExpressionContext)_localctx).inst =  ((ExpressionContext)_localctx).exp_rel.inst;
				        
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(531);
				((ExpressionContext)_localctx).exp_asig = asignation();

				            ((ExpressionContext)_localctx).inst =  ((ExpressionContext)_localctx).exp_asig.inst;
				        
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(534);
				((ExpressionContext)_localctx).accArray = access_array();

				            ((ExpressionContext)_localctx).inst =  ((ExpressionContext)_localctx).accArray.inst;
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_arithmeticContext extends ParserRuleContext {
		public NodeInst inst;
		public Expression_arithmeticContext opIz;
		public Token minus;
		public Expression_arithmeticContext opU;
		public Expression_valueContext expr_valor;
		public ExpressionContext exp;
		public Token op;
		public Expression_arithmeticContext opDe;
		public TerminalNode SUBSTACT() { return getToken(Gramatica.SUBSTACT, 0); }
		public List<Expression_arithmeticContext> expression_arithmetic() {
			return getRuleContexts(Expression_arithmeticContext.class);
		}
		public Expression_arithmeticContext expression_arithmetic(int i) {
			return getRuleContext(Expression_arithmeticContext.class,i);
		}
		public Expression_valueContext expression_value() {
			return getRuleContext(Expression_valueContext.class,0);
		}
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode POWER() { return getToken(Gramatica.POWER, 0); }
		public TerminalNode MULTIPLICATION() { return getToken(Gramatica.MULTIPLICATION, 0); }
		public TerminalNode DIVISION() { return getToken(Gramatica.DIVISION, 0); }
		public TerminalNode SUM() { return getToken(Gramatica.SUM, 0); }
		public Expression_arithmeticContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_arithmetic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterExpression_arithmetic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitExpression_arithmetic(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitExpression_arithmetic(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expression_arithmeticContext expression_arithmetic() throws RecognitionException {
		return expression_arithmetic(0);
	}

	private Expression_arithmeticContext expression_arithmetic(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expression_arithmeticContext _localctx = new Expression_arithmeticContext(_ctx, _parentState);
		Expression_arithmeticContext _prevctx = _localctx;
		int _startState = 62;
		enterRecursionRule(_localctx, 62, RULE_expression_arithmetic, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(552);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SUBSTACT:
				{
				setState(540);
				((Expression_arithmeticContext)_localctx).minus = match(SUBSTACT);
				setState(541);
				((Expression_arithmeticContext)_localctx).opU = expression_arithmetic(6);

				            ((Expression_arithmeticContext)_localctx).inst =  new Operations(((Expression_arithmeticContext)_localctx).opU.inst, Tipo_Operations.NEGATIVO, ((Expression_arithmeticContext)_localctx).minus.getLine(), ((Expression_arithmeticContext)_localctx).minus.getCharPositionInLine());
				        
				}
				break;
			case EXIT:
			case CYCLE:
			case SIZE:
			case CALL:
			case NUMBER:
			case FLOAT:
			case STRING:
			case STRING2:
			case TRUE:
			case FALSE:
			case ID:
				{
				setState(544);
				((Expression_arithmeticContext)_localctx).expr_valor = expression_value();

				            ((Expression_arithmeticContext)_localctx).inst =  ((Expression_arithmeticContext)_localctx).expr_valor.inst;
				        
				}
				break;
			case LP:
				{
				setState(547);
				match(LP);
				setState(548);
				((Expression_arithmeticContext)_localctx).exp = expression();
				setState(549);
				match(RP);

				            ((Expression_arithmeticContext)_localctx).inst =  ((Expression_arithmeticContext)_localctx).exp.inst;
				        
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(571);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(569);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
					case 1:
						{
						_localctx = new Expression_arithmeticContext(_parentctx, _parentState);
						_localctx.opIz = _prevctx;
						_localctx.opIz = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression_arithmetic);
						setState(554);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(555);
						((Expression_arithmeticContext)_localctx).op = match(POWER);
						setState(556);
						((Expression_arithmeticContext)_localctx).opDe = expression_arithmetic(6);

						                      ((Expression_arithmeticContext)_localctx).inst =  new Operations(((Expression_arithmeticContext)_localctx).opIz.inst,((Expression_arithmeticContext)_localctx).opDe.inst,(((Expression_arithmeticContext)_localctx).op!=null?((Expression_arithmeticContext)_localctx).op.getText():null), ((Expression_arithmeticContext)_localctx).op.getLine(), ((Expression_arithmeticContext)_localctx).op.getCharPositionInLine());
						                  
						}
						break;
					case 2:
						{
						_localctx = new Expression_arithmeticContext(_parentctx, _parentState);
						_localctx.opIz = _prevctx;
						_localctx.opIz = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression_arithmetic);
						setState(559);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(560);
						((Expression_arithmeticContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MULTIPLICATION || _la==DIVISION) ) {
							((Expression_arithmeticContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(561);
						((Expression_arithmeticContext)_localctx).opDe = expression_arithmetic(5);

						                      ((Expression_arithmeticContext)_localctx).inst =  new Operations(((Expression_arithmeticContext)_localctx).opIz.inst,((Expression_arithmeticContext)_localctx).opDe.inst,(((Expression_arithmeticContext)_localctx).op!=null?((Expression_arithmeticContext)_localctx).op.getText():null), ((Expression_arithmeticContext)_localctx).op.getLine(), ((Expression_arithmeticContext)_localctx).op.getCharPositionInLine());
						                  
						}
						break;
					case 3:
						{
						_localctx = new Expression_arithmeticContext(_parentctx, _parentState);
						_localctx.opIz = _prevctx;
						_localctx.opIz = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression_arithmetic);
						setState(564);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(565);
						((Expression_arithmeticContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==SUM || _la==SUBSTACT) ) {
							((Expression_arithmeticContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(566);
						((Expression_arithmeticContext)_localctx).opDe = expression_arithmetic(4);

						                      ((Expression_arithmeticContext)_localctx).inst =  new Operations(((Expression_arithmeticContext)_localctx).opIz.inst,((Expression_arithmeticContext)_localctx).opDe.inst,(((Expression_arithmeticContext)_localctx).op!=null?((Expression_arithmeticContext)_localctx).op.getText():null), ((Expression_arithmeticContext)_localctx).op.getLine(), ((Expression_arithmeticContext)_localctx).op.getCharPositionInLine());
						                  
						}
						break;
					}
					} 
				}
				setState(573);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Expression_relationalContext extends ParserRuleContext {
		public NodeInst inst;
		public Expression_relationalContext opIz;
		public Expression_relationalContext opIzC;
		public Token NOT;
		public Expression_relationalContext opDe;
		public Expression_arithmeticContext expr;
		public Token op;
		public Expression_relationalContext opDeC;
		public TerminalNode NOT() { return getToken(Gramatica.NOT, 0); }
		public List<Expression_relationalContext> expression_relational() {
			return getRuleContexts(Expression_relationalContext.class);
		}
		public Expression_relationalContext expression_relational(int i) {
			return getRuleContext(Expression_relationalContext.class,i);
		}
		public Expression_arithmeticContext expression_arithmetic() {
			return getRuleContext(Expression_arithmeticContext.class,0);
		}
		public TerminalNode EQUALS_ALTERNATIVE() { return getToken(Gramatica.EQUALS_ALTERNATIVE, 0); }
		public TerminalNode EQUALS() { return getToken(Gramatica.EQUALS, 0); }
		public TerminalNode INEQUALITY_ALTERNATIVE() { return getToken(Gramatica.INEQUALITY_ALTERNATIVE, 0); }
		public TerminalNode INEQUALITY() { return getToken(Gramatica.INEQUALITY, 0); }
		public TerminalNode GREATER_EQUAL_ALTERNATIVE() { return getToken(Gramatica.GREATER_EQUAL_ALTERNATIVE, 0); }
		public TerminalNode GREATER_EQUAL() { return getToken(Gramatica.GREATER_EQUAL, 0); }
		public TerminalNode LESS_EQUAL_ALTERNATIVE() { return getToken(Gramatica.LESS_EQUAL_ALTERNATIVE, 0); }
		public TerminalNode LESS_EQUAL() { return getToken(Gramatica.LESS_EQUAL, 0); }
		public TerminalNode LESS_THAN_ALTERNATIVE() { return getToken(Gramatica.LESS_THAN_ALTERNATIVE, 0); }
		public TerminalNode LESS_THAN() { return getToken(Gramatica.LESS_THAN, 0); }
		public TerminalNode GREATER_THAN_ALTERNATIVE() { return getToken(Gramatica.GREATER_THAN_ALTERNATIVE, 0); }
		public TerminalNode GREATER_THAN() { return getToken(Gramatica.GREATER_THAN, 0); }
		public TerminalNode AND() { return getToken(Gramatica.AND, 0); }
		public TerminalNode OR() { return getToken(Gramatica.OR, 0); }
		public Expression_relationalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_relational; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterExpression_relational(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitExpression_relational(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitExpression_relational(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expression_relationalContext expression_relational() throws RecognitionException {
		return expression_relational(0);
	}

	private Expression_relationalContext expression_relational(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expression_relationalContext _localctx = new Expression_relationalContext(_ctx, _parentState);
		Expression_relationalContext _prevctx = _localctx;
		int _startState = 64;
		enterRecursionRule(_localctx, 64, RULE_expression_relational, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(582);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT:
				{
				setState(575);
				((Expression_relationalContext)_localctx).NOT = match(NOT);
				setState(576);
				((Expression_relationalContext)_localctx).opDe = expression_relational(2);

				             ((Expression_relationalContext)_localctx).inst =  new Operations(((Expression_relationalContext)_localctx).opDe.inst,null,(((Expression_relationalContext)_localctx).NOT!=null?((Expression_relationalContext)_localctx).NOT.getText():null), ((Expression_relationalContext)_localctx).NOT.getLine(), ((Expression_relationalContext)_localctx).NOT.getCharPositionInLine());
				         
				}
				break;
			case LP:
			case EXIT:
			case CYCLE:
			case SUBSTACT:
			case SIZE:
			case CALL:
			case NUMBER:
			case FLOAT:
			case STRING:
			case STRING2:
			case TRUE:
			case FALSE:
			case ID:
				{
				setState(579);
				((Expression_relationalContext)_localctx).expr = expression_arithmetic(0);

				            ((Expression_relationalContext)_localctx).inst =  ((Expression_relationalContext)_localctx).expr.inst;
				        
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(596);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(594);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
					case 1:
						{
						_localctx = new Expression_relationalContext(_parentctx, _parentState);
						_localctx.opIz = _prevctx;
						_localctx.opIz = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression_relational);
						setState(584);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(585);
						((Expression_relationalContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQUALS) | (1L << EQUALS_ALTERNATIVE) | (1L << INEQUALITY) | (1L << INEQUALITY_ALTERNATIVE) | (1L << GREATER_THAN) | (1L << GREATER_THAN_ALTERNATIVE) | (1L << LESS_THAN) | (1L << LESS_THAN_ALTERNATIVE) | (1L << GREATER_EQUAL) | (1L << GREATER_EQUAL_ALTERNATIVE) | (1L << LESS_EQUAL) | (1L << LESS_EQUAL_ALTERNATIVE))) != 0)) ) {
							((Expression_relationalContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(586);
						((Expression_relationalContext)_localctx).opDe = expression_relational(5);

						                      ((Expression_relationalContext)_localctx).inst =  new Operations(((Expression_relationalContext)_localctx).opIz.inst,((Expression_relationalContext)_localctx).opDe.inst,(((Expression_relationalContext)_localctx).op!=null?((Expression_relationalContext)_localctx).op.getText():null), ((Expression_relationalContext)_localctx).op.getLine(), ((Expression_relationalContext)_localctx).op.getCharPositionInLine());
						                  
						}
						break;
					case 2:
						{
						_localctx = new Expression_relationalContext(_parentctx, _parentState);
						_localctx.opIzC = _prevctx;
						_localctx.opIzC = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression_relational);
						setState(589);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(590);
						((Expression_relationalContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==AND || _la==OR) ) {
							((Expression_relationalContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(591);
						((Expression_relationalContext)_localctx).opDeC = expression_relational(4);

						                        ((Expression_relationalContext)_localctx).inst =  new Operations(((Expression_relationalContext)_localctx).opIzC.inst,((Expression_relationalContext)_localctx).opDeC.inst,(((Expression_relationalContext)_localctx).op!=null?((Expression_relationalContext)_localctx).op.getText():null), ((Expression_relationalContext)_localctx).op.getLine(), ((Expression_relationalContext)_localctx).op.getCharPositionInLine());
						                    
						}
						break;
					}
					} 
				}
				setState(598);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Expression_valueContext extends ParserRuleContext {
		public NodeInst inst;
		public PrimitivoContext primitivo;
		public Access_arrayContext accArray;
		public Call_functionContext cFunction;
		public Sent_sizeContext senSize;
		public PrimitivoContext primitivo() {
			return getRuleContext(PrimitivoContext.class,0);
		}
		public Access_arrayContext access_array() {
			return getRuleContext(Access_arrayContext.class,0);
		}
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public Sent_sizeContext sent_size() {
			return getRuleContext(Sent_sizeContext.class,0);
		}
		public Expression_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterExpression_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitExpression_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitExpression_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expression_valueContext expression_value() throws RecognitionException {
		Expression_valueContext _localctx = new Expression_valueContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_expression_value);
		try {
			setState(611);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(599);
				((Expression_valueContext)_localctx).primitivo = primitivo();

				            ((Expression_valueContext)_localctx).inst =  ((Expression_valueContext)_localctx).primitivo.inst;
				        
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(602);
				((Expression_valueContext)_localctx).accArray = access_array();

				            ((Expression_valueContext)_localctx).inst =  ((Expression_valueContext)_localctx).accArray.inst;
				        
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(605);
				((Expression_valueContext)_localctx).cFunction = call_function();

				                 ((Expression_valueContext)_localctx).inst =  ((Expression_valueContext)_localctx).cFunction.inst;
				        
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(608);
				((Expression_valueContext)_localctx).senSize = sent_size();

				                 ((Expression_valueContext)_localctx).inst =  ((Expression_valueContext)_localctx).senSize.inst;
				        
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sent_sizeContext extends ParserRuleContext {
		public NodeInst inst;
		public Token SIZE;
		public Token ID;
		public TerminalNode SIZE() { return getToken(Gramatica.SIZE, 0); }
		public TerminalNode LP() { return getToken(Gramatica.LP, 0); }
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode RP() { return getToken(Gramatica.RP, 0); }
		public Sent_sizeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sent_size; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSent_size(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSent_size(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSent_size(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sent_sizeContext sent_size() throws RecognitionException {
		Sent_sizeContext _localctx = new Sent_sizeContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_sent_size);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(613);
			((Sent_sizeContext)_localctx).SIZE = match(SIZE);
			setState(614);
			match(LP);
			setState(615);
			((Sent_sizeContext)_localctx).ID = match(ID);
			setState(616);
			match(RP);

			        ((Sent_sizeContext)_localctx).inst =  new Size((((Sent_sizeContext)_localctx).ID!=null?((Sent_sizeContext)_localctx).ID.getText():null), ((Sent_sizeContext)_localctx).SIZE.getLine(), ((Sent_sizeContext)_localctx).SIZE.getCharPositionInLine());
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimitivoContext extends ParserRuleContext {
		public NodeInst inst;
		public Token NUMBER;
		public Token FLOAT;
		public Token TRUE;
		public Token FALSE;
		public Token ID;
		public Token STRING2;
		public Token STRING;
		public Token EXIT;
		public Token CYCLE;
		public TerminalNode NUMBER() { return getToken(Gramatica.NUMBER, 0); }
		public TerminalNode FLOAT() { return getToken(Gramatica.FLOAT, 0); }
		public TerminalNode TRUE() { return getToken(Gramatica.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(Gramatica.FALSE, 0); }
		public TerminalNode ID() { return getToken(Gramatica.ID, 0); }
		public TerminalNode STRING2() { return getToken(Gramatica.STRING2, 0); }
		public TerminalNode STRING() { return getToken(Gramatica.STRING, 0); }
		public TerminalNode EXIT() { return getToken(Gramatica.EXIT, 0); }
		public TerminalNode CYCLE() { return getToken(Gramatica.CYCLE, 0); }
		public PrimitivoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitivo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterPrimitivo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitPrimitivo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitPrimitivo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimitivoContext primitivo() throws RecognitionException {
		PrimitivoContext _localctx = new PrimitivoContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_primitivo);
		try {
			setState(637);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMBER:
				enterOuterAlt(_localctx, 1);
				{
				setState(619);
				((PrimitivoContext)_localctx).NUMBER = match(NUMBER);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).NUMBER!=null?((PrimitivoContext)_localctx).NUMBER.getText():null), Tipo.ENTERO, ((PrimitivoContext)_localctx).NUMBER.getLine(), ((PrimitivoContext)_localctx).NUMBER.getCharPositionInLine()); 
				}
				break;
			case FLOAT:
				enterOuterAlt(_localctx, 2);
				{
				setState(621);
				((PrimitivoContext)_localctx).FLOAT = match(FLOAT);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).FLOAT!=null?((PrimitivoContext)_localctx).FLOAT.getText():null), Tipo.REAL, ((PrimitivoContext)_localctx).FLOAT.getLine(), ((PrimitivoContext)_localctx).FLOAT.getCharPositionInLine()); 
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(623);
				((PrimitivoContext)_localctx).TRUE = match(TRUE);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).TRUE!=null?((PrimitivoContext)_localctx).TRUE.getText():null), Tipo.LOGICAL, ((PrimitivoContext)_localctx).TRUE.getLine(), ((PrimitivoContext)_localctx).TRUE.getCharPositionInLine()); 
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 4);
				{
				setState(625);
				((PrimitivoContext)_localctx).FALSE = match(FALSE);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).FALSE!=null?((PrimitivoContext)_localctx).FALSE.getText():null), Tipo.LOGICAL, ((PrimitivoContext)_localctx).FALSE.getLine(), ((PrimitivoContext)_localctx).FALSE.getCharPositionInLine()); 
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 5);
				{
				setState(627);
				((PrimitivoContext)_localctx).ID = match(ID);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).ID!=null?((PrimitivoContext)_localctx).ID.getText():null), Tipo.IDENTIFICADOR, ((PrimitivoContext)_localctx).ID.getLine(), ((PrimitivoContext)_localctx).ID.getCharPositionInLine()); 
				}
				break;
			case STRING2:
				enterOuterAlt(_localctx, 6);
				{
				setState(629);
				((PrimitivoContext)_localctx).STRING2 = match(STRING2);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).STRING2!=null?((PrimitivoContext)_localctx).STRING2.getText():null), Tipo.CHARACTER, ((PrimitivoContext)_localctx).STRING2.getLine(), ((PrimitivoContext)_localctx).STRING2.getCharPositionInLine()); 
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 7);
				{
				setState(631);
				((PrimitivoContext)_localctx).STRING = match(STRING);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).STRING!=null?((PrimitivoContext)_localctx).STRING.getText():null), Tipo.CHARACTER, ((PrimitivoContext)_localctx).STRING.getLine(), ((PrimitivoContext)_localctx).STRING.getCharPositionInLine()); 
				}
				break;
			case EXIT:
				enterOuterAlt(_localctx, 8);
				{
				setState(633);
				((PrimitivoContext)_localctx).EXIT = match(EXIT);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).EXIT!=null?((PrimitivoContext)_localctx).EXIT.getText():null), Tipo.EXIT, ((PrimitivoContext)_localctx).EXIT.getLine(), ((PrimitivoContext)_localctx).EXIT.getCharPositionInLine()); 
				}
				break;
			case CYCLE:
				enterOuterAlt(_localctx, 9);
				{
				setState(635);
				((PrimitivoContext)_localctx).CYCLE = match(CYCLE);
				 ((PrimitivoContext)_localctx).inst =  new Primitives((((PrimitivoContext)_localctx).CYCLE!=null?((PrimitivoContext)_localctx).CYCLE.getText():null), Tipo.CYCLE, ((PrimitivoContext)_localctx).CYCLE.getLine(), ((PrimitivoContext)_localctx).CYCLE.getCharPositionInLine()); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return listFunctions_sempred((ListFunctionsContext)_localctx, predIndex);
		case 8:
			return list_cont_instruction_sempred((List_cont_instructionContext)_localctx, predIndex);
		case 15:
			return list_variable_sempred((List_variableContext)_localctx, predIndex);
		case 18:
			return list_If_sempred((List_IfContext)_localctx, predIndex);
		case 26:
			return dimension_array_sempred((Dimension_arrayContext)_localctx, predIndex);
		case 27:
			return dimension_array_dinamyc_sempred((Dimension_array_dinamycContext)_localctx, predIndex);
		case 29:
			return list_expression_sempred((List_expressionContext)_localctx, predIndex);
		case 31:
			return expression_arithmetic_sempred((Expression_arithmeticContext)_localctx, predIndex);
		case 32:
			return expression_relational_sempred((Expression_relationalContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean listFunctions_sempred(ListFunctionsContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean list_cont_instruction_sempred(List_cont_instructionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean list_variable_sempred(List_variableContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean list_If_sempred(List_IfContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean dimension_array_sempred(Dimension_arrayContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean dimension_array_dinamyc_sempred(Dimension_array_dinamycContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean list_expression_sempred(List_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expression_arithmetic_sempred(Expression_arithmeticContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 5);
		case 8:
			return precpred(_ctx, 4);
		case 9:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean expression_relational_sempred(Expression_relationalContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10:
			return precpred(_ctx, 4);
		case 11:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001F\u0280\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007\u0018"+
		"\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007\u001b"+
		"\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007\u001e"+
		"\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007\"\u0002"+
		"#\u0007#\u0001\u0000\u0001\u0000\u0001\u0000\u0001\u0001\u0005\u0001M"+
		"\b\u0001\n\u0001\f\u0001P\t\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0005\u0002]\b\u0002\n\u0002\f\u0002`\t\u0002"+
		"\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0003\u0004\u009f\b\u0004\u0001\u0005\u0001\u0005\u0001\u0005"+
		"\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0003\u0005"+
		"\u00a9\b\u0005\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0003\u0006"+
		"\u00c1\b\u0006\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007"+
		"\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\b\u0001"+
		"\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0001\b\u0005\b\u00d5\b\b\n"+
		"\b\f\b\u00d8\t\b\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0003"+
		"\t\u0104\b\t\u0001\n\u0001\n\u0001\n\u0001\n\u0001\n\u0001\u000b\u0001"+
		"\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001\f\u0001\f\u0001\f\u0001"+
		"\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\f\u0001\r\u0001\r\u0001"+
		"\r\u0001\r\u0001\r\u0001\r\u0003\r\u0120\b\r\u0001\u000e\u0001\u000e\u0001"+
		"\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001\u000e\u0001"+
		"\u000e\u0001\u000e\u0003\u000e\u012c\b\u000e\u0001\u000f\u0001\u000f\u0001"+
		"\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0001"+
		"\u000f\u0005\u000f\u0137\b\u000f\n\u000f\f\u000f\u013a\t\u000f\u0001\u0010"+
		"\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010"+
		"\u0003\u0010\u0143\b\u0010\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011"+
		"\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0001\u0011\u0003\u0011"+
		"\u0178\b\u0011\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0005\u0012\u0182\b\u0012\n\u0012"+
		"\f\u0012\u0185\t\u0012\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0003\u0013\u019f\b\u0013\u0001\u0014\u0001\u0014"+
		"\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014\u0001\u0014"+
		"\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015\u0001\u0015"+
		"\u0001\u0015\u0001\u0015\u0003\u0015\u01bd\b\u0015\u0001\u0016\u0001\u0016"+
		"\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016\u0001\u0016"+
		"\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0017"+
		"\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0018\u0001\u0018\u0001\u0018"+
		"\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018\u0001\u0018"+
		"\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019"+
		"\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a\u0001\u001a"+
		"\u0001\u001a\u0001\u001a\u0001\u001a\u0005\u001a\u01e8\b\u001a\n\u001a"+
		"\f\u001a\u01eb\t\u001a\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001b"+
		"\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001b\u0005\u001b\u01f5\b\u001b"+
		"\n\u001b\f\u001b\u01f8\t\u001b\u0001\u001c\u0001\u001c\u0001\u001c\u0001"+
		"\u001c\u0001\u001c\u0001\u001c\u0001\u001d\u0001\u001d\u0001\u001d\u0001"+
		"\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0005"+
		"\u001d\u0209\b\u001d\n\u001d\f\u001d\u020c\t\u001d\u0001\u001e\u0001\u001e"+
		"\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e"+
		"\u0001\u001e\u0001\u001e\u0001\u001e\u0001\u001e\u0003\u001e\u021a\b\u001e"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0003\u001f\u0229\b\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f\u0001\u001f"+
		"\u0005\u001f\u023a\b\u001f\n\u001f\f\u001f\u023d\t\u001f\u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0003 \u0247\b \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0005 \u0253"+
		"\b \n \f \u0256\t \u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001!\u0001"+
		"!\u0001!\u0001!\u0001!\u0001!\u0003!\u0264\b!\u0001\"\u0001\"\u0001\""+
		"\u0001\"\u0001\"\u0001\"\u0001#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001"+
		"#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001#\u0001"+
		"#\u0001#\u0003#\u027e\b#\u0001#\u0000\t\u0004\u0010\u001e$46:>@$\u0000"+
		"\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a\u001c"+
		"\u001e \"$&(*,.02468:<>@BDF\u0000\u0004\u0001\u0000\u001b\u001c\u0001"+
		"\u0000\u0019\u001a\u0001\u0000\u001e)\u0001\u0000*+\u0299\u0000H\u0001"+
		"\u0000\u0000\u0000\u0002N\u0001\u0000\u0000\u0000\u0004T\u0001\u0000\u0000"+
		"\u0000\u0006a\u0001\u0000\u0000\u0000\b\u009e\u0001\u0000\u0000\u0000"+
		"\n\u00a8\u0001\u0000\u0000\u0000\f\u00c0\u0001\u0000\u0000\u0000\u000e"+
		"\u00c2\u0001\u0000\u0000\u0000\u0010\u00cc\u0001\u0000\u0000\u0000\u0012"+
		"\u0103\u0001\u0000\u0000\u0000\u0014\u0105\u0001\u0000\u0000\u0000\u0016"+
		"\u010a\u0001\u0000\u0000\u0000\u0018\u010f\u0001\u0000\u0000\u0000\u001a"+
		"\u011f\u0001\u0000\u0000\u0000\u001c\u012b\u0001\u0000\u0000\u0000\u001e"+
		"\u012d\u0001\u0000\u0000\u0000 \u0142\u0001\u0000\u0000\u0000\"\u0177"+
		"\u0001\u0000\u0000\u0000$\u0179\u0001\u0000\u0000\u0000&\u019e\u0001\u0000"+
		"\u0000\u0000(\u01a0\u0001\u0000\u0000\u0000*\u01bc\u0001\u0000\u0000\u0000"+
		",\u01be\u0001\u0000\u0000\u0000.\u01c6\u0001\u0000\u0000\u00000\u01cf"+
		"\u0001\u0000\u0000\u00002\u01d8\u0001\u0000\u0000\u00004\u01de\u0001\u0000"+
		"\u0000\u00006\u01ec\u0001\u0000\u0000\u00008\u01f9\u0001\u0000\u0000\u0000"+
		":\u01ff\u0001\u0000\u0000\u0000<\u0219\u0001\u0000\u0000\u0000>\u0228"+
		"\u0001\u0000\u0000\u0000@\u0246\u0001\u0000\u0000\u0000B\u0263\u0001\u0000"+
		"\u0000\u0000D\u0265\u0001\u0000\u0000\u0000F\u027d\u0001\u0000\u0000\u0000"+
		"HI\u0003\u0002\u0001\u0000IJ\u0006\u0000\uffff\uffff\u0000J\u0001\u0001"+
		"\u0000\u0000\u0000KM\u0003\u0004\u0002\u0000LK\u0001\u0000\u0000\u0000"+
		"MP\u0001\u0000\u0000\u0000NL\u0001\u0000\u0000\u0000NO\u0001\u0000\u0000"+
		"\u0000OQ\u0001\u0000\u0000\u0000PN\u0001\u0000\u0000\u0000QR\u0003\u000e"+
		"\u0007\u0000RS\u0006\u0001\uffff\uffff\u0000S\u0003\u0001\u0000\u0000"+
		"\u0000TU\u0006\u0002\uffff\uffff\u0000UV\u0003\u0006\u0003\u0000VW\u0006"+
		"\u0002\uffff\uffff\u0000W^\u0001\u0000\u0000\u0000XY\n\u0002\u0000\u0000"+
		"YZ\u0003\u0006\u0003\u0000Z[\u0006\u0002\uffff\uffff\u0000[]\u0001\u0000"+
		"\u0000\u0000\\X\u0001\u0000\u0000\u0000]`\u0001\u0000\u0000\u0000^\\\u0001"+
		"\u0000\u0000\u0000^_\u0001\u0000\u0000\u0000_\u0005\u0001\u0000\u0000"+
		"\u0000`^\u0001\u0000\u0000\u0000ab\u0003\b\u0004\u0000bc\u0006\u0003\uffff"+
		"\uffff\u0000c\u0007\u0001\u0000\u0000\u0000de\u0005<\u0000\u0000ef\u0005"+
		"D\u0000\u0000fg\u0005\u0007\u0000\u0000gh\u0003\n\u0005\u0000hi\u0005"+
		"\b\u0000\u0000ij\u0005=\u0000\u0000jk\u0005\u0007\u0000\u0000kl\u0005"+
		"D\u0000\u0000lm\u0005\b\u0000\u0000mn\u0005\u000b\u0000\u0000no\u0005"+
		"\f\u0000\u0000op\u0003\u0010\b\u0000pq\u0005\u000f\u0000\u0000qr\u0005"+
		"<\u0000\u0000rs\u0005D\u0000\u0000st\u0006\u0004\uffff\uffff\u0000t\u009f"+
		"\u0001\u0000\u0000\u0000uv\u0005;\u0000\u0000vw\u0005D\u0000\u0000wx\u0005"+
		"\u0007\u0000\u0000xy\u0003\n\u0005\u0000yz\u0005\b\u0000\u0000z{\u0005"+
		"\u000b\u0000\u0000{|\u0005\f\u0000\u0000|}\u0003\u0010\b\u0000}~\u0005"+
		"\u000f\u0000\u0000~\u007f\u0005;\u0000\u0000\u007f\u0080\u0005D\u0000"+
		"\u0000\u0080\u0081\u0006\u0004\uffff\uffff\u0000\u0081\u009f\u0001\u0000"+
		"\u0000\u0000\u0082\u0083\u0005<\u0000\u0000\u0083\u0084\u0005D\u0000\u0000"+
		"\u0084\u0085\u0005\u0007\u0000\u0000\u0085\u0086\u0005\b\u0000\u0000\u0086"+
		"\u0087\u0005=\u0000\u0000\u0087\u0088\u0005\u0007\u0000\u0000\u0088\u0089"+
		"\u0005D\u0000\u0000\u0089\u008a\u0005\b\u0000\u0000\u008a\u008b\u0005"+
		"\u000b\u0000\u0000\u008b\u008c\u0005\f\u0000\u0000\u008c\u008d\u0003\u0010"+
		"\b\u0000\u008d\u008e\u0005\u000f\u0000\u0000\u008e\u008f\u0005<\u0000"+
		"\u0000\u008f\u0090\u0005D\u0000\u0000\u0090\u0091\u0006\u0004\uffff\uffff"+
		"\u0000\u0091\u009f\u0001\u0000\u0000\u0000\u0092\u0093\u0005;\u0000\u0000"+
		"\u0093\u0094\u0005D\u0000\u0000\u0094\u0095\u0005\u0007\u0000\u0000\u0095"+
		"\u0096\u0005\b\u0000\u0000\u0096\u0097\u0005\u000b\u0000\u0000\u0097\u0098"+
		"\u0005\f\u0000\u0000\u0098\u0099\u0003\u0010\b\u0000\u0099\u009a\u0005"+
		"\u000f\u0000\u0000\u009a\u009b\u0005;\u0000\u0000\u009b\u009c\u0005D\u0000"+
		"\u0000\u009c\u009d\u0006\u0004\uffff\uffff\u0000\u009d\u009f\u0001\u0000"+
		"\u0000\u0000\u009ed\u0001\u0000\u0000\u0000\u009eu\u0001\u0000\u0000\u0000"+
		"\u009e\u0082\u0001\u0000\u0000\u0000\u009e\u0092\u0001\u0000\u0000\u0000"+
		"\u009f\t\u0001\u0000\u0000\u0000\u00a0\u00a1\u0003\u001e\u000f\u0000\u00a1"+
		"\u00a2\u0005\u0003\u0000\u0000\u00a2\u00a3\u0003 \u0010\u0000\u00a3\u00a4"+
		"\u0006\u0005\uffff\uffff\u0000\u00a4\u00a9\u0001\u0000\u0000\u0000\u00a5"+
		"\u00a6\u0003 \u0010\u0000\u00a6\u00a7\u0006\u0005\uffff\uffff\u0000\u00a7"+
		"\u00a9\u0001\u0000\u0000\u0000\u00a8\u00a0\u0001\u0000\u0000\u0000\u00a8"+
		"\u00a5\u0001\u0000\u0000\u0000\u00a9\u000b\u0001\u0000\u0000\u0000\u00aa"+
		"\u00ab\u0005D\u0000\u0000\u00ab\u00ac\u0005\u0007\u0000\u0000\u00ac\u00ad"+
		"\u00034\u001a\u0000\u00ad\u00ae\u0005\b\u0000\u0000\u00ae\u00af\u0006"+
		"\u0006\uffff\uffff\u0000\u00af\u00c1\u0001\u0000\u0000\u0000\u00b0\u00b1"+
		"\u0005:\u0000\u0000\u00b1\u00b2\u0005D\u0000\u0000\u00b2\u00b3\u0005\u0007"+
		"\u0000\u0000\u00b3\u00b4\u00034\u001a\u0000\u00b4\u00b5\u0005\b\u0000"+
		"\u0000\u00b5\u00b6\u0006\u0006\uffff\uffff\u0000\u00b6\u00c1\u0001\u0000"+
		"\u0000\u0000\u00b7\u00b8\u0005D\u0000\u0000\u00b8\u00b9\u0005\u0007\u0000"+
		"\u0000\u00b9\u00ba\u0005\b\u0000\u0000\u00ba\u00c1\u0006\u0006\uffff\uffff"+
		"\u0000\u00bb\u00bc\u0005:\u0000\u0000\u00bc\u00bd\u0005D\u0000\u0000\u00bd"+
		"\u00be\u0005\u0007\u0000\u0000\u00be\u00bf\u0005\b\u0000\u0000\u00bf\u00c1"+
		"\u0006\u0006\uffff\uffff\u0000\u00c0\u00aa\u0001\u0000\u0000\u0000\u00c0"+
		"\u00b0\u0001\u0000\u0000\u0000\u00c0\u00b7\u0001\u0000\u0000\u0000\u00c0"+
		"\u00bb\u0001\u0000\u0000\u0000\u00c1\r\u0001\u0000\u0000\u0000\u00c2\u00c3"+
		"\u0005\n\u0000\u0000\u00c3\u00c4\u0005D\u0000\u0000\u00c4\u00c5\u0005"+
		"\u000b\u0000\u0000\u00c5\u00c6\u0005\f\u0000\u0000\u00c6\u00c7\u0003\u0010"+
		"\b\u0000\u00c7\u00c8\u0005\u000f\u0000\u0000\u00c8\u00c9\u0005\n\u0000"+
		"\u0000\u00c9\u00ca\u0005D\u0000\u0000\u00ca\u00cb\u0006\u0007\uffff\uffff"+
		"\u0000\u00cb\u000f\u0001\u0000\u0000\u0000\u00cc\u00cd\u0006\b\uffff\uffff"+
		"\u0000\u00cd\u00ce\u0003\u0012\t\u0000\u00ce\u00cf\u0006\b\uffff\uffff"+
		"\u0000\u00cf\u00d6\u0001\u0000\u0000\u0000\u00d0\u00d1\n\u0002\u0000\u0000"+
		"\u00d1\u00d2\u0003\u0012\t\u0000\u00d2\u00d3\u0006\b\uffff\uffff\u0000"+
		"\u00d3\u00d5\u0001\u0000\u0000\u0000\u00d4\u00d0\u0001\u0000\u0000\u0000"+
		"\u00d5\u00d8\u0001\u0000\u0000\u0000\u00d6\u00d4\u0001\u0000\u0000\u0000"+
		"\u00d6\u00d7\u0001\u0000\u0000\u0000\u00d7\u0011\u0001\u0000\u0000\u0000"+
		"\u00d8\u00d6\u0001\u0000\u0000\u0000\u00d9\u00da\u0005\u0013\u0000\u0000"+
		"\u00da\u00db\u0005\u001b\u0000\u0000\u00db\u00dc\u0005\u0003\u0000\u0000"+
		"\u00dc\u00dd\u0003:\u001d\u0000\u00dd\u00de\u0006\t\uffff\uffff\u0000"+
		"\u00de\u0104\u0001\u0000\u0000\u0000\u00df\u00e0\u0003\u0016\u000b\u0000"+
		"\u00e0\u00e1\u0006\t\uffff\uffff\u0000\u00e1\u0104\u0001\u0000\u0000\u0000"+
		"\u00e2\u00e3\u0003\u0018\f\u0000\u00e3\u00e4\u0006\t\uffff\uffff\u0000"+
		"\u00e4\u0104\u0001\u0000\u0000\u0000\u00e5\u00e6\u0003\u0014\n\u0000\u00e6"+
		"\u00e7\u0006\t\uffff\uffff\u0000\u00e7\u0104\u0001\u0000\u0000\u0000\u00e8"+
		"\u00e9\u0003\"\u0011\u0000\u00e9\u00ea\u0006\t\uffff\uffff\u0000\u00ea"+
		"\u0104\u0001\u0000\u0000\u0000\u00eb\u00ec\u0003&\u0013\u0000\u00ec\u00ed"+
		"\u0006\t\uffff\uffff\u0000\u00ed\u0104\u0001\u0000\u0000\u0000\u00ee\u00ef"+
		"\u0003(\u0014\u0000\u00ef\u00f0\u0006\t\uffff\uffff\u0000\u00f0\u0104"+
		"\u0001\u0000\u0000\u0000\u00f1\u00f2\u0003*\u0015\u0000\u00f2\u00f3\u0006"+
		"\t\uffff\uffff\u0000\u00f3\u0104\u0001\u0000\u0000\u0000\u00f4\u00f5\u0003"+
		",\u0016\u0000\u00f5\u00f6\u0006\t\uffff\uffff\u0000\u00f6\u0104\u0001"+
		"\u0000\u0000\u0000\u00f7\u00f8\u0003.\u0017\u0000\u00f8\u00f9\u0006\t"+
		"\uffff\uffff\u0000\u00f9\u0104\u0001\u0000\u0000\u0000\u00fa\u00fb\u0003"+
		"\f\u0006\u0000\u00fb\u00fc\u0006\t\uffff\uffff\u0000\u00fc\u0104\u0001"+
		"\u0000\u0000\u0000\u00fd\u00fe\u00030\u0018\u0000\u00fe\u00ff\u0006\t"+
		"\uffff\uffff\u0000\u00ff\u0104\u0001\u0000\u0000\u0000\u0100\u0101\u0003"+
		"2\u0019\u0000\u0101\u0102\u0006\t\uffff\uffff\u0000\u0102\u0104\u0001"+
		"\u0000\u0000\u0000\u0103\u00d9\u0001\u0000\u0000\u0000\u0103\u00df\u0001"+
		"\u0000\u0000\u0000\u0103\u00e2\u0001\u0000\u0000\u0000\u0103\u00e5\u0001"+
		"\u0000\u0000\u0000\u0103\u00e8\u0001\u0000\u0000\u0000\u0103\u00eb\u0001"+
		"\u0000\u0000\u0000\u0103\u00ee\u0001\u0000\u0000\u0000\u0103\u00f1\u0001"+
		"\u0000\u0000\u0000\u0103\u00f4\u0001\u0000\u0000\u0000\u0103\u00f7\u0001"+
		"\u0000\u0000\u0000\u0103\u00fa\u0001\u0000\u0000\u0000\u0103\u00fd\u0001"+
		"\u0000\u0000\u0000\u0103\u0100\u0001\u0000\u0000\u0000\u0104\u0013\u0001"+
		"\u0000\u0000\u0000\u0105\u0106\u0005D\u0000\u0000\u0106\u0107\u0005\u0001"+
		"\u0000\u0000\u0107\u0108\u0003<\u001e\u0000\u0108\u0109\u0006\n\uffff"+
		"\uffff\u0000\u0109\u0015\u0001\u0000\u0000\u0000\u010a\u010b\u0003\u001c"+
		"\u000e\u0000\u010b\u010c\u0005\t\u0000\u0000\u010c\u010d\u0003\u001e\u000f"+
		"\u0000\u010d\u010e\u0006\u000b\uffff\uffff\u0000\u010e\u0017\u0001\u0000"+
		"\u0000\u0000\u010f\u0110\u0003\u001c\u000e\u0000\u0110\u0111\u0005\u0003"+
		"\u0000\u0000\u0111\u0112\u00056\u0000\u0000\u0112\u0113\u0005\u0007\u0000"+
		"\u0000\u0113\u0114\u0003\u001a\r\u0000\u0114\u0115\u0005\b\u0000\u0000"+
		"\u0115\u0116\u0005\t\u0000\u0000\u0116\u0117\u0003\u001e\u000f\u0000\u0117"+
		"\u0118\u0006\f\uffff\uffff\u0000\u0118\u0019\u0001\u0000\u0000\u0000\u0119"+
		"\u011a\u00057\u0000\u0000\u011a\u0120\u0006\r\uffff\uffff\u0000\u011b"+
		"\u011c\u00058\u0000\u0000\u011c\u0120\u0006\r\uffff\uffff\u0000\u011d"+
		"\u011e\u00059\u0000\u0000\u011e\u0120\u0006\r\uffff\uffff\u0000\u011f"+
		"\u0119\u0001\u0000\u0000\u0000\u011f\u011b\u0001\u0000\u0000\u0000\u011f"+
		"\u011d\u0001\u0000\u0000\u0000\u0120\u001b\u0001\u0000\u0000\u0000\u0121"+
		"\u0122\u0005\u0014\u0000\u0000\u0122\u012c\u0006\u000e\uffff\uffff\u0000"+
		"\u0123\u0124\u0005\u0015\u0000\u0000\u0124\u012c\u0006\u000e\uffff\uffff"+
		"\u0000\u0125\u0126\u0005\u0016\u0000\u0000\u0126\u012c\u0006\u000e\uffff"+
		"\uffff\u0000\u0127\u0128\u0005\u0017\u0000\u0000\u0128\u012c\u0006\u000e"+
		"\uffff\uffff\u0000\u0129\u012a\u0005\u0018\u0000\u0000\u012a\u012c\u0006"+
		"\u000e\uffff\uffff\u0000\u012b\u0121\u0001\u0000\u0000\u0000\u012b\u0123"+
		"\u0001\u0000\u0000\u0000\u012b\u0125\u0001\u0000\u0000\u0000\u012b\u0127"+
		"\u0001\u0000\u0000\u0000\u012b\u0129\u0001\u0000\u0000\u0000\u012c\u001d"+
		"\u0001\u0000\u0000\u0000\u012d\u012e\u0006\u000f\uffff\uffff\u0000\u012e"+
		"\u012f\u0003 \u0010\u0000\u012f\u0130\u0006\u000f\uffff\uffff\u0000\u0130"+
		"\u0138\u0001\u0000\u0000\u0000\u0131\u0132\n\u0002\u0000\u0000\u0132\u0133"+
		"\u0005\u0003\u0000\u0000\u0133\u0134\u0003 \u0010\u0000\u0134\u0135\u0006"+
		"\u000f\uffff\uffff\u0000\u0135\u0137\u0001\u0000\u0000\u0000\u0136\u0131"+
		"\u0001\u0000\u0000\u0000\u0137\u013a\u0001\u0000\u0000\u0000\u0138\u0136"+
		"\u0001\u0000\u0000\u0000\u0138\u0139\u0001\u0000\u0000\u0000\u0139\u001f"+
		"\u0001\u0000\u0000\u0000\u013a\u0138\u0001\u0000\u0000\u0000\u013b\u013c"+
		"\u0005D\u0000\u0000\u013c\u0143\u0006\u0010\uffff\uffff\u0000\u013d\u013e"+
		"\u0005D\u0000\u0000\u013e\u013f\u0005\u0001\u0000\u0000\u013f\u0140\u0003"+
		"<\u001e\u0000\u0140\u0141\u0006\u0010\uffff\uffff\u0000\u0141\u0143\u0001"+
		"\u0000\u0000\u0000\u0142\u013b\u0001\u0000\u0000\u0000\u0142\u013d\u0001"+
		"\u0000\u0000\u0000\u0143!\u0001\u0000\u0000\u0000\u0144\u0145\u00052\u0000"+
		"\u0000\u0145\u0146\u0005\u0007\u0000\u0000\u0146\u0147\u0003<\u001e\u0000"+
		"\u0147\u0148\u0005\b\u0000\u0000\u0148\u0149\u0005\u0010\u0000\u0000\u0149"+
		"\u014a\u0003\u0010\b\u0000\u014a\u014b\u0005\u000f\u0000\u0000\u014b\u014c"+
		"\u00052\u0000\u0000\u014c\u014d\u0006\u0011\uffff\uffff\u0000\u014d\u0178"+
		"\u0001\u0000\u0000\u0000\u014e\u014f\u00052\u0000\u0000\u014f\u0150\u0005"+
		"\u0007\u0000\u0000\u0150\u0151\u0003<\u001e\u0000\u0151\u0152\u0005\b"+
		"\u0000\u0000\u0152\u0153\u0005\u0010\u0000\u0000\u0153\u0154\u0005\u000f"+
		"\u0000\u0000\u0154\u0155\u00052\u0000\u0000\u0155\u0178\u0001\u0000\u0000"+
		"\u0000\u0156\u0157\u00052\u0000\u0000\u0157\u0158\u0005\u0007\u0000\u0000"+
		"\u0158\u0159\u0003<\u001e\u0000\u0159\u015a\u0005\b\u0000\u0000\u015a"+
		"\u015b\u0005\u0010\u0000\u0000\u015b\u015c\u0003\u0010\b\u0000\u015c\u015d"+
		"\u00053\u0000\u0000\u015d\u015e\u0003\u0010\b\u0000\u015e\u015f\u0005"+
		"\u000f\u0000\u0000\u015f\u0160\u00052\u0000\u0000\u0160\u0161\u0006\u0011"+
		"\uffff\uffff\u0000\u0161\u0178\u0001\u0000\u0000\u0000\u0162\u0163\u0005"+
		"2\u0000\u0000\u0163\u0164\u0005\u0007\u0000\u0000\u0164\u0165\u0003<\u001e"+
		"\u0000\u0165\u0166\u0005\b\u0000\u0000\u0166\u0167\u0005\u0010\u0000\u0000"+
		"\u0167\u0168\u0003\u0010\b\u0000\u0168\u0169\u00053\u0000\u0000\u0169"+
		"\u016a\u0003$\u0012\u0000\u016a\u016b\u0006\u0011\uffff\uffff\u0000\u016b"+
		"\u0178\u0001\u0000\u0000\u0000\u016c\u016d\u00052\u0000\u0000\u016d\u016e"+
		"\u0005\u0007\u0000\u0000\u016e\u016f\u0003<\u001e\u0000\u016f\u0170\u0005"+
		"\b\u0000\u0000\u0170\u0171\u0005\u0010\u0000\u0000\u0171\u0172\u00053"+
		"\u0000\u0000\u0172\u0173\u0003\u0010\b\u0000\u0173\u0174\u0005\u000f\u0000"+
		"\u0000\u0174\u0175\u00052\u0000\u0000\u0175\u0176\u0006\u0011\uffff\uffff"+
		"\u0000\u0176\u0178\u0001\u0000\u0000\u0000\u0177\u0144\u0001\u0000\u0000"+
		"\u0000\u0177\u014e\u0001\u0000\u0000\u0000\u0177\u0156\u0001\u0000\u0000"+
		"\u0000\u0177\u0162\u0001\u0000\u0000\u0000\u0177\u016c\u0001\u0000\u0000"+
		"\u0000\u0178#\u0001\u0000\u0000\u0000\u0179\u017a\u0006\u0012\uffff\uffff"+
		"\u0000\u017a\u017b\u0003\"\u0011\u0000\u017b\u017c\u0006\u0012\uffff\uffff"+
		"\u0000\u017c\u0183\u0001\u0000\u0000\u0000\u017d\u017e\n\u0002\u0000\u0000"+
		"\u017e\u017f\u0003\"\u0011\u0000\u017f\u0180\u0006\u0012\uffff\uffff\u0000"+
		"\u0180\u0182\u0001\u0000\u0000\u0000\u0181\u017d\u0001\u0000\u0000\u0000"+
		"\u0182\u0185\u0001\u0000\u0000\u0000\u0183\u0181\u0001\u0000\u0000\u0000"+
		"\u0183\u0184\u0001\u0000\u0000\u0000\u0184%\u0001\u0000\u0000\u0000\u0185"+
		"\u0183\u0001\u0000\u0000\u0000\u0186\u0187\u00054\u0000\u0000\u0187\u0188"+
		"\u0005D\u0000\u0000\u0188\u0189\u0005\u0001\u0000\u0000\u0189\u018a\u0003"+
		"<\u001e\u0000\u018a\u018b\u0005\u0003\u0000\u0000\u018b\u018c\u0003<\u001e"+
		"\u0000\u018c\u018d\u0005\u0003\u0000\u0000\u018d\u018e\u0003<\u001e\u0000"+
		"\u018e\u018f\u0003\u0010\b\u0000\u018f\u0190\u0005\u000f\u0000\u0000\u0190"+
		"\u0191\u00054\u0000\u0000\u0191\u0192\u0006\u0013\uffff\uffff\u0000\u0192"+
		"\u019f\u0001\u0000\u0000\u0000\u0193\u0194\u00054\u0000\u0000\u0194\u0195"+
		"\u0005D\u0000\u0000\u0195\u0196\u0005\u0001\u0000\u0000\u0196\u0197\u0003"+
		"<\u001e\u0000\u0197\u0198\u0005\u0003\u0000\u0000\u0198\u0199\u0003<\u001e"+
		"\u0000\u0199\u019a\u0003\u0010\b\u0000\u019a\u019b\u0005\u000f\u0000\u0000"+
		"\u019b\u019c\u00054\u0000\u0000\u019c\u019d\u0006\u0013\uffff\uffff\u0000"+
		"\u019d\u019f\u0001\u0000\u0000\u0000\u019e\u0186\u0001\u0000\u0000\u0000"+
		"\u019e\u0193\u0001\u0000\u0000\u0000\u019f\'\u0001\u0000\u0000\u0000\u01a0"+
		"\u01a1\u00054\u0000\u0000\u01a1\u01a2\u00055\u0000\u0000\u01a2\u01a3\u0003"+
		"<\u001e\u0000\u01a3\u01a4\u0003\u0010\b\u0000\u01a4\u01a5\u0005\u000f"+
		"\u0000\u0000\u01a5\u01a6\u00054\u0000\u0000\u01a6\u01a7\u0006\u0014\uffff"+
		"\uffff\u0000\u01a7)\u0001\u0000\u0000\u0000\u01a8\u01a9\u0003\u001c\u000e"+
		"\u0000\u01a9\u01aa\u0005\u0003\u0000\u0000\u01aa\u01ab\u0005-\u0000\u0000"+
		"\u01ab\u01ac\u0005\u0007\u0000\u0000\u01ac\u01ad\u00034\u001a\u0000\u01ad"+
		"\u01ae\u0005\b\u0000\u0000\u01ae\u01af\u0005\t\u0000\u0000\u01af\u01b0"+
		"\u0005D\u0000\u0000\u01b0\u01b1\u0006\u0015\uffff\uffff\u0000\u01b1\u01bd"+
		"\u0001\u0000\u0000\u0000\u01b2\u01b3\u0003\u001c\u000e\u0000\u01b3\u01b4"+
		"\u0005\u0003\u0000\u0000\u01b4\u01b5\u0005/\u0000\u0000\u01b5\u01b6\u0005"+
		"\t\u0000\u0000\u01b6\u01b7\u0005D\u0000\u0000\u01b7\u01b8\u0005\u0007"+
		"\u0000\u0000\u01b8\u01b9\u00036\u001b\u0000\u01b9\u01ba\u0005\b\u0000"+
		"\u0000\u01ba\u01bb\u0006\u0015\uffff\uffff\u0000\u01bb\u01bd\u0001\u0000"+
		"\u0000\u0000\u01bc\u01a8\u0001\u0000\u0000\u0000\u01bc\u01b2\u0001\u0000"+
		"\u0000\u0000\u01bd+\u0001\u0000\u0000\u0000\u01be\u01bf\u0005D\u0000\u0000"+
		"\u01bf\u01c0\u0005\r\u0000\u0000\u01c0\u01c1\u00034\u001a\u0000\u01c1"+
		"\u01c2\u0005\u000e\u0000\u0000\u01c2\u01c3\u0005\u0001\u0000\u0000\u01c3"+
		"\u01c4\u0003<\u001e\u0000\u01c4\u01c5\u0006\u0016\uffff\uffff\u0000\u01c5"+
		"-\u0001\u0000\u0000\u0000\u01c6\u01c7\u0005D\u0000\u0000\u01c7\u01c8\u0005"+
		"\u0001\u0000\u0000\u01c8\u01c9\u0005\u0007\u0000\u0000\u01c9\u01ca\u0005"+
		"\u001c\u0000\u0000\u01ca\u01cb\u00034\u001a\u0000\u01cb\u01cc\u0005\u001c"+
		"\u0000\u0000\u01cc\u01cd\u0005\b\u0000\u0000\u01cd\u01ce\u0006\u0017\uffff"+
		"\uffff\u0000\u01ce/\u0001\u0000\u0000\u0000\u01cf\u01d0\u00050\u0000\u0000"+
		"\u01d0\u01d1\u0005\u0007\u0000\u0000\u01d1\u01d2\u0005D\u0000\u0000\u01d2"+
		"\u01d3\u0005\u0007\u0000\u0000\u01d3\u01d4\u00034\u001a\u0000\u01d4\u01d5"+
		"\u0005\b\u0000\u0000\u01d5\u01d6\u0005\b\u0000\u0000\u01d6\u01d7\u0006"+
		"\u0018\uffff\uffff\u0000\u01d71\u0001\u0000\u0000\u0000\u01d8\u01d9\u0005"+
		"1\u0000\u0000\u01d9\u01da\u0005\u0007\u0000\u0000\u01da\u01db\u0005D\u0000"+
		"\u0000\u01db\u01dc\u0005\b\u0000\u0000\u01dc\u01dd\u0006\u0019\uffff\uffff"+
		"\u0000\u01dd3\u0001\u0000\u0000\u0000\u01de\u01df\u0006\u001a\uffff\uffff"+
		"\u0000\u01df\u01e0\u0003<\u001e\u0000\u01e0\u01e1\u0006\u001a\uffff\uffff"+
		"\u0000\u01e1\u01e9\u0001\u0000\u0000\u0000\u01e2\u01e3\n\u0002\u0000\u0000"+
		"\u01e3\u01e4\u0005\u0003\u0000\u0000\u01e4\u01e5\u0003<\u001e\u0000\u01e5"+
		"\u01e6\u0006\u001a\uffff\uffff\u0000\u01e6\u01e8\u0001\u0000\u0000\u0000"+
		"\u01e7\u01e2\u0001\u0000\u0000\u0000\u01e8\u01eb\u0001\u0000\u0000\u0000"+
		"\u01e9\u01e7\u0001\u0000\u0000\u0000\u01e9\u01ea\u0001\u0000\u0000\u0000"+
		"\u01ea5\u0001\u0000\u0000\u0000\u01eb\u01e9\u0001\u0000\u0000\u0000\u01ec"+
		"\u01ed\u0006\u001b\uffff\uffff\u0000\u01ed\u01ee\u0005\u0002\u0000\u0000"+
		"\u01ee\u01ef\u0006\u001b\uffff\uffff\u0000\u01ef\u01f6\u0001\u0000\u0000"+
		"\u0000\u01f0\u01f1\n\u0002\u0000\u0000\u01f1\u01f2\u0005\u0003\u0000\u0000"+
		"\u01f2\u01f3\u0005\u0002\u0000\u0000\u01f3\u01f5\u0006\u001b\uffff\uffff"+
		"\u0000\u01f4\u01f0\u0001\u0000\u0000\u0000\u01f5\u01f8\u0001\u0000\u0000"+
		"\u0000\u01f6\u01f4\u0001\u0000\u0000\u0000\u01f6\u01f7\u0001\u0000\u0000"+
		"\u0000\u01f77\u0001\u0000\u0000\u0000\u01f8\u01f6\u0001\u0000\u0000\u0000"+
		"\u01f9\u01fa\u0005D\u0000\u0000\u01fa\u01fb\u0005\r\u0000\u0000\u01fb"+
		"\u01fc\u00034\u001a\u0000\u01fc\u01fd\u0005\u000e\u0000\u0000\u01fd\u01fe"+
		"\u0006\u001c\uffff\uffff\u0000\u01fe9\u0001\u0000\u0000\u0000\u01ff\u0200"+
		"\u0006\u001d\uffff\uffff\u0000\u0200\u0201\u0003<\u001e\u0000\u0201\u0202"+
		"\u0006\u001d\uffff\uffff\u0000\u0202\u020a\u0001\u0000\u0000\u0000\u0203"+
		"\u0204\n\u0002\u0000\u0000\u0204\u0205\u0005\u0003\u0000\u0000\u0205\u0206"+
		"\u0003<\u001e\u0000\u0206\u0207\u0006\u001d\uffff\uffff\u0000\u0207\u0209"+
		"\u0001\u0000\u0000\u0000\u0208\u0203\u0001\u0000\u0000\u0000\u0209\u020c"+
		"\u0001\u0000\u0000\u0000\u020a\u0208\u0001\u0000\u0000\u0000\u020a\u020b"+
		"\u0001\u0000\u0000\u0000\u020b;\u0001\u0000\u0000\u0000\u020c\u020a\u0001"+
		"\u0000\u0000\u0000\u020d\u020e\u0003>\u001f\u0000\u020e\u020f\u0006\u001e"+
		"\uffff\uffff\u0000\u020f\u021a\u0001\u0000\u0000\u0000\u0210\u0211\u0003"+
		"@ \u0000\u0211\u0212\u0006\u001e\uffff\uffff\u0000\u0212\u021a\u0001\u0000"+
		"\u0000\u0000\u0213\u0214\u0003\u0014\n\u0000\u0214\u0215\u0006\u001e\uffff"+
		"\uffff\u0000\u0215\u021a\u0001\u0000\u0000\u0000\u0216\u0217\u00038\u001c"+
		"\u0000\u0217\u0218\u0006\u001e\uffff\uffff\u0000\u0218\u021a\u0001\u0000"+
		"\u0000\u0000\u0219\u020d\u0001\u0000\u0000\u0000\u0219\u0210\u0001\u0000"+
		"\u0000\u0000\u0219\u0213\u0001\u0000\u0000\u0000\u0219\u0216\u0001\u0000"+
		"\u0000\u0000\u021a=\u0001\u0000\u0000\u0000\u021b\u021c\u0006\u001f\uffff"+
		"\uffff\u0000\u021c\u021d\u0005\u001a\u0000\u0000\u021d\u021e\u0003>\u001f"+
		"\u0006\u021e\u021f\u0006\u001f\uffff\uffff\u0000\u021f\u0229\u0001\u0000"+
		"\u0000\u0000\u0220\u0221\u0003B!\u0000\u0221\u0222\u0006\u001f\uffff\uffff"+
		"\u0000\u0222\u0229\u0001\u0000\u0000\u0000\u0223\u0224\u0005\u0007\u0000"+
		"\u0000\u0224\u0225\u0003<\u001e\u0000\u0225\u0226\u0005\b\u0000\u0000"+
		"\u0226\u0227\u0006\u001f\uffff\uffff\u0000\u0227\u0229\u0001\u0000\u0000"+
		"\u0000\u0228\u021b\u0001\u0000\u0000\u0000\u0228\u0220\u0001\u0000\u0000"+
		"\u0000\u0228\u0223\u0001\u0000\u0000\u0000\u0229\u023b\u0001\u0000\u0000"+
		"\u0000\u022a\u022b\n\u0005\u0000\u0000\u022b\u022c\u0005\u001d\u0000\u0000"+
		"\u022c\u022d\u0003>\u001f\u0006\u022d\u022e\u0006\u001f\uffff\uffff\u0000"+
		"\u022e\u023a\u0001\u0000\u0000\u0000\u022f\u0230\n\u0004\u0000\u0000\u0230"+
		"\u0231\u0007\u0000\u0000\u0000\u0231\u0232\u0003>\u001f\u0005\u0232\u0233"+
		"\u0006\u001f\uffff\uffff\u0000\u0233\u023a\u0001\u0000\u0000\u0000\u0234"+
		"\u0235\n\u0003\u0000\u0000\u0235\u0236\u0007\u0001\u0000\u0000\u0236\u0237"+
		"\u0003>\u001f\u0004\u0237\u0238\u0006\u001f\uffff\uffff\u0000\u0238\u023a"+
		"\u0001\u0000\u0000\u0000\u0239\u022a\u0001\u0000\u0000\u0000\u0239\u022f"+
		"\u0001\u0000\u0000\u0000\u0239\u0234\u0001\u0000\u0000\u0000\u023a\u023d"+
		"\u0001\u0000\u0000\u0000\u023b\u0239\u0001\u0000\u0000\u0000\u023b\u023c"+
		"\u0001\u0000\u0000\u0000\u023c?\u0001\u0000\u0000\u0000\u023d\u023b\u0001"+
		"\u0000\u0000\u0000\u023e\u023f\u0006 \uffff\uffff\u0000\u023f\u0240\u0005"+
		",\u0000\u0000\u0240\u0241\u0003@ \u0002\u0241\u0242\u0006 \uffff\uffff"+
		"\u0000\u0242\u0247\u0001\u0000\u0000\u0000\u0243\u0244\u0003>\u001f\u0000"+
		"\u0244\u0245\u0006 \uffff\uffff\u0000\u0245\u0247\u0001\u0000\u0000\u0000"+
		"\u0246\u023e\u0001\u0000\u0000\u0000\u0246\u0243\u0001\u0000\u0000\u0000"+
		"\u0247\u0254\u0001\u0000\u0000\u0000\u0248\u0249\n\u0004\u0000\u0000\u0249"+
		"\u024a\u0007\u0002\u0000\u0000\u024a\u024b\u0003@ \u0005\u024b\u024c\u0006"+
		" \uffff\uffff\u0000\u024c\u0253\u0001\u0000\u0000\u0000\u024d\u024e\n"+
		"\u0003\u0000\u0000\u024e\u024f\u0007\u0003\u0000\u0000\u024f\u0250\u0003"+
		"@ \u0004\u0250\u0251\u0006 \uffff\uffff\u0000\u0251\u0253\u0001\u0000"+
		"\u0000\u0000\u0252\u0248\u0001\u0000\u0000\u0000\u0252\u024d\u0001\u0000"+
		"\u0000\u0000\u0253\u0256\u0001\u0000\u0000\u0000\u0254\u0252\u0001\u0000"+
		"\u0000\u0000\u0254\u0255\u0001\u0000\u0000\u0000\u0255A\u0001\u0000\u0000"+
		"\u0000\u0256\u0254\u0001\u0000\u0000\u0000\u0257\u0258\u0003F#\u0000\u0258"+
		"\u0259\u0006!\uffff\uffff\u0000\u0259\u0264\u0001\u0000\u0000\u0000\u025a"+
		"\u025b\u00038\u001c\u0000\u025b\u025c\u0006!\uffff\uffff\u0000\u025c\u0264"+
		"\u0001\u0000\u0000\u0000\u025d\u025e\u0003\f\u0006\u0000\u025e\u025f\u0006"+
		"!\uffff\uffff\u0000\u025f\u0264\u0001\u0000\u0000\u0000\u0260\u0261\u0003"+
		"D\"\u0000\u0261\u0262\u0006!\uffff\uffff\u0000\u0262\u0264\u0001\u0000"+
		"\u0000\u0000\u0263\u0257\u0001\u0000\u0000\u0000\u0263\u025a\u0001\u0000"+
		"\u0000\u0000\u0263\u025d\u0001\u0000\u0000\u0000\u0263\u0260\u0001\u0000"+
		"\u0000\u0000\u0264C\u0001\u0000\u0000\u0000\u0265\u0266\u0005.\u0000\u0000"+
		"\u0266\u0267\u0005\u0007\u0000\u0000\u0267\u0268\u0005D\u0000\u0000\u0268"+
		"\u0269\u0005\b\u0000\u0000\u0269\u026a\u0006\"\uffff\uffff\u0000\u026a"+
		"E\u0001\u0000\u0000\u0000\u026b\u026c\u0005>\u0000\u0000\u026c\u027e\u0006"+
		"#\uffff\uffff\u0000\u026d\u026e\u0005?\u0000\u0000\u026e\u027e\u0006#"+
		"\uffff\uffff\u0000\u026f\u0270\u0005B\u0000\u0000\u0270\u027e\u0006#\uffff"+
		"\uffff\u0000\u0271\u0272\u0005C\u0000\u0000\u0272\u027e\u0006#\uffff\uffff"+
		"\u0000\u0273\u0274\u0005D\u0000\u0000\u0274\u027e\u0006#\uffff\uffff\u0000"+
		"\u0275\u0276\u0005A\u0000\u0000\u0276\u027e\u0006#\uffff\uffff\u0000\u0277"+
		"\u0278\u0005@\u0000\u0000\u0278\u027e\u0006#\uffff\uffff\u0000\u0279\u027a"+
		"\u0005\u0011\u0000\u0000\u027a\u027e\u0006#\uffff\uffff\u0000\u027b\u027c"+
		"\u0005\u0012\u0000\u0000\u027c\u027e\u0006#\uffff\uffff\u0000\u027d\u026b"+
		"\u0001\u0000\u0000\u0000\u027d\u026d\u0001\u0000\u0000\u0000\u027d\u026f"+
		"\u0001\u0000\u0000\u0000\u027d\u0271\u0001\u0000\u0000\u0000\u027d\u0273"+
		"\u0001\u0000\u0000\u0000\u027d\u0275\u0001\u0000\u0000\u0000\u027d\u0277"+
		"\u0001\u0000\u0000\u0000\u027d\u0279\u0001\u0000\u0000\u0000\u027d\u027b"+
		"\u0001\u0000\u0000\u0000\u027eG\u0001\u0000\u0000\u0000\u001bN^\u009e"+
		"\u00a8\u00c0\u00d6\u0103\u011f\u012b\u0138\u0142\u0177\u0183\u019e\u01bc"+
		"\u01e9\u01f6\u020a\u0219\u0228\u0239\u023b\u0246\u0252\u0254\u0263\u027d";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}