// Generated from C:/Users/BDGSA/Desktop/ProyectosUsac/Vacaciones Junio/compi2/Proyecto1/olc2vj22_201212854/FortranAnalyzer/src/com\Gramatica.g4 by ANTLR 4.10.1
package Gramatica;

     import java.util.HashMap;
        import Tree.Arbol;
        import Tree.PrintInstruction;
        import Tree.NodeInst;
        import Tree.Operations;
        import Tree.Primitives;
        import Tree.Symbol.Tipo;
        import Tree.Operations.Tipo_Operations;
        import Tree.Assignment;
        import Tree.Declaracion;
        import Tree.Identifier;
        import Tree.For;
        import Tree.DoWhile;
        import Tree.If;
        import java.util.LinkedList;
        import Tree.AccessArray;
        import Tree.AsignArray;
        import Tree.DeclarationArray;
        import Tree.Size;
        import Tree.ParamsFunction;
        import Tree.Function;
        import Tree.CallFunction;
        import Tree.Return;
        import Tree.Allocate;
        import Tree.Deallocate;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link Gramatica}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GramaticaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link Gramatica#initStart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitStart(Gramatica.InitStartContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(Gramatica.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#listFunctions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitListFunctions(Gramatica.ListFunctionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#instruction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstruction(Gramatica.InstructionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(Gramatica.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#list_paramaters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_paramaters(Gramatica.List_paramatersContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#call_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall_function(Gramatica.Call_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(Gramatica.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#list_cont_instruction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_cont_instruction(Gramatica.List_cont_instructionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#content_inst}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContent_inst(Gramatica.Content_instContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#asignation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignation(Gramatica.AsignationContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#variables}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariables(Gramatica.VariablesContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#declareVars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareVars(Gramatica.DeclareVarsContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#type_declare_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_declare_var(Gramatica.Type_declare_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#type_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_var(Gramatica.Type_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#list_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_variable(Gramatica.List_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(Gramatica.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_if(Gramatica.Sentence_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#list_If}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_If(Gramatica.List_IfContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_do}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_do(Gramatica.Sentence_doContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_do_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_do_while(Gramatica.Sentence_do_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_array(Gramatica.Sentence_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_asignacion_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_asignacion_array(Gramatica.Sentence_asignacion_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_asig_array_inicial}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_asig_array_inicial(Gramatica.Sentence_asig_array_inicialContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_allocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_allocate(Gramatica.Sentence_allocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sentence_deallocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentence_deallocate(Gramatica.Sentence_deallocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#dimension_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDimension_array(Gramatica.Dimension_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#dimension_array_dinamyc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDimension_array_dinamyc(Gramatica.Dimension_array_dinamycContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#access_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccess_array(Gramatica.Access_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#list_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_expression(Gramatica.List_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(Gramatica.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#expression_arithmetic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_arithmetic(Gramatica.Expression_arithmeticContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#expression_relational}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_relational(Gramatica.Expression_relationalContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#expression_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_value(Gramatica.Expression_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#sent_size}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSent_size(Gramatica.Sent_sizeContext ctx);
	/**
	 * Visit a parse tree produced by {@link Gramatica#primitivo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitivo(Gramatica.PrimitivoContext ctx);
}