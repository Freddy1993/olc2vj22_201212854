// Generated from C:/Users/BDGSA/Desktop/ProyectosUsac/Vacaciones Junio/compi2/Proyecto1/olc2vj22_201212854/FortranAnalyzer/src/com\Gramatica.g4 by ANTLR 4.10.1
package Gramatica;

     import java.util.HashMap;
        import Tree.Arbol;
        import Tree.PrintInstruction;
        import Tree.NodeInst;
        import Tree.Operations;
        import Tree.Primitives;
        import Tree.Symbol.Tipo;
        import Tree.Operations.Tipo_Operations;
        import Tree.Assignment;
        import Tree.Declaracion;
        import Tree.Identifier;
        import Tree.For;
        import Tree.DoWhile;
        import Tree.If;
        import java.util.LinkedList;
        import Tree.AccessArray;
        import Tree.AsignArray;
        import Tree.DeclarationArray;
        import Tree.Size;
        import Tree.ParamsFunction;
        import Tree.Function;
        import Tree.CallFunction;
        import Tree.Return;
        import Tree.Allocate;
        import Tree.Deallocate;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link Gramatica}.
 */
public interface GramaticaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link Gramatica#initStart}.
	 * @param ctx the parse tree
	 */
	void enterInitStart(Gramatica.InitStartContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#initStart}.
	 * @param ctx the parse tree
	 */
	void exitInitStart(Gramatica.InitStartContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(Gramatica.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(Gramatica.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#listFunctions}.
	 * @param ctx the parse tree
	 */
	void enterListFunctions(Gramatica.ListFunctionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#listFunctions}.
	 * @param ctx the parse tree
	 */
	void exitListFunctions(Gramatica.ListFunctionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#instruction}.
	 * @param ctx the parse tree
	 */
	void enterInstruction(Gramatica.InstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#instruction}.
	 * @param ctx the parse tree
	 */
	void exitInstruction(Gramatica.InstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(Gramatica.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(Gramatica.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#list_paramaters}.
	 * @param ctx the parse tree
	 */
	void enterList_paramaters(Gramatica.List_paramatersContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#list_paramaters}.
	 * @param ctx the parse tree
	 */
	void exitList_paramaters(Gramatica.List_paramatersContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#call_function}.
	 * @param ctx the parse tree
	 */
	void enterCall_function(Gramatica.Call_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#call_function}.
	 * @param ctx the parse tree
	 */
	void exitCall_function(Gramatica.Call_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(Gramatica.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(Gramatica.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#list_cont_instruction}.
	 * @param ctx the parse tree
	 */
	void enterList_cont_instruction(Gramatica.List_cont_instructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#list_cont_instruction}.
	 * @param ctx the parse tree
	 */
	void exitList_cont_instruction(Gramatica.List_cont_instructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#content_inst}.
	 * @param ctx the parse tree
	 */
	void enterContent_inst(Gramatica.Content_instContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#content_inst}.
	 * @param ctx the parse tree
	 */
	void exitContent_inst(Gramatica.Content_instContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#asignation}.
	 * @param ctx the parse tree
	 */
	void enterAsignation(Gramatica.AsignationContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#asignation}.
	 * @param ctx the parse tree
	 */
	void exitAsignation(Gramatica.AsignationContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#variables}.
	 * @param ctx the parse tree
	 */
	void enterVariables(Gramatica.VariablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#variables}.
	 * @param ctx the parse tree
	 */
	void exitVariables(Gramatica.VariablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#declareVars}.
	 * @param ctx the parse tree
	 */
	void enterDeclareVars(Gramatica.DeclareVarsContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#declareVars}.
	 * @param ctx the parse tree
	 */
	void exitDeclareVars(Gramatica.DeclareVarsContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#type_declare_var}.
	 * @param ctx the parse tree
	 */
	void enterType_declare_var(Gramatica.Type_declare_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#type_declare_var}.
	 * @param ctx the parse tree
	 */
	void exitType_declare_var(Gramatica.Type_declare_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#type_var}.
	 * @param ctx the parse tree
	 */
	void enterType_var(Gramatica.Type_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#type_var}.
	 * @param ctx the parse tree
	 */
	void exitType_var(Gramatica.Type_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#list_variable}.
	 * @param ctx the parse tree
	 */
	void enterList_variable(Gramatica.List_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#list_variable}.
	 * @param ctx the parse tree
	 */
	void exitList_variable(Gramatica.List_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(Gramatica.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(Gramatica.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_if}.
	 * @param ctx the parse tree
	 */
	void enterSentence_if(Gramatica.Sentence_ifContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_if}.
	 * @param ctx the parse tree
	 */
	void exitSentence_if(Gramatica.Sentence_ifContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#list_If}.
	 * @param ctx the parse tree
	 */
	void enterList_If(Gramatica.List_IfContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#list_If}.
	 * @param ctx the parse tree
	 */
	void exitList_If(Gramatica.List_IfContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_do}.
	 * @param ctx the parse tree
	 */
	void enterSentence_do(Gramatica.Sentence_doContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_do}.
	 * @param ctx the parse tree
	 */
	void exitSentence_do(Gramatica.Sentence_doContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_do_while}.
	 * @param ctx the parse tree
	 */
	void enterSentence_do_while(Gramatica.Sentence_do_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_do_while}.
	 * @param ctx the parse tree
	 */
	void exitSentence_do_while(Gramatica.Sentence_do_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_array}.
	 * @param ctx the parse tree
	 */
	void enterSentence_array(Gramatica.Sentence_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_array}.
	 * @param ctx the parse tree
	 */
	void exitSentence_array(Gramatica.Sentence_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_asignacion_array}.
	 * @param ctx the parse tree
	 */
	void enterSentence_asignacion_array(Gramatica.Sentence_asignacion_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_asignacion_array}.
	 * @param ctx the parse tree
	 */
	void exitSentence_asignacion_array(Gramatica.Sentence_asignacion_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_asig_array_inicial}.
	 * @param ctx the parse tree
	 */
	void enterSentence_asig_array_inicial(Gramatica.Sentence_asig_array_inicialContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_asig_array_inicial}.
	 * @param ctx the parse tree
	 */
	void exitSentence_asig_array_inicial(Gramatica.Sentence_asig_array_inicialContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_allocate}.
	 * @param ctx the parse tree
	 */
	void enterSentence_allocate(Gramatica.Sentence_allocateContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_allocate}.
	 * @param ctx the parse tree
	 */
	void exitSentence_allocate(Gramatica.Sentence_allocateContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sentence_deallocate}.
	 * @param ctx the parse tree
	 */
	void enterSentence_deallocate(Gramatica.Sentence_deallocateContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sentence_deallocate}.
	 * @param ctx the parse tree
	 */
	void exitSentence_deallocate(Gramatica.Sentence_deallocateContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#dimension_array}.
	 * @param ctx the parse tree
	 */
	void enterDimension_array(Gramatica.Dimension_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#dimension_array}.
	 * @param ctx the parse tree
	 */
	void exitDimension_array(Gramatica.Dimension_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#dimension_array_dinamyc}.
	 * @param ctx the parse tree
	 */
	void enterDimension_array_dinamyc(Gramatica.Dimension_array_dinamycContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#dimension_array_dinamyc}.
	 * @param ctx the parse tree
	 */
	void exitDimension_array_dinamyc(Gramatica.Dimension_array_dinamycContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#access_array}.
	 * @param ctx the parse tree
	 */
	void enterAccess_array(Gramatica.Access_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#access_array}.
	 * @param ctx the parse tree
	 */
	void exitAccess_array(Gramatica.Access_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#list_expression}.
	 * @param ctx the parse tree
	 */
	void enterList_expression(Gramatica.List_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#list_expression}.
	 * @param ctx the parse tree
	 */
	void exitList_expression(Gramatica.List_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(Gramatica.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(Gramatica.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#expression_arithmetic}.
	 * @param ctx the parse tree
	 */
	void enterExpression_arithmetic(Gramatica.Expression_arithmeticContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#expression_arithmetic}.
	 * @param ctx the parse tree
	 */
	void exitExpression_arithmetic(Gramatica.Expression_arithmeticContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#expression_relational}.
	 * @param ctx the parse tree
	 */
	void enterExpression_relational(Gramatica.Expression_relationalContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#expression_relational}.
	 * @param ctx the parse tree
	 */
	void exitExpression_relational(Gramatica.Expression_relationalContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#expression_value}.
	 * @param ctx the parse tree
	 */
	void enterExpression_value(Gramatica.Expression_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#expression_value}.
	 * @param ctx the parse tree
	 */
	void exitExpression_value(Gramatica.Expression_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#sent_size}.
	 * @param ctx the parse tree
	 */
	void enterSent_size(Gramatica.Sent_sizeContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#sent_size}.
	 * @param ctx the parse tree
	 */
	void exitSent_size(Gramatica.Sent_sizeContext ctx);
	/**
	 * Enter a parse tree produced by {@link Gramatica#primitivo}.
	 * @param ctx the parse tree
	 */
	void enterPrimitivo(Gramatica.PrimitivoContext ctx);
	/**
	 * Exit a parse tree produced by {@link Gramatica#primitivo}.
	 * @param ctx the parse tree
	 */
	void exitPrimitivo(Gramatica.PrimitivoContext ctx);
}