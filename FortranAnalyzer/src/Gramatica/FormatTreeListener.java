package Gramatica;

import Tree.Arbol;

public class FormatTreeListener extends GramaticaBaseListener{
    public Arbol ast;

    @Override public void exitInitStart(Gramatica.InitStartContext ctx) {
        this.ast = ctx.ast;
    }
}
