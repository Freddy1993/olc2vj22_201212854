package Tree;

import java.util.HashMap;
import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class If implements NodeInst {
    /**
     * Lista de instrucciones SubIf que serán ejecutadas.
     */
    private NodeInst condition;
    private final LinkedList<NodeInst> list_If;
    private final LinkedList<NodeInst> list_Else;

    public int line = 0;
    public int column = 0;

    public If(NodeInst condition, LinkedList<NodeInst> list_If, LinkedList<NodeInst> list_Else, int line, int column) {
        this.condition = condition;
        this.list_If = list_If;
        this.list_Else = list_Else;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        TableSymbol tablaLocal=new TableSymbol();
        tablaLocal.addAll(ts);

        Object object = this.condition.execute(tablaLocal, ar);
        if(object instanceof Excepcion) {
            return object;
        }
        Boolean condition = (Boolean) object;

        if (condition) {
            for(NodeInst ins: list_If){
                Object r;
                r=ins.execute(tablaLocal,ar);
                if(r instanceof Excepcion) {
                    return r;
                }
                if(r!=null){
                    if(r instanceof Break){
                        return null;
                    }else{
                        return r;
                    }
                }
            }
        } else {
            if (list_Else != null) {
                for(NodeInst ins: list_Else){
                    Object r;
                    r=ins.execute(tablaLocal,ar);
                    if(r instanceof Excepcion) {
                        return r;
                    }
                    if(r!=null){
                        if(r instanceof Break){
                            return null;
                        }else{
                            return r;
                        }
                    }
                }
            }

        }
        return null;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar)
    {
        Object valorCondicion = this.condition.code3D(ts,ar);
        Tipo tipoCondicion = this.condition.getTipoInstrution();

        if(tipoCondicion==Tipo.LOGICAL)
        {
            if(valorCondicion instanceof HashMap<?,?>)
            {
                String LSalida = Utils.generarEtiqueta();
                Utils.printConsole(((HashMap<?, ?>) valorCondicion).get("LV")+":\n");
                for (NodeInst inst: list_If)
                {
                    inst.code3D(ts, ar);
                }
                Utils.printConsole("goto "+LSalida+";\n");
                Utils.printConsole(((HashMap<?, ?>) valorCondicion).get("LF")+":\n");
                if(this.list_Else != null)
                {
                    for (NodeInst inst: list_Else)
                    {
                        inst.code3D(ts, ar);
                    }
                }
                Utils.printConsole(LSalida+":\n");
            }
            else
            {
                String LV = Utils.generateTemp();
                String LF = Utils.generateTemp();
                String LSalida = Utils.generarEtiqueta();
                Utils.printConsole("if("+valorCondicion+"==1) goto "+LV+";\n");
                Utils.printConsole("goto "+LF+";\n");

                Utils.printConsole(LV+":\n");
                for (NodeInst inst: list_If)
                {
                    inst.code3D(ts, ar);
                }
                Utils.printConsole("goto "+LSalida+";\n");
                Utils.printConsole(LF+":\n");
                if(this.list_Else != null)
                {
                    for (NodeInst inst: list_Else)
                    {
                        inst.code3D(ts, ar);
                    }
                }
                Utils.printConsole(LSalida+":\n");

            }
        }

        return null;
    }
}
