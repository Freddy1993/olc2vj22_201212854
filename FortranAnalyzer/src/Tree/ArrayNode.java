package Tree;

import Tree.Symbol.Tipo;
import java.util.LinkedList;

public class ArrayNode {

    private final LinkedList<ArrayNode> celdasVecinas;

    private Object valor;
    private Tipo tipo;

    public ArrayNode() {
        this.celdasVecinas = new LinkedList<>();
        this.valor = new Nulo();
    }

    public void inicializarNodo(int cantDimensiones, int dimensionActual, LinkedList<Integer> tamaniosDimensiones){
        if(dimensionActual>cantDimensiones){
            return;
        }
        for (int i = 0; i < tamaniosDimensiones.get(dimensionActual-1) ; i++) {
            ArrayNode arr=new ArrayNode();
            arr.setTipo(tipo);
            celdasVecinas.add(arr);
            arr.inicializarNodo(cantDimensiones, dimensionActual+1, tamaniosDimensiones);            
        }
    }

    public Object setValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, Object val, String id){
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual >= 0){
            ArrayNode arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                // Validar que el valor a insertar sea del mismo tipo del arreglo
                if(val instanceof Integer && tipo != Tipo.ENTERO
                        || val instanceof Double && tipo != Tipo.REAL
                        || val instanceof Boolean && tipo != Tipo.LOGICAL
                        || val instanceof String && tipo != Tipo.CHARACTER){
                    Excepcion error = new Excepcion("Esta intentando insertar un valor de tipo "+val.getClass().getSimpleName()+" al arreglo de tipo "+tipo, 0, 0);
                    return error;
                }
                arr.valor=val;
            }else{
                Object obj = arr.setValor(cantIndices, indiceActual+1, indices, val, id);
                return obj;
            }
        }else{
            Excepcion error = new Excepcion("La asignación del arreglo "+id+" no se puede realizar porque no esta en el limite", 0, 0);
            return error;
        }

        return null;
    }
    /**
     * Método que recoge cierto valor en una celda específica del arreglo y 
     * devuelve nulo cuando no lo encuentra
     * @param cantIndices Cantidad de indices que se reciben para el acceso al arreglo
     * @param indiceActual Indice que se está analizando en la propagación actual
     * @param indices Lista de los indices con los que se accederá al arreglo para asignar el valor
     * @param id Identificador del arreglo
     * @return El valor almacenado por la celda específica o null en caso no lo encuentre
     */
    Object getValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, String id) {
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual >= 0){
            ArrayNode arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                Object val = arr.valor;
                return val == null ? new Nulo() : val;
            }else{
                return arr.getValor(cantIndices, indiceActual+1, indices, id);
            }
        }else{
            System.err.println("El acceso al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.");
            Excepcion error = new Excepcion("El acceso al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.", 0, 0);
            return error;
        }
    }

    public Object getValorCeldas() {
        return this.valor;
    }
    public Tipo getTipo() {
        return tipo;
    }
    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    public LinkedList<ArrayNode> getCeldasVecinas() {
        return celdasVecinas;
    }
}
