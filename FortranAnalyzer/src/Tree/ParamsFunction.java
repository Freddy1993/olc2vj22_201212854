
package Tree;

import Tree.Symbol.*;
import java.util.LinkedList;

public class ParamsFunction implements NodeInst {

    protected boolean parametro;

    protected String id;

    protected Tipo tipo;

    protected Tipo tipoEntrada;

    protected NodeInst valor;

    public LinkedList<NodeInst> variables;

    public Object valorDefault = 0;

    public int line = 0;
    public int column = 0;
    public boolean is3d = false;

    public ParamsFunction(String a, String b, NodeInst c, int line, int column) {
        id=a;
        String reservadaTipo=b.toLowerCase();
        switch (reservadaTipo) {
            case "number": tipo=Symbol.Tipo.ENTERO;
                break;
            case "string":  tipo=Symbol.Tipo.CHARACTER;
                break;
            case "boolean": tipo=Symbol.Tipo.LOGICAL;
                break;
            default:
                tipo=null;
        }
        parametro=false;
        this.valor = c;
        this.line = line;
        this.column = column;
    }

    public ParamsFunction(String a, Tipo b, NodeInst c, int line, int column) {
        id=a;
        tipo = b;
        parametro=false;
        this.valor = c;
        this.line = line;
        this.column = column;
    }

    public ParamsFunction(LinkedList<NodeInst> variables, Tipo tipo, Tipo tipoIn, int line, int column) {
        this.variables = variables;
        this.tipo = tipo;
        parametro=false;
        tipoEntrada = tipoIn;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        for(NodeInst instruccion: this.variables) {
            if(instruccion instanceof Identifier){
                Identifier id = (Identifier) instruccion;
                if(ts.existeVariable(id.identifier)) {
                    Object instr = instruccion.execute(ts,ar);
                    if(instr instanceof Excepcion) {
                        return instr;
                    }
                    Symbol aux=new Symbol(id.identifier,tipo);
                    aux.setParametroInicializado(this.is3d);
                    aux.setParametro(this.parametro);
                    aux.setIndice(ts.size()+1);
                    Object obj = aux.setValor(instr);
                    if(obj instanceof Boolean) {
                        boolean asignacionCorrecta = false;
                        asignacionCorrecta = (Boolean) obj;
                        if(asignacionCorrecta)
                            ts.add(aux);
                    }else if(obj instanceof Excepcion) {
                        Excepcion error = (Excepcion) obj;
                        error.line = line;
                        error.column = column;
                        ar.excepciones.add(error);
                        ar.console.add(error.description);
                        return error;
                    }
                } else {
                    Object instr = instruccion.execute(ts,ar);
                    if(instr instanceof Excepcion) {
                        return instr;
                    }
                    Symbol aux=new Symbol(instr.toString(),tipo);
                    aux.setParametroInicializado(this.is3d);
                    aux.setParametro(this.parametro);
                    aux.setIndice(ts.size()+1);
                    Object obj = aux.setValor(getValorDefault());
                    if(obj instanceof Boolean) {
                        boolean asignacionCorrecta = false;
                        asignacionCorrecta = (Boolean) obj;
                        if(asignacionCorrecta)
                            ts.add(aux);
                    }else if(obj instanceof Excepcion) {
                        Excepcion error = (Excepcion) obj;
                        error.line = line;
                        error.column = column;
                        ar.excepciones.add(error);
                        ar.console.add(error.description);
                        return error;
                    }
                }
            } else if(instruccion instanceof Assignment) {
                Assignment instrAsignacion = (Assignment) instruccion;
                Symbol aux = new Symbol(instrAsignacion.id,tipo);
                aux.setParametro(this.parametro);
                Object val = instrAsignacion.valor.execute(ts, ar);
                Object obj = aux.setValor(val);
                if(obj instanceof Boolean) {
                    boolean asignacionCorrecta = false;
                    asignacionCorrecta = (Boolean) obj;
                    if(asignacionCorrecta){
                        ts.add(aux);
                    }
                } else if(obj instanceof Excepcion) {
                    Excepcion error = (Excepcion) obj;
                    error.line = line;
                    error.column = column;
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
            }
        }

        return null;
    }

    public String getIdentificador() {
        return id;
    }

    public boolean isParametro() {
        return parametro;
    }

    public void setParametro(boolean parametro) {
        this.parametro = parametro;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    public Object getValorDefault() {
        if (this.tipo == Tipo.ENTERO) {
            return 0;
        } else if (this.tipo == Tipo.REAL) {
            return 0.00000000;
        } else if (this.tipo == Tipo.COMPLEX) {
            return 0.00000000;
        } else if (this.tipo == Tipo.CHARACTER) {
            return "";
        } else if (this.tipo == Tipo.LOGICAL) {
            return false;
        }

        return "";
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }
    public void setIs3d(boolean val){this.is3d = val;}
}
