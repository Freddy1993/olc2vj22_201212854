package Tree;

public class Excepcion {

    public String description;
    public int line;
    public int column;
    public String tipo;

    public Excepcion(String description, int line, int column) {
        this.description = description;
        this.line = line;
        this.column = column;
        this.tipo = "Semántico";
    }

    public int getLine() {
        return this.line;
    }

    public int getColumn() {
        return this.column;
    }

    public String getDescription() {
        return this.description;
    }

    public String getTipo() {
        return this.tipo;
    }
}
