package Tree;

import Tree.Symbol.*;
import Utils.Utils;

import java.util.HashMap;

public class Assignment implements NodeInst {

    protected TableSymbol tablaPadre;
    protected final String id;

    protected final NodeInst valor;

    public int line = 0;
    public int column = 0;

    public Assignment(String a, NodeInst b, int line, int column) {
        this.id=a;
        this.valor=b;
        this.line = line;
        this.column = column;
    }
    
    public void setTablaDeSimbolosPadre(TableSymbol ts) {
        this.tablaPadre = ts;
    }

    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        Object obj;
        if(tablaPadre != null) {
            obj = ts.setValor(id,valor.execute(tablaPadre,ar));
        }
        else {
            obj = ts.setValor(id, valor.execute(ts,ar));
        }
        if(obj instanceof Excepcion) {
            Excepcion error = (Excepcion) obj;
            error.line = this.line;
            error.column = this.column;
            ar.excepciones.add(error);
            ar.console.add(error.description);
            return error;
        }
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        Symbol VarBuscada = (Symbol) ts.getValor(this.id);
        if(VarBuscada == null)
        {
            return null;
        }
        else
        {
            Object valorExpresion = valor.code3D(ts,ar);
            Tipo tipoExpresion = valor.getTipoInstrution();
            if(tipoExpresion == VarBuscada.getTipo()) {
                // Si son iguales no hay problema
                Symbol variableLocal = VarBuscada;
                if (VarBuscada.getTipo() == Tipo.LOGICAL)
                {
                    if (valorExpresion instanceof HashMap<?,?>)
                    {
                        String t100 = Utils.generateTemp();
                        String LS = Utils.generarEtiqueta();
                        Utils.printConsole(((HashMap<?, ?>) valorExpresion).get("LV")+ ":\n");
                        Utils.printConsole(t100 + "=1;\n");
                        Utils.printConsole("goto " + LS + ";\n");
                        Utils.printConsole(((HashMap<?, ?>) valorExpresion).get("LF") + ":\n");
                        Utils.printConsole(t100 + "=0;\n");
                        Utils.printConsole(LS + ":\n");
                        valorExpresion = t100;
                    }
                    if(variableLocal != null)
                    {
                        String t0 = Utils.generateTemp();
                        Utils.printConsole(t0+"=P+"+VarBuscada.getIndice()+";\n");
                        Utils.printConsole("stack[(int)"+t0+"]="+valorExpresion+";\n");
                    }
                    else
                    {
                        Utils.printConsole("stack[(int)"+VarBuscada.getIndice()+"]="+valorExpresion+";\n");
                    }
                }
                else if(true)
                {

                    if(variableLocal != null)
                    {
                        String t0 = Utils.generateTemp();
                        Utils.printConsole(t0+"=P+"+VarBuscada.getIndice()+";\n");
                        Utils.printConsole("stack[(int)"+t0+"]="+valorExpresion+";\n");
                    }
                    else
                    {
                        String t0 = Utils.generateTemp();
                        String t1 = Utils.generateTemp();
                        Utils.printConsole(t1+"=P-"+ts.size()+";// Simular cambio de entorno global\n");
                        Utils.printConsole(t0+"="+t1+'+'+VarBuscada.getIndice()+";\n");
                        Utils.printConsole("stack[(int)"+t0+"]="+valorExpresion+";\n");
                    }
                }
            }
        }
        return null;
    }

}
