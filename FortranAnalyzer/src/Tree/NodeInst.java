
package Tree;
import Tree.Symbol.*;
public interface NodeInst {

    public Object execute(TableSymbol ts,Arbol ar);

    public Tipo getTipoInstrution();

    public int getLine();

    public int getColumn();

    public Object code3D(TableSymbol ts, Arbol ar);

}
