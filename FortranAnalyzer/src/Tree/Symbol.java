
package Tree;

import Utils.Utils;

import java.util.LinkedList;

public class Symbol {
    /**
     * Bandera que indica si el símbolo creado es un parámetro o no.
     */
    private boolean parametro;
    /**
     * Si el símbolo es un parámetro y además ya se le asigno valor esta bandera es verdadera, de lo contrario es falsa.
     */
    private boolean parametroInicializado;
    /**
     * Tipo del símbolo que se almacena
     */
    public Tipo tipo;
    /**
     * Identificador del símbolo que se almacena
     */
    private final String id;
    /**
     * Lista de los tamaños de las dimensiones del arreglo, en caso de que lo que
     * se almacene sea un arreglo
     */
    private LinkedList<Integer> tamaniosDimensiones;
    /**
     * Variable que almacena el valor del símbolo para la operación de ejecución
     */
    private Object valor;

    private int indice;

    public enum Rol {VAR, VOID, FUNCTION, SUBRUTINE, ARRAY}
    private Rol rol;
    private String ambito;
    /**
     * Constructor de la clase Símbolo.
     * @param id identificador de la variable que se desea almacenar
     * @param tipo tipo de la variable que se desea almacenar
     */
    public Symbol(String id, Tipo tipo) {
        this.tipo = tipo;
        this.id = id;
        this.tamaniosDimensiones = null;
        this.parametro=false;
        this.parametroInicializado=false;
        this.ambito = Utils.getAmbit();
    }
    /**
     * Constructor para los símbolos que almacenan un arreglo
     * @param id Identificador del arreglo
     * @param tipo Tipo del arreglo
     * @param td
     */
    public Symbol(String id, Tipo tipo, LinkedList<Integer> td) {
        this.tipo = tipo;
        this.id = id;
        this.tamaniosDimensiones = td;
        ArrayNode arr=new ArrayNode();
        arr.setTipo(tipo);
        arr.inicializarNodo(tamaniosDimensiones.size(), 1, tamaniosDimensiones);
        this.valor=arr;
        this.ambito = Utils.getAmbit();
    }
    /**
     * Método que devuelve el identificador de la variable almacenada en el símbolo.
     * @return Identificador de la variable
     */
    public String getId() {
        return id;
    }

    public Object getValor() {
        return valor;
    }

    Object setValor(Object v) {
        Tipo vTipo=null;
        boolean retorno = false;
        if(v == null) {
            return false;
        }

        if (v instanceof Double) {
            vTipo = Symbol.Tipo.REAL;
        } else if(v instanceof String) {
            vTipo = Symbol.Tipo.CHARACTER;
        } else if(v instanceof Boolean){
            vTipo = Symbol.Tipo.LOGICAL;
        } else if(v instanceof Integer){
            vTipo = Symbol.Tipo.ENTERO;
            if(tipo == Tipo.REAL) {
                v = new Double(v.toString());
                vTipo = Symbol.Tipo.REAL;
            }
        } else if(v == ".true."){
            vTipo = Symbol.Tipo.LOGICAL;
        } else if(v == ".false."){
            vTipo = Symbol.Tipo.LOGICAL;
        }
        if(v instanceof ArrayNode && valor instanceof ArrayNode){
            ArrayNode arregloNuevo = (ArrayNode) v;
            ArrayNode arregloActual = (ArrayNode) this.valor;
            vTipo = arregloNuevo.getTipo();
            if(verificarDimensionesArreglo(arregloActual, arregloNuevo)){
                if(vTipo == tipo){
                    valor=v;
                    retorno = true;
                }else{
                    Excepcion error = new Excepcion("Esta intentando asignar un arreglo de "
                            + "tipo " + arregloNuevo.getTipo()
                            + " a un arreglo ["+id+"] de tipo " + arregloActual.getTipo(), 0, 0);
                    return error;
                }
            }else{
                Excepcion error = new Excepcion("Las dimensiones del arreglo a asignar no"
                        + " coinciden con las dimensiones del arreglo ["+id+"]", 0, 0);
                return error;
            }
        }else if(!(v instanceof ArrayNode) && !(valor instanceof ArrayNode)){
            if(vTipo == tipo){
                valor=v;
                retorno = true;
            }else{
                Excepcion error = new Excepcion("Esta intentando asignar un valor de tipo ["+(vTipo==null?"null":vTipo.name())+"] a la variable ["+id+"] de tipo ["+tipo.name()+ "], esto no está permitido.", 0, 0);
                return error;
            }
        }else{

            if(!(v instanceof ArrayNode) && valor instanceof ArrayNode){
                Excepcion error = new Excepcion("Esta intentando asignar un valor de tipo ["+(vTipo==null?"null":vTipo.name())+"] a la variable [Arreglo "+id+"] de tipo ["+tipo.name()+ "], esto no está permitido.", 0, 0);
                return error;
            }else{
                Excepcion error = new Excepcion("Esta intentando asignar un valor de tipo [Arreglo] a la variable ["+id+"] de tipo ["+tipo.name()+ "], esto no está permitido.", 0, 0);
                return error;
            }
        }
        return retorno;
    }

    Object setValor(Object val, LinkedList<Integer> indices) {
        if(this.valor instanceof ArrayNode){
            if(this.tamaniosDimensiones.size()==indices.size()){
                ArrayNode arr=(ArrayNode)this.valor;
                Object obj = arr.setValor(indices.size(), 1, indices, val, id);
                if(obj instanceof Excepcion) {
                    return obj;
                }
            }else{
                Excepcion error = new Excepcion("La cantidad de indices indicados no "
                        + "coincide con la cantidad de dimensiones del arreglo "+id+", no puede asignársele un valor.", 0, 0);
                return error;
            }
        }else{
            Excepcion error = new Excepcion("La variable "+id+" no es un arreglo, por lo "
                    + "que no puede asignársele un valor de esta manera.", 0, 0);
            return error;
        }

        return null;
    }

    Object getValor(String id, LinkedList<Integer> indices) {
        if(this.valor instanceof ArrayNode){
            if(this.tamaniosDimensiones.size()==indices.size()){
                ArrayNode arr=(ArrayNode)this.valor;
                return arr.getValor(indices.size(), 1, indices, id);
            }else{
                Excepcion error = new Excepcion("La cantidad de indices indicados no "
                        + "coincide con la cantidad de dimensiones del arreglo "+id+", no puede accederse a este arreglo.", 0, 0);
                return error;
            }
        }else{
            Excepcion error = new Excepcion("La variable "+id+" no es un arreglo, por lo "
                    + "que no se puede accesar de esta manera.", 0, 0);
            return error;
        }
    }

    boolean verificarDimensionesArreglo(ArrayNode Actual, ArrayNode Nuevo){
        LinkedList<ArrayNode> arregloActual = Actual.getCeldasVecinas();
        LinkedList<ArrayNode> arregloNuevo = Nuevo.getCeldasVecinas();
        if(arregloActual.size() == arregloNuevo.size()){
            if(arregloActual.size() == 0){
                return true;
            }else{
                return verificarDimensionesArreglo(arregloActual.get(0), arregloNuevo.get(0));
            }
        }
        return false;
    }

    /**
     * Enumeración que lista todos los tipos de variable reconocidos en el lenguaje.
     */
    public static enum Tipo{
        ENTERO,
        REAL,
        COMPLEX,
        CHARACTER,
        LOGICAL,
        IDENTIFICADOR,
        VOID,
        IN,
        OUT,
        INOUT,
        EXIT,
        CYCLE,
        ARRAY,
    }
    /**
     * Método que devuelve el valor de la bandera parámetro.
     * @return
     */
    public boolean isParametro() {
        return parametro;
    }
    /**
     * Método con el que se configura el valor de la bandera parámetro.
     * @param parametro
     */
    public void setParametro(boolean parametro) {
        this.parametro = parametro;
    }
    /**
     * Método que devuelve el valor de la bandera parámetro inicializado.
     * @return
     */
    public boolean isParametroInicializado() {
        return parametroInicializado;
    }
    /**
     * Método con el que se configura el valor de la bandera parámetro inicializado.
     * @param parametroInicializado
     */
    public void setParametroInicializado(boolean parametroInicializado) {
        this.parametroInicializado = parametroInicializado;
    }

    public Tipo getTipo() {
        return this.tipo;
    }

    public LinkedList<Integer> getTamaniosDimensiones() {
        return this.tamaniosDimensiones;
    }

    public void setTamaniosDimensiones(LinkedList<Integer> newDimensions) {
        this.tamaniosDimensiones = newDimensions;
        ArrayNode arr=new ArrayNode();
        arr.setTipo(tipo);
        arr.inicializarNodo(tamaniosDimensiones.size(), 1, tamaniosDimensiones);
        this.valor=arr;
    }

    public Rol getRol(){return this.rol;}
    public int getIndice(){return this.indice;}
    public String getAmbito(){return this.ambito;}
    public void setIndice(int indice){this.indice = indice;}
    public void setAmbito(String ambito){this.ambito  = ambito;}
    public void setRol(Rol rol){this.rol = rol;}
}