package Tree;

import Tree.Symbol.*;
import Utils.Utils;

public class Primitives implements NodeInst {
    private Object valor;
    public Tipo tipo;
    public Tipo subtipo;
    public int line;
    public int colum;

    public Primitives(Object valor, Tipo tipo){
        this.valor = valor;
        this.tipo = tipo;
        if(this.tipo == Tipo.ENTERO ) {
            this.subtipo = Tipo.ENTERO;
        } else if (this.tipo == Tipo.REAL) {
            this.subtipo = Tipo.REAL;
        } else if (this.tipo == Tipo.LOGICAL) {
            this.subtipo = Tipo.LOGICAL;
        } else if (this.tipo == Tipo.CHARACTER) {
            this.subtipo = Tipo.CHARACTER;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.IDENTIFICADOR;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.EXIT;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.CYCLE;
        }
    }

    public Primitives(Object valor, Tipo tipo, int line, int column){
        this.valor = valor;
        this.tipo = tipo;
        if(this.tipo == Tipo.ENTERO ) {
            this.subtipo = Tipo.ENTERO;
        } else if (this.tipo == Tipo.REAL) {
            this.subtipo = Tipo.REAL;
        } else if (this.tipo == Tipo.LOGICAL) {
            this.subtipo = Tipo.LOGICAL;
        } else if (this.tipo == Tipo.CHARACTER) {
            this.subtipo = Tipo.CHARACTER;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.IDENTIFICADOR;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.EXIT;
        } else if (this.tipo == Tipo.IDENTIFICADOR) {
            this.subtipo = Tipo.CYCLE;
        }
        this.line = line;
        this.colum = column;
    }


    @Override
    public Tipo getTipoInstrution() {
        return this.tipo;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.colum;
    }

    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        if(this.subtipo == Tipo.ENTERO) {
            return new Integer(valor.toString());
        } else if (subtipo == Tipo.REAL) {
            return new Double(valor.toString());
        } else if (subtipo == Tipo.CHARACTER) {
            return new String(valor.toString().replace("'", "").replace("\"", ""));
        } else if (subtipo == Tipo.LOGICAL){
            if(this.valor.toString().equals(".true.")) {
                return new Boolean(true);
            } else {
                return new Boolean(false);
            }
        } else if(subtipo == Tipo.IDENTIFICADOR) {
            Object obj = ts.getValor(valor.toString());
            if(obj instanceof Symbol) {
                Symbol variable = (Symbol) obj;
                this.tipo = variable.getTipo();
                return variable.getValor();
            } else if(obj instanceof Excepcion){
                Excepcion error = (Excepcion) obj;
                error.line = this.line;
                error.column = this.colum;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        }
        //return valor.toString();
        return null;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar)
    {
        try {
            if(subtipo == Tipo.IDENTIFICADOR)
            {
                Symbol variable = (Symbol) ts.getValor(this.valor.toString());
                this.tipo = variable.getTipo();
                if(variable != null)
                {
                    //return varBuscada.valor;
                    String t0 = Utils.generateTemp();
                    String t1 = Utils.generateTemp();

                    Utils.printConsole(t0+"=P+"+variable.getIndice()+";//Direccion variable "+this.valor.toString()+"\n");
                    Utils.printConsole(t1+"=stack[(int)"+t0+"];\n");
                    return t1;
                }
                return null;
            }
            if(this.tipo == Tipo.ENTERO)
            {
                return valor;
            } else if (tipo == Tipo.REAL)
            {
                return valor;
            } else if (this.tipo == Tipo.CHARACTER)
            {

                if(valor.toString().length()==3)
                {
                    return (int)valor.toString().toCharArray()[1];
                }
                else if(valor.toString().length()>3)
                {
                    String t0 = Utils.generarCadena3D(valor.toString().substring(1,valor.toString().length()-1));
                    this.tipo = Tipo.VOID;
                    return t0;
                }
                return 0;

            } else if (tipo == Tipo.LOGICAL)
            {
                if(this.valor.toString().equals(".true."))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }


            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
