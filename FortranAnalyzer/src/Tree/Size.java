package Tree;

import Tree.Symbol.*;

public class Size implements NodeInst {
    private String identifier;

    public int line = 0;
    public int column = 0;
    /**
     * Constructor de la clase imprimir
     * @param identifier contenido que será impreso al ejecutar la instrucción
     */
    public Size(String identifier, int line, int column) {
        this.identifier = identifier;
        this.line = line;
        this.column = column;
    }
    /**
     * Método que ejecuta la accion de imprimir un valor, es una sobreescritura del
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts Tabla de símbolos del ámbito padre de la sentencia.
     * @return Esta instrucción retorna nulo porque no produce ningun valor al ser
     * ejecutada.
     */
    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        Object object = ts.getSimboloArreglo(this.identifier);
        if(object instanceof Symbol) {
            Symbol obj = (Symbol)object;
            if(obj != null) {
                int contadorsize = 1;
                for (int i = 0; i < obj.getTamaniosDimensiones().size(); i++) {
                    contadorsize *= obj.getTamaniosDimensiones().get(i);
                }
                return contadorsize;
            }
        } else {
            Excepcion error = (Excepcion) object;
            error.line = line;
            error.column = column;
            ar.excepciones.add(error);
            ar.console.add(error.description);
            return error;
        }

        return null;
    }

    @Override
    public Tipo getTipoInstrution() {
        return Tipo.ENTERO;
    }

    @Override
    public int getLine() {
        return this.line;
    }
    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }

}
