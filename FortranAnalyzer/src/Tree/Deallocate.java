package Tree;

import java.util.LinkedList;

public class Deallocate implements NodeInst {

    public String identifier;
    public int line = 0;
    public int column = 0;

    public Deallocate(String id, int line, int column) {
        this.identifier = id;
        this.line = line;
        this.column = column;
    }
    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        if(ts.existeVariable(this.identifier)) {
            Object obj = ts.getSimboloArreglo(this.identifier);
            if(obj instanceof Symbol) {
                Symbol arreglo = (Symbol) obj;
                LinkedList<Integer> valoresIndices=new LinkedList<>();
                arreglo.setTamaniosDimensiones(valoresIndices);
            } else {
                Excepcion error = (Excepcion) obj;
                error.line = line;
                error.column = column;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        }
        return null;
    }


    @Override
    public Symbol.Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }

}
