
package Tree;


import java.util.LinkedList;

public class AsignArray extends Assignment implements NodeInst {
    private final LinkedList<NodeInst> indices;
    private Boolean asignacionArregloInicial;

    public int line = 0;
    public int column = 0;
    public AsignArray(String a, NodeInst b, LinkedList<NodeInst> c, int line, int column) {
        super(a,b, line, column);
        indices=c;
        this.asignacionArregloInicial = false;
        this.line = line;
        this.column = column;
    }
    public AsignArray(String a, LinkedList<NodeInst> c, int line, int column) {
        super(a,null, line, column);
        indices=c;
        this.asignacionArregloInicial = true;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        if (!this.asignacionArregloInicial) {
            LinkedList<Integer> valoresIndices=new LinkedList<>();
            for (NodeInst dim : indices) {
                Object er=dim.execute(ts, ar);
                if(er instanceof Excepcion) {
                    return er;
                }
                if(!((er instanceof Double) || (er instanceof Integer))){
                    Excepcion error = new Excepcion("Los indices para asignar un valor a un arreglo deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.", line, column);
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
                if (er instanceof Integer) {
                    valoresIndices.add(((Integer)er).intValue());
                } else if (er instanceof Double) {
                    valoresIndices.add(((Double)er).intValue());
                }

            }
            Object val = valor.execute(ts,ar);
            if(val instanceof Excepcion) {
                return val;
            }
            Object obj = ts.setValor(id,val,valoresIndices);
            if(obj instanceof Excepcion) {
                Excepcion error = (Excepcion) obj;
                error.line = line;
                error.column = column;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else {
            Object obj = ts.getSimboloArreglo(id);
            if(obj instanceof Symbol) {
                Symbol arreglo = (Symbol) obj;
                int contador = 0;
                for (NodeInst dim : indices) {
                    LinkedList<Integer> valoresIndices=new LinkedList<>();
                    Object er=dim.execute(ts, ar);
                    if(er instanceof Excepcion) {
                        return er;
                    }
                    valoresIndices.add(((Integer)contador).intValue());
                    Object objs = ts.setValor(id,er,valoresIndices);
                    if(objs instanceof Excepcion) {
                        Excepcion error = (Excepcion) objs;
                        error.line = line;
                        error.column = column;
                        ar.excepciones.add(error);
                        ar.console.add(error.description);
                        return error;
                    }
                    contador += 1;
                }
            } else {
                Excepcion error = (Excepcion) obj;
                error.line = this.line;
                error.column = this.column;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        }

        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }
}
