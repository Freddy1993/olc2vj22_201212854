package Tree;

import java.util.LinkedList;

public class TableSymbol extends LinkedList<Symbol>{
    public TableSymbol() {
        super();
    }
    Object getValor(String id) {
        for (int i = this.size()-1; i >= 0; i--) {
            Symbol s=this.get(i);
            if(s.isParametro() && s.isParametroInicializado() || !s.isParametro()){
                if(s.getId().equals(id)){
                    //Object aux=s.getValor();
                    //return aux;
                    return s;
                }
            }
        }
        System.out.println("La variable "+id+" no existe en este ámbito.");
        Excepcion error = new Excepcion("La variable "+id+" no existe en este ámbito.", 0, 0);
        return error;
    }

    Boolean existeVariable(String id) {
        for (int i = this.size()-1; i >= 0; i--) {
            Symbol s=this.get(i);
            if(s.isParametro() && s.isParametroInicializado() || !s.isParametro()){
                if(s.getId().equals(id)){
                    return true;
                }
            }
        }
        return false;
    }

    Object getValor(String id, LinkedList<Integer> indices) {
        for(Symbol s:this){
            if(s.getId().equals(id)){
                return s.getValor(id, indices);
            }
        }
        Excepcion error = new Excepcion("La variable "+id+" no existe en este ámbito.", 0, 0);
        return error;
    }

    Object getSimboloArreglo(String id) {
        for(Symbol s:this){
            if(s.getId().equals(id)){
                return s;
            }
        }
        Excepcion error = new Excepcion("La variable "+id+" no existe en este ámbito.", 0, 0);
        return error;
    }

    /**
     * Método que asigna un valor a una variable específica, si no la encuentra
     * no realiza la asignación y despliega un mensaje de error.
     * @param id Identificador de la variable que quiere buscarse
     * @param valor Valor que quiere asignársele a la variable buscada
     */
    Object setValor(String id, Object valor) {
        for (int i = this.size()-1; i >= 0; i--) {
            Symbol s=this.get(i);
            if(s.getId().equals(id)){
                Object obj = s.setValor(valor);
                if(obj instanceof Excepcion) {
                    return obj;
                }
                return null;
            }
        }
        Excepcion error = new Excepcion("La variable "+id+" no existe en este ámbito, por lo "
                + "que no puede asignársele un valor.", 0, 0);
        return error;
    }
    /**
     * Método que asigna un valor a cierta celda de un arreglo.
     * @param id Identificador del arreglo
     * @param valor Valor que se desea asignar a la celda del arreglo
     * @param indices Indices para acceder a la celda del arreglo
     */
    Object setValor(String id, Object valor, LinkedList<Integer> indices) {
        for(Symbol s:this){
            if(s.getId().equals(id)){
                Object obj = s.setValor(valor,indices);
                if(obj instanceof Excepcion) {
                    return obj;
                }
                return null;
            }
        }
        Excepcion error = new Excepcion("La variable "+id+" no existe en este ámbito, por lo "
                + "que no puede asignársele un valor.", 0, 0);
        return error;
    }
    /**
     * Méotodo que marca como inicializado el último parámetro agregado con el nombre de identificador indicado.
     * @param id
     */
    Object setParametroInicializado(String id) {
        for (int i = this.size()-1; i >= 0; i--) {
            Symbol s=this.get(i);
            if(s.getId().equals(id)){
                s.setParametroInicializado(true);
                return null;
            }
        }
        Excepcion error = new Excepcion("El parámtro "+id+" que quiere marcar como inicializado no existe en este ámbito, por lo "
                + "que no puede marcar.", 0, 0);
        return error;
    }

    int getSize() {
        return this.size();
    }
}