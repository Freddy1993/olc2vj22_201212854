package Tree;

import Tree.Symbol.*;
import Utils.Utils;

public class Return implements NodeInst {
    /**
     * Lista de las instrucciones (Funciones y declaraciones de variables globales) que componen el archivo.
     */
    public final NodeInst valorRetorno;
    public String etiquetaSalida;
    public int line = 0;
    public int column = 0;
    /**
     * Constructor para un retorno que no es de tipo VOID
     * @param a Instrucción que contiene el valor de retorno de la función
     */
    public Return(NodeInst a, int line, int column) {
        valorRetorno=a;
        this.line = line;
        this.column = column;
    }
    /**
     * Constructor para un retorno que es de tipo VOID
     */
    public Return() {
        valorRetorno=null;
    }
    /**
     * Método que ejecuta la instrucción retorno, es una sobreescritura del
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia
     * @return Esta instrucción retorna el valor producido por la operación que se ejecutó
     */
    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        if(valorRetorno==null){
            return this;
        }else{
            return valorRetorno.execute(ts, ar);
        }
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {

        String t0 = Utils.generateTemp();
        Utils.printConsole(t0+"=P+0; // Direccion retorno\n");
        Object valor = this.valorRetorno.code3D(ts,ar);
        Utils.printConsole("stack[(int)"+t0+"]="+valor+";//Set valor retorno\n");
        Utils.printConsole("goto "+this.etiquetaSalida+";//Etiqueta salida\n");
        return null;
    }

    public void setEtiquetaSalida(String etiqueta){this.etiquetaSalida = etiqueta;}
}
