package Tree;

import java.util.LinkedList;

public class Allocate implements NodeInst {

    private NodeInst contenido;

    private LinkedList<NodeInst> dimensiones;
    public String identifier;

    public int line = 0;
    public int column = 0;

    public Allocate(String id, LinkedList<NodeInst> dimensions, int line, int column) {
        this.identifier = id;
        this.dimensiones = dimensions;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        if(ts.existeVariable(this.identifier)) {
            Object obj = ts.getSimboloArreglo(this.identifier);
            if(obj instanceof Symbol) {
                Symbol arreglo = (Symbol) obj;
                LinkedList<Integer> valoresIndices=new LinkedList<>();
                for (NodeInst dim : dimensiones) {
                    Object er = dim.execute(ts, ar);
                    if(er instanceof Excepcion) {
                        return er;
                    }
                    if(!((er instanceof Double) || (er instanceof Integer))){
                        Excepcion error = new Excepcion("Los indices para asignar un valor a un arreglo deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.", line, column);
                        ar.excepciones.add(error);
                        ar.console.add(error.description + " Linea: " +  line + " Columna" + column);
                        return error;
                    }
                    if (er instanceof Integer) {
                        valoresIndices.add(((Integer)er).intValue());
                    } else if (er instanceof Double) {
                        valoresIndices.add(((Double)er).intValue());
                    }
                }

                if(arreglo.getTamaniosDimensiones().size() != valoresIndices.size()) {
                    Excepcion error = new Excepcion("Las dimensiones del arreglo a asignar no"
                            + " coinciden con las dimensiones del arreglo ["+identifier+"]", line, column);
                    ar.excepciones.add(error);
                    ar.console.add(error.description +  " Linea: " +  line + " Columna" + column);
                    return error;
                }
                arreglo.setTamaniosDimensiones(valoresIndices);
            } else {
                Excepcion error = (Excepcion) obj;
                error.line = this.line;
                error.column = this.column;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        }
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Symbol.Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }

}
