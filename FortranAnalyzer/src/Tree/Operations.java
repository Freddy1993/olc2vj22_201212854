package Tree;

import Tree.Symbol.*;
import Utils.Utils;

import java.util.HashMap;

public class Operations implements NodeInst {
    public static enum Tipo_Operations {
        SUMA,
        RESTA,
        MULTIPLICACION,
        DIVISION,
        NEGATIVO,
        ENTERO,
        REAL,
        IDENTIFICADOR,
        CADENA,
        MAYOR_QUE,
        MENOR_QUE,
        MAYOR_IGUAL_QUE,
        MENOR_IGUAL_QUE,
        DIFERENTE_QUE,
        IGUAL_QUE,
        NOT,
        AND,
        OR,
        TRUE,
        FALSE,
        CONCATENACION,
        POTENCIA,
        COMPLEX,
        CHARACTER,
        LOGICAL
    }
    /**
     * Tipo de operación a ejecutar.
     */
    private Tipo_Operations operador;
    private Tipo tipo;

    private NodeInst operadorIzq;

    private NodeInst operadorDer;

    private Object valor;
    public int line = 0;
    public int column = 0;

    public Operations(NodeInst operadorIzq, NodeInst operadorDer, String tipo, int line, int column) {
        if(tipo.equals("+")){
            this.operador = Tipo_Operations.SUMA;
        } else if(tipo.equals("-")){
            this.operador = Tipo_Operations.RESTA;
        } else if(tipo.equals("*")){
            this.operador = Tipo_Operations.MULTIPLICACION;
        } else if(tipo.equals("/")){
            this.operador = Tipo_Operations.DIVISION;
        } else if(tipo.equals("**")){
            this.operador = Tipo_Operations.POTENCIA;
        } else if (tipo.equals(".eq.") || tipo.equals("==")) { //
            this.operador = Tipo_Operations.IGUAL_QUE;
        } else if (tipo.equals(".ne.") || tipo.equals("/=")) { //
            this.operador = Tipo_Operations.DIFERENTE_QUE;
        } else if (tipo.equals(".gt.") || tipo.equals(">")) { //
            this.operador = Tipo_Operations.MAYOR_QUE;
        } else if (tipo.equals(".lt.") || tipo.equals("<")) { //
            this.operador = Tipo_Operations.MENOR_QUE;
        } else if (tipo.equals(".ge.") || tipo.equals(">=")) { //
            this.operador = Tipo_Operations.MAYOR_IGUAL_QUE;
        } else if (tipo.equals(".le.") || tipo.equals("<=")) { //
            this.operador = Tipo_Operations.MENOR_IGUAL_QUE;
        } else if (tipo.equals(".and.")) { //
            this.operador = Tipo_Operations.AND;
        } else if (tipo.equals(".not.")) { //
            this.operador = Tipo_Operations.NOT;
        } else if (tipo.equals(".or.")) { //
            this.operador = Tipo_Operations.OR;
        }
        this.line = line;
        this.column = column;
        this.operadorIzq = operadorIzq;
        this.operadorDer = operadorDer;
    }

    public Operations(NodeInst operadorIzq, Tipo_Operations tipo, int line, int column) {
        this.operador = tipo;
        this.operadorIzq = operadorIzq;
        this.line = line;
        this.column = column;
    }

    public Operations(String a, Tipo_Operations tipo, int line, int column) {
        this.valor = a;
        this.operador = tipo;
        this.line = line;
        this.column = column;
    }

    public Operations(Double a, int line, int column) {
        this.valor = a;
        this.operador = Tipo_Operations.ENTERO;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        Object a = (operadorIzq == null) ? null : operadorIzq.execute(ts, ar);
        Object b = (operadorDer == null) ? null : operadorDer.execute(ts, ar);

        if(a instanceof Excepcion) {
            return a;
        }

        if(b instanceof Excepcion) {
            return b;
        }

        if(this.operador == Tipo_Operations.NEGATIVO) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            if(tipoNodoIzq == Tipo.ENTERO) {
                this.tipo = Tipo.REAL;
                return Integer.parseInt(a.toString()) * -1;
            } else if(tipoNodoIzq == Tipo.REAL) {
                this.tipo = Tipo.REAL;
                return Double.parseDouble(a.toString()) * -1;
            } else {
                Excepcion error = new Excepcion("Error de tipos, el operador negativo debe aplicarse a un número.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.POTENCIA) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                this.tipo = Tipo.REAL;
                return Math.pow(Double.parseDouble(a.toString()), Double.parseDouble(b.toString()));
            }  else {
                Excepcion error = new Excepcion("Error de tipos, la potencia debe hacerse entre números.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.DIVISION) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) {
                if ((Integer) b != 0) {
                    this.tipo = Tipo.ENTERO;
                    return (Integer) a / (Integer) b;
                } else {
                    System.err.println("Error division entre 0, la division debe hacerse con numero diferente de cero en el divisor.");
                    Excepcion error = new Excepcion("Error division entre 0, la division debe hacerse con numero diferente de cero en el divisor.", line, column);
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
            } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                if (Double.parseDouble(b.toString()) != 0) {
                    this.tipo = Tipo.REAL;
                    return Double.parseDouble(a.toString()) / Double.parseDouble(b.toString());
                } else {
                    Excepcion error = new Excepcion("Error division entre 0, la division debe hacerse con numero diferente de cero en el divisor.", line, column);
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
            } else {
                Excepcion error = new Excepcion("Error de tipos, la división debe hacerse entre números.", line, column);
                ar.excepciones.add(error);
                return error;
            }
        } else if (this.operador == Tipo_Operations.MULTIPLICACION) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) {
                this.tipo = Tipo.ENTERO;
                return (Integer) a * (Integer) b;
            } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                this.tipo = Tipo.REAL;
                return Double.parseDouble(a.toString()) * Double.parseDouble(b.toString());
            } else {
                System.err.println("Error de tipos, la multiplicación debe hacerse entre números.");
                Excepcion error = new Excepcion("Error de tipos, la multiplicación debe hacerse entre números.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.RESTA) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) {
                this.tipo = Tipo.ENTERO;
                return (Integer) a - (Integer) b;
            } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                this.tipo = Tipo.REAL;
                return Double.parseDouble(a.toString()) - Double.parseDouble(b.toString());
            } else {
                System.err.println("Error de tipos, la resta debe hacerse entre números.");
                Excepcion error = new Excepcion("Error de tipos, la resta debe hacerse entre números.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.SUMA) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) {
                this.tipo = Tipo.ENTERO;
                return (Integer) a + (Integer) b;
            } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)|| (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                this.tipo = Tipo.REAL;
                return Double.parseDouble(a.toString()) + Double.parseDouble(b.toString());
            }

            else {
                System.err.println("Error de tipos, la suma debe hacerse entre números.");
                Excepcion error = new Excepcion("Error de tipos, la suma debe hacerse entre números.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.IGUAL_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if ((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                    (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                    (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL)||
                    (tipoNodoIzq == Tipo.CHARACTER && tipoNodoDer == Tipo.CHARACTER)
            ) {
                this.tipo = Tipo.LOGICAL;
                return a.toString().equals(b.toString());
            } else {
                Excepcion error = new Excepcion("Error de tipos en la expresion logica ==.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.DIFERENTE_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if (
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL)||
                            (tipoNodoIzq == Tipo.CHARACTER && tipoNodoDer == Tipo.CHARACTER)
            ) {
                this.tipo = Tipo.LOGICAL;
                return !(a.toString().equals(b.toString()));
            } else {
                Excepcion error = new Excepcion("Error de tipos en la expresion logica /=.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.MENOR_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if (
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
            ) {
                this.tipo = Tipo.LOGICAL;
                return Double.parseDouble(a.toString()) < Double.parseDouble(b.toString());
            } else {
                Excepcion error = new Excepcion("Error de tipos en la expresion logica <.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.MENOR_IGUAL_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if (
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
            ) {
                this.tipo = Tipo.LOGICAL;
                return Double.parseDouble(a.toString()) <= Double.parseDouble(b.toString());
            } else {
                Excepcion error = new Excepcion("Error de tipos en la expresion logica <.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;

            }
        } else if (this.operador == Tipo_Operations.MAYOR_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if (
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
            ) {
                this.tipo = Tipo.LOGICAL;
                return Double.parseDouble(a.toString()) > Double.parseDouble(b.toString());
            } else {
                Excepcion error = new Excepcion("Error de tipos en la expresion logica >.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if (this.operador == Tipo_Operations.MAYOR_IGUAL_QUE) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if (
                    (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                            (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
            ) {
                this.tipo = Tipo.LOGICAL;
                return Double.parseDouble(a.toString()) >= Double.parseDouble(b.toString());
            } else {
                System.err.println("Error de tipos en la expresion logica >=.");
                Excepcion error = new Excepcion("Error de tipos en la expresion logica >=.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if(this.operador == Tipo_Operations.AND) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if ( (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL) ) {
                this.tipo = Tipo.LOGICAL;
                return ((Boolean) a) && ((Boolean) b);
            } else {
                System.err.println("Error de tipos, la operación and debe hacerse entre expresiones booleanas.");
                Excepcion error = new Excepcion("Error de tipos, la operación and debe hacerse entre expresiones booleanas.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if(this.operador == Tipo_Operations.NOT) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            if (tipoNodoIzq == Tipo.LOGICAL) {
                this.tipo = Tipo.LOGICAL;
                return !((Boolean) a);
            } else {
                System.err.println("Error de tipos, la negación debe hacerse a una expresión booleana.");
                Excepcion error = new Excepcion("Error de tipos, la negación debe hacerse a una expresión booleana.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else if(this.operador == Tipo_Operations.OR) {
            Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
            Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
            if ( (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL) ) {
                this.tipo = Tipo.LOGICAL;
                return ((Boolean) a) || ((Boolean) b);
            } else {
                System.err.println("Error de tipos, la operación or debe hacerse entre expresiones booleanas.");
                Excepcion error = new Excepcion("Error de tipos, la operación or debe hacerse entre expresiones booleanas.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        }

        return null;
    }

    @Override
    public Tipo getTipoInstrution() {
        return this.tipo;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        try {
            if(this.operador == Tipo_Operations.OR ||
                    this.operador ==  Tipo_Operations.AND || this.operador == Tipo_Operations.NOT)
            {
                if(this.operador == Tipo_Operations.AND)
                {

                    Object valorI = (operadorIzq == null) ? null : operadorIzq.code3D(ts, ar);
                    HashMap<String, String> etiquetas = new HashMap<>();
                    if(valorI instanceof HashMap<?,?>)
                    {
                        Utils.printConsole(((HashMap<?, ?>) valorI).get("LV")+":\n");
                        Object valorD = (operadorDer == null) ? null : operadorDer.code3D(ts, ar);
                        if(valorD instanceof HashMap<?,?>)
                        {
                            etiquetas.put("LV", ((HashMap<?, ?>) valorD).get("LV").toString());
                            etiquetas.put("LF", ((HashMap<?, ?>) valorD).get("LF") + ":\n"+ ((HashMap<?, ?>) valorI).get("LF")) ;
                        }
                        else
                        {
                            String LV = Utils.generarEtiqueta();
                            etiquetas.put("LV", LV);
                            String LF = Utils.generarEtiqueta();
                            Utils.printConsole("if("+valorD+"==1) goto "+LV+";//Verdadero\n");
                            Utils.printConsole("goto "+LF+";//Falso\n");
                            //this.LF = valorI.LF + ":\n"+ LF;
                            etiquetas.put("LF", ((HashMap<?, ?>) valorI).get("LF") + ":\n"+ LF);
                        }
                    }
                    else
                    {

                        String LV = Utils.generarEtiqueta();
                        String LF = Utils.generarEtiqueta();
                        Utils.printConsole("if("+valorI+"==1) goto "+LV+";//Verdadero\n");
                        Utils.printConsole("goto "+LF+";//Falso\n");
                        Utils.printConsole(LV+":\n");
                        Object valorD = (operadorDer == null) ? null : operadorDer.code3D(ts, ar);

                        if(valorD instanceof HashMap<?,?>)
                        {
                            etiquetas.put("LV", ((HashMap<?, ?>) valorD).get("LV").toString());
                            etiquetas.put("LF", LF + ":\n"+ ((HashMap<?, ?>) valorD).get("LF"));
                        }
                        else
                        {
                            LV = Utils.generarEtiqueta();
                            etiquetas.put("LV", LV);
                            Utils.printConsole("if("+valorD+"==1) goto "+LV+";//Verdadero\n");
                            Utils.printConsole("goto "+LF+";//Falso\n");
                            etiquetas.put("LF", LF);
                        }

                    }
                    return etiquetas;
                } else if (this.operador == Tipo_Operations.OR){
                    Object valorI = (operadorIzq == null) ? null : operadorIzq.code3D(ts, ar);
                    HashMap<String, String> etiquetas = new HashMap<>();
                    if(valorI instanceof HashMap<?,?>)
                    {
                        Utils.printConsole(((HashMap<?, ?>) valorI).get("LF")+":\n");
                        Object valorD = (operadorDer == null) ? null : operadorDer.code3D(ts, ar);
                        if(valorD instanceof HashMap<?,?>)
                        {
                            etiquetas.put("LV", ((HashMap<?, ?>) valorD).get("LV").toString() + ":\n"+ ((HashMap<?, ?>) valorI).get("LV"));
                            etiquetas.put("LF", ((HashMap<?, ?>) valorD).get("LF").toString()) ;
                        }
                        else
                        {
                            String LV = Utils.generarEtiqueta();
                            String LF = Utils.generarEtiqueta();
                            etiquetas.put("LF", LF);
                            Utils.printConsole("if("+valorD+"==1) goto "+LV+";//Verdadero\n");
                            Utils.printConsole("goto "+LF+";//Falso\n");
                            //this.LF = valorI.LF + ":\n"+ LF;
                            etiquetas.put("LV", ((HashMap<?, ?>) valorI).get("LV") + ":\n"+ LV);
                        }
                    }
                    else
                    {

                        String LV = Utils.generarEtiqueta();
                        String LF = Utils.generarEtiqueta();
                        Utils.printConsole("if("+valorI+"==1) goto "+LV+";//Verdadero\n");
                        Utils.printConsole("goto "+LF+";//Falso\n");
                        Utils.printConsole(LV+":\n");
                        Object valorD = (operadorDer == null) ? null : operadorDer.code3D(ts, ar);

                        if(valorD instanceof HashMap<?,?>)
                        {
                            etiquetas.put("LF", ((HashMap<?, ?>) valorD).get("LF").toString());
                            etiquetas.put("LV", LV + ":\n"+ ((HashMap<?, ?>) valorD).get("LV"));
                        }
                        else
                        {
                            String LV2 = Utils.generarEtiqueta();

                            etiquetas.put("LV", LV + ":\n"+LV2);
                            Utils.printConsole("if("+valorD+"==1) goto "+LV2+";//Verdadero\n");
                            Utils.printConsole("goto "+LF+";//Falso\n");
                            etiquetas.put("LF", LF);
                        }

                    }
                    return etiquetas;
                } else {
                    Object valorI = (operadorIzq == null) ? null : operadorIzq.code3D(ts, ar);
                    HashMap<String, String> etiquetas = new HashMap<>();
                    if(valorI instanceof HashMap<?,?>)
                    {
                        etiquetas.put("LF", ((HashMap<?, ?>) valorI).get("LV").toString());
                        etiquetas.put("LV", ((HashMap<?, ?>) valorI).get("LF").toString());

                        return etiquetas;
                    } else {
                        String LF = Utils.generarEtiqueta();
                        String LV = Utils.generarEtiqueta();
                        Utils.printConsole("if("+valorI+"==1) goto "+LF+";//Falso\n");
                        Utils.printConsole("goto "+LV+";//Verdadero\n");
                        etiquetas.put("LV", LV);
                        etiquetas.put("LF", LF);
                        return etiquetas;
                    }
                }
            }


            Object a = (operadorIzq == null) ? null : operadorIzq.code3D(ts, ar);
            Object b = (operadorDer == null) ? null : operadorDer.code3D(ts, ar);
            if(this.operador == Tipo_Operations.NEGATIVO)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                if(tipoNodoIzq == Tipo.ENTERO)
                {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"*-1;  // Calculo negativo\n");
                    return t0;
                }
                else if(tipoNodoIzq == Tipo.REAL)
                {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"*-1;  // Calculo negativo\n");
                    return t0;
                } else
                {
                    return null;
                }
            }
            else if (this.operador == Tipo_Operations.SUMA)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO)
                {
                    this.tipo = Tipo.ENTERO;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"+" + b +";//\n");
                    return t0;
                } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)|| (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"+" + b +";  // Calculo negativo\n");
                    return t0;
                } else
                {
                    return null;
                }
            }
            else if (this.operador == Tipo_Operations.RESTA)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO)
                {
                    this.tipo = Tipo.ENTERO;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"-" + b +";//\n");
                    return t0;
                } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"-" + b +";//\n");
                    return t0;
                } else
                {
                    return null;
                }
            }
            else if (this.operador == Tipo_Operations.MULTIPLICACION)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO)
                {
                    this.tipo = Tipo.ENTERO;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"*" + b +";//\n");
                    return t0;
                } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL)) {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=" + a +"*" + b +";//\n");
                    return t0;
                } else
                {
                    return null;
                }
            }
            else if (this.operador == Tipo_Operations.DIVISION)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                String t0 = Utils.generateTemp();
                if(tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO)
                {
                    String L1 = Utils.generarEtiqueta();
                    String L2 = Utils.generarEtiqueta();
                    String L3 = Utils.generarEtiqueta();
                    Utils.printConsole("if("+b+"==0) goto "+L1+"; // Error dividendo\n");
                    Utils.printConsole("goto "+L2+";//correcto\n");
                    Utils.printConsole(L1+":\n");
                    Utils.printConsole("//Aqui va el codigo para manejo de error de division sobre 0.\n");
                    String inicioCadena = Utils.generarCadena3D("Error. El dividendo no puede ser igual a 0.");
                    //String nodoString = new ExpString(this.linea, this.columna, "Error. El dividendo no puede ser igual a 0.");
                    //String inicioCadena = nodoString.generar3D(entorno);
                    Utils.generarCodigoImprimirCadena(inicioCadena, ts.size());
                    Utils.printConsole(t0+"=0;//correcto\n");
                    Utils.printConsole("goto "+L3+";//Salida\n");
                    Utils.printConsole(L2+":\n");
                    Utils.printConsole(t0+"="+a+"/"+b+";\n");
                    Utils.printConsole(L3+":\n");
                    this.tipo = Tipo.ENTERO;
                    return t0;

                } else if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                        || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL))
                {
                    String L1 = Utils.generarEtiqueta();
                    String L2 = Utils.generarEtiqueta();
                    String L3 = Utils.generarEtiqueta();
                    Utils.printConsole("if("+b+"==0) goto "+L1+"; // Error dividendo\n");
                    Utils.printConsole("goto "+L2+";//correcto\n");
                    Utils.printConsole(L1+":\n");
                    Utils.printConsole("//Aqui va el codigo para manejo de error de division sobre 0.\n");
                    Utils.printConsole(t0+"=0;//correcto\n");
                    Utils.printConsole("goto "+L3+";//Salida\n");
                    Utils.printConsole(L2+":\n");
                    Utils.printConsole(t0+"="+a+"/"+b+";\n");
                    Utils.printConsole(L3+":\n");
                    this.tipo = Tipo.REAL;
                    return t0;
                }
                else
                {
                    return null;
                }
            }
            else if (this.operador == Tipo_Operations.POTENCIA)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                if((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO)
                        || (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL)
                        || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                        || (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL))
                {
                    this.tipo = Tipo.REAL;
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=pow(" + a +"," + b +");//\n");
                    return t0;
                }  else
                {
                    return null;
                }
            }

            else if (this.operador == Tipo_Operations.IGUAL_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if ((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                        (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                        (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL) ||
                        (tipoNodoIzq == Tipo.CHARACTER && tipoNodoDer == Tipo.CHARACTER)
                )
                {
                    String EtiquetaVerdadera = Utils.generarEtiqueta();
                    String EtiquetaFalsa = Utils.generarEtiqueta();
                    if(a instanceof HashMap<?,?>) // Verificamos si tenemos etiquetas
                    {
                        String t100 = Utils.generateTemp();
                        String LS = Utils.generarEtiqueta();
                        HashMap<String, String> etiquetasA = (HashMap<String, String>)a;
                        Utils.printConsole(etiquetasA.get("LV")+":\n");
                        Utils.printConsole(t100+"=1;\n");
                        Utils.printConsole("goto "+LS+";\n");
                        Utils.printConsole(etiquetasA.get("LF")+":\n");
                        Utils.printConsole(t100+"=0;\n");
                        Utils.printConsole(LS+":\n");
                        a = t100;
                    }
                    if(b instanceof HashMap<?,?>)
                    {
                        String t100 = Utils.generateTemp();
                        String LS = Utils.generarEtiqueta();
                        HashMap<String, String> etiquetasB = (HashMap<String, String>)a;
                        Utils.printConsole(etiquetasB.get("LV")+":\n");
                        Utils.printConsole(t100+"=1;\n");
                        Utils.printConsole("goto "+LS+";\n");
                        Utils.printConsole(etiquetasB.get("LF")+":\n");
                        Utils.printConsole(t100+"=0;\n");
                        Utils.printConsole(LS+":\n");
                        b = t100;
                    }
                    Utils.printConsole("if("+a+"=="+b+") goto "+EtiquetaVerdadera+"; \n");
                    Utils.printConsole("goto "+EtiquetaFalsa+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV",EtiquetaVerdadera);
                    etiquetas.put("LF",EtiquetaFalsa);
                    return etiquetas;
                } else if (tipoNodoIzq == Tipo.VOID && tipoNodoDer == Tipo.VOID) // Comparacion de cadenas
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();

                    String t0 = Utils.generateTemp();
                    String t1 = Utils.generateTemp();
                    String t2 = Utils.generateTemp();
                    String t3 = Utils.generateTemp();
                    String t4 = Utils.generateTemp();

                    Utils.printConsole(t0+"=P+"+ts.size()+"; // Simulacion de cambio de entorno\n");
                    Utils.printConsole(t1+"="+t0+"+1; //Direccion Cadena 1\n");
                    Utils.printConsole("stack[(int)"+t1+"] = "+a+"; // Pasando primer cadena\n");
                    Utils.printConsole(t2+"="+t0+"+2; //Direccion Cadena 2\n");
                    Utils.printConsole("stack[(int)"+t2+"] = "+b+"; // Pasando segunda cadena\n");
                    Utils.printConsole("P=P+"+ts.size()+"; // Cambio de entorno\n");
                    Utils.printConsole("_compararCadenas_();\n");
                    Utils.printConsole("P=P-"+ts.size()+"; // Retomar de entorno\n");
                    Utils.printConsole(t3+"="+t0+"+0; //Direccion retorno\n");
                    Utils.printConsole(t4+"=stack[(int)"+t3+"]; //Valor de retorno\n");
                    Utils.printConsole("if("+t4+"==1) goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<String, String>();
                    etiquetas.put("LV",LV);
                    etiquetas.put("LF",LF);
                    this.valor = etiquetas;
                    return this.valor;
                }

            }
            else if (this.operador == Tipo_Operations.DIFERENTE_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if ((tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                        (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                        (tipoNodoIzq == Tipo.LOGICAL && tipoNodoDer == Tipo.LOGICAL) ||
                        (tipoNodoIzq == Tipo.CHARACTER && tipoNodoDer == Tipo.CHARACTER)
                )
                {
                    String EtiquetaVerdadera = Utils.generarEtiqueta();
                    String EtiquetaFalsa = Utils.generarEtiqueta();
                    if(a instanceof HashMap<?,?>) // Verificamos si tenemos etiquetas
                    {
                        String t100 = Utils.generateTemp();
                        String LS = Utils.generarEtiqueta();
                        HashMap<String, String> etiquetasA = (HashMap<String, String>)a;
                        Utils.printConsole(etiquetasA.get("LV")+":\n");
                        Utils.printConsole(t100+"=1;\n");
                        Utils.printConsole("goto "+LS+";\n");
                        Utils.printConsole(etiquetasA.get("LF")+":\n");
                        Utils.printConsole(t100+"=0;\n");
                        Utils.printConsole(LS+":\n");
                        a = t100;
                    }
                    if(b instanceof HashMap<?,?>)
                    {
                        String t100 = Utils.generateTemp();
                        String LS = Utils.generarEtiqueta();
                        HashMap<String, String> etiquetasB = (HashMap<String, String>)a;
                        Utils.printConsole(etiquetasB.get("LV")+":\n");
                        Utils.printConsole(t100+"=1;\n");
                        Utils.printConsole("goto "+LS+";\n");
                        Utils.printConsole(etiquetasB.get("LF")+":\n");
                        Utils.printConsole(t100+"=0;\n");
                        Utils.printConsole(LS+":\n");
                        b = t100;
                    }
                    Utils.printConsole("if("+a+"!="+b+") goto "+EtiquetaVerdadera+"; \n");
                    Utils.printConsole("goto "+EtiquetaFalsa+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV",EtiquetaVerdadera);
                    etiquetas.put("LF",EtiquetaFalsa);
                    return etiquetas;
                } else if (tipoNodoIzq == Tipo.VOID && tipoNodoDer == Tipo.VOID) // Comparacion de cadenas
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();

                    String t0 = Utils.generateTemp();
                    String t1 = Utils.generateTemp();
                    String t2 = Utils.generateTemp();
                    String t3 = Utils.generateTemp();
                    String t4 = Utils.generateTemp();

                    Utils.printConsole(t0+"=P+"+ts.size()+"; // Simulacion de cambio de entorno\n");
                    Utils.printConsole(t1+"="+t0+"+1; //Direccion Cadena 1\n");
                    Utils.printConsole("stack[(int)"+t1+"] = "+a+"; // Pasando primer cadena\n");
                    Utils.printConsole(t2+"="+t0+"+2; //Direccion Cadena 2\n");
                    Utils.printConsole("stack[(int)"+t2+"] = "+b+"; // Pasando segunda cadena\n");
                    Utils.printConsole("P=P+"+ts.size()+"; // Cambio de entorno\n");
                    Utils.printConsole("_compararCadenas_();\n");
                    Utils.printConsole("P=P-"+ts.size()+"; // Retomar de entorno\n");
                    Utils.printConsole(t3+"="+t0+"+0; //Direccion retorno\n");
                    Utils.printConsole(t4+"=stack[(int)"+t3+"]; //Valor de retorno\n");
                    Utils.printConsole("if("+t4+"==1) goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<String, String>();
                    etiquetas.put("LV",LF);
                    etiquetas.put("LF",LV);
                    this.valor = etiquetas;
                    return this.valor;
                }
            }
            else if (this.operador == Tipo_Operations.MENOR_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if (
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                )
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();
                    String valorI = a.toString();
                    String valorD = b.toString();

                    Utils.printConsole("if("+valorI+"<"+valorD+") goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV", LV);
                    etiquetas.put("LF", LF);
                    return etiquetas;
                }
            }
            else if (this.operador == Tipo_Operations.MENOR_IGUAL_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if (
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                )
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();
                    String valorI = a.toString();
                    String valorD = b.toString();

                    Utils.printConsole("if("+valorI+"<="+valorD+") goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV", LV);
                    etiquetas.put("LF", LF);
                    return etiquetas;
                }
            }
            else if (this.operador == Tipo_Operations.MAYOR_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if (
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                )
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();
                    String valorI = a.toString();
                    String valorD = b.toString();

                    Utils.printConsole("if("+valorI+">"+valorD+") goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV", LV);
                    etiquetas.put("LF", LF);
                    return etiquetas;
                }
            }
            else if (this.operador == Tipo_Operations.MAYOR_IGUAL_QUE)
            {
                Tipo tipoNodoIzq = this.operadorIzq.getTipoInstrution();
                Tipo tipoNodoDer = this.operadorDer.getTipoInstrution();
                this.tipo = Tipo.LOGICAL;
                if (
                        (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.ENTERO) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.ENTERO && tipoNodoDer == Tipo.REAL) ||
                                (tipoNodoIzq == Tipo.REAL && tipoNodoDer == Tipo.ENTERO)
                )
                {
                    String LV = Utils.generarEtiqueta();
                    String LF = Utils.generarEtiqueta();
                    String valorI = a.toString();
                    String valorD = b.toString();

                    Utils.printConsole("if("+valorI+">="+valorD+") goto "+LV+"; \n");
                    Utils.printConsole("goto "+LF+"; \n");
                    HashMap<String, String> etiquetas = new HashMap<>();
                    etiquetas.put("LV", LV);
                    etiquetas.put("LF", LF);
                    return etiquetas;
                }
            }
        }catch (Exception e) {
            System.out.println(e);
        }


        return null;
    }
}
