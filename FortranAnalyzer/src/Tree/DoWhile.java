
package Tree;

import java.util.HashMap;
import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class DoWhile implements NodeInst {

    private final NodeInst condicion;
    private final LinkedList<NodeInst> listaInstrucciones;
    public int line = 0;
    public int column = 0;
    public DoWhile(NodeInst a, LinkedList<NodeInst> b, int line, int column) {
        this.condicion=a;
        this.listaInstrucciones=b;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        Object cond;
        do {
            TableSymbol tablaLocal = new TableSymbol();
            tablaLocal.addAll(ts);
            for (NodeInst ins : listaInstrucciones) {
                Object r;
                r = ins.execute(tablaLocal, ar);
                if (r instanceof Excepcion) {
                    return r;
                }
                if (r != null) {
                    if (r instanceof Break) {
                        return null;
                    } else {
                        return r;
                    }
                }
            }
            cond = condicion.execute(ts, ar);
            if (cond instanceof Excepcion) {
                return cond;
            }
        } while ((Boolean) cond);
        return null;
    }


    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }
    @Override
    public Object code3D(TableSymbol ts, Arbol ar)
    {
        Tipo tipo = condicion.getTipoInstrution();
        if(Tipo.LOGICAL == Tipo.LOGICAL)
        {
            String LWhile = Utils.generarEtiqueta();
            Utils.printConsole(LWhile+":// Inicio ciclo While\n");
            Object valorCondicion = this.condicion.code3D(ts,ar);
            String t100 = Utils.generateTemp();
            if(valorCondicion instanceof HashMap<?,?>)
            {
                String LS = Utils.generarEtiqueta();
                Utils.printConsole(((HashMap<?, ?>) valorCondicion).get("LV")+":\n");
                Utils.printConsole(t100+"=1;\n");
                Utils.printConsole("goto "+LS+";\n");
                Utils.printConsole(((HashMap<?, ?>) valorCondicion).get("LF")+":\n");
                Utils.printConsole(t100+"=0;\n");
                Utils.printConsole(LS+":\n");
                valorCondicion = t100;
            }
            String LVWhile = Utils.generarEtiqueta();
            String LFWhile = Utils.generarEtiqueta();
            Utils.printConsole("if("+valorCondicion+"==1) goto "+LVWhile+";\n");
            Utils.printConsole("goto "+LFWhile+";\n");
            Utils.printConsole(LVWhile+": // Bloque de instrucciones\n");
            //Generamos codigo de las instrucciones
            for(NodeInst ins:listaInstrucciones){
                Object r;
                r=ins.code3D(ts,ar);
            }
            Utils.printConsole("goto "+LWhile+";\n");
            Utils.printConsole(LFWhile+":\n");
        }
        return null;
    }
}