package Tree;
import Tree.Symbol.*;
import java.util.LinkedList;
public class AccessArray implements NodeInst {
   
    private final LinkedList<NodeInst> indices;

    public Tipo tipo;

    protected final String id;

    public int line = 0;
    public int column = 0;

    public AccessArray(String id, LinkedList<NodeInst> indices, int line, int column) {
        this.indices = indices;
        this.id = id;
        this.line = line;
        this.column = column;
    }
   
    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        LinkedList<Integer> valoresIndices=new LinkedList<>();
        for (NodeInst dim : indices) {
            Object er=dim.execute(ts, ar);
            if(er instanceof Excepcion) {
                return er;
            }
            if(!((er instanceof Double) || (er instanceof Integer))){
                Excepcion error = new Excepcion("Los indices deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description + " Linea: " +  line + " Columna" + column);
                return error;
            }
            if (er instanceof Integer) {
                valoresIndices.add(((Integer)er).intValue());
            } else if (er instanceof Double) {
                valoresIndices.add(((Double)er).intValue());
            }

        }
        Object val = ts.getValor(id, valoresIndices);
        if(val instanceof Excepcion) {
            Excepcion error = (Excepcion) val;
            error.line = this.line;
            error.column = this.column;
            ar.excepciones.add(error);
            ar.console.add(error.description);
            return error;
        } else {
            if(val instanceof Double) {
                tipo = Tipo.REAL;
            } else if(val instanceof Integer) {
                tipo = Tipo.ENTERO;
            } else if(val instanceof String) {
                tipo = Tipo.CHARACTER;
            }
            return ts.getValor(id, valoresIndices);
        }
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Tipo getTipoInstrution() {
        return tipo;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }
}
