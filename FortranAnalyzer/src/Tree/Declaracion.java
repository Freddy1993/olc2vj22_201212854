package Tree;
import java.util.HashMap;
import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class Declaracion implements NodeInst {
    protected boolean parametro;

    public int line = 0;
    public int column = 0;

    protected String id;

    protected Tipo tipo;

    protected NodeInst valor;

    public LinkedList<NodeInst> variables;

    public Object valorDefault = 0;

    public Declaracion(String a, String b, NodeInst c, int line, int column) {
        id=a;
        String reservadaTipo=b.toLowerCase();
        switch (reservadaTipo) {
            case "number": tipo=Symbol.Tipo.ENTERO;
                break;
            case "string":  tipo=Symbol.Tipo.CHARACTER;
                break;
            case "boolean": tipo=Symbol.Tipo.LOGICAL;
                break;
            default:
                tipo=null;
        }
        parametro=false;
        this.valor = c;
        this.line = line;
        this.column = column;
    }

    public Declaracion(String a, Tipo b, NodeInst c, int line, int column) {
        id=a;
        tipo = b;
        parametro=false;
        this.valor = c;
        this.line = line;
        this.column = column;
    }

    public Declaracion(LinkedList<NodeInst> variables, Tipo tipo, int line, int column) {
        this.variables = variables;
        this.tipo = tipo;
        parametro=false;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        for (int i = 0; i < this.variables.size(); i++) {
            NodeInst instruccion = this.variables.get(i);
            if(instruccion instanceof Identifier){
                Object instr = instruccion.execute(ts,ar);
                if(instr instanceof Excepcion) {
                    return instr;
                }
                Symbol aux=new Symbol(instr.toString(),tipo);
                aux.setParametro(this.parametro);
                Object obj = aux.setValor(getValorDefault());
                if(obj instanceof Boolean) {
                    boolean asignacionCorrecta = false;
                    asignacionCorrecta = (Boolean) obj;
                    if(asignacionCorrecta)
                        ts.add(aux);
                } else if(obj instanceof Excepcion) {
                    Excepcion error = (Excepcion) obj;
                    error.line = this.line;
                    error.column = this.column;
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
            } else if(instruccion instanceof Assignment) {
                Assignment instrAsignacion = (Assignment) instruccion;
                Symbol aux = new Symbol(instrAsignacion.id,tipo);
                aux.setParametro(this.parametro);
                Object val = instrAsignacion.valor.execute(ts, ar);
                if(val instanceof Excepcion) {
                    return val;
                }
                Object obj = aux.setValor(val);
                if(obj instanceof Boolean) {
                    boolean asignacionCorrecta = false;
                    asignacionCorrecta = (Boolean) obj;
                    if(asignacionCorrecta){
                        ts.add(aux);
                    }
                } else {
                    Excepcion error = (Excepcion) obj;
                    error.line = this.line;
                    error.column = this.column;
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                }
            }
        }

        return null;
    }

    public String getIdentificador() {
        return id;
    }

    public boolean isParametro() {
        return parametro;
    }

    public void setParametro(boolean parametro) {
        this.parametro = parametro;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    public Object getValorDefault() {
        if (this.tipo == Tipo.ENTERO) {
            return 0;
        } else if (this.tipo == Tipo.REAL) {
            return 0.00000000;
        } else if (this.tipo == Tipo.COMPLEX) {
            return 0.00000000;
        } else if (this.tipo == Tipo.CHARACTER) {
            return "";
        } else if (this.tipo == Tipo.LOGICAL) {
            return false;
        }

        return "";
    }

    public String getValorDefault3D() {
        if (this.tipo == Tipo.ENTERO) {
            return "0";
        } else if (this.tipo == Tipo.REAL) {
            return "0.00000000";
        } else if (this.tipo == Tipo.COMPLEX) {
            return "0.00000000";
        } else if (this.tipo == Tipo.CHARACTER)
        {
            return Utils.generarCadena3D("");
        } else if (this.tipo == Tipo.LOGICAL) {
            return "0";
        }

        return "";
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        try {
            for (int i = 0; i < this.variables.size(); i++) {
                NodeInst instruccion = this.variables.get(i);
                if (instruccion instanceof Identifier) {
                    String id = ((Identifier) instruccion).identifier;
                    Tipo tipo = this.tipo;//((Identificador) instruccion).tipo;
                    //Tipo tipo = Tipo.IDENTIFICADOR;
                    Symbol aux = new Symbol(id, tipo);
                    aux.setIndice(ts.size() + 1);
                    aux.setParametro(this.parametro); // Vemos si es parametro >:v
                    boolean asignacionCorrecta = false;
                    //asignacionCorrecta = aux.setValor(getValorDefault());
                    String valorInicial = getValorDefault3D();
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=P+" + aux.getIndice() + "; // Declaracion variable " + id + "\n");
                    Utils.printConsole("stack[(int)" + t0 + "]=" + valorInicial + "; // Valor por defecto \n");
                    ts.add(aux);
                } else if (instruccion instanceof Assignment) {
                    Assignment instrAsignacion = (Assignment) instruccion;
                    String id = ((Assignment) instruccion).id;
                    Tipo tipo = this.tipo;//((Identificador) instruccion).tipo;
                    //Tipo tipo = Tipo.IDENTIFICADOR;
                    Symbol aux = new Symbol(id, tipo);
                    aux.setIndice(ts.size() + 1);
                    aux.setParametro(this.parametro); // Vemos si es parametro >:v
                    boolean asignacionCorrecta = false;
                    //asignacionCorrecta = aux.setValor(getValorDefault());
                    String t0 = Utils.generateTemp();
                    Utils.printConsole(t0 + "=P+" + aux.getIndice() + "; //Direccion local variable " + id + '\n');
                    Object valorExpresion = instrAsignacion.valor.code3D(ts, ar);
                    Tipo tipoExpresion = instrAsignacion.valor.getTipoInstrution();
                    /*if (tipoExpresion != tipo) {
                        Utils.registrarErrorSemantico(0, 0, id, "No coindice el tipo declarado al valor asignado.");
                        return null;
                    }*/

                    if (tipoExpresion == Tipo.LOGICAL) {
                        if (valorExpresion instanceof HashMap<?, ?>)//Vemos si tenemos etiquetas
                        {
                            String t100 = Utils.generateTemp();
                            String LS = Utils.generarEtiqueta();
                            Utils.printConsole(((HashMap<?, ?>) valorExpresion).get("LV") + ":\n");
                            Utils.printConsole(t100 + "=1;\n");
                            Utils.printConsole("goto " + LS + ";\n");
                            Utils.printConsole(((HashMap<?, ?>) valorExpresion).get("LF") + ":\n");
                            Utils.printConsole(t100 + "=0;\n");
                            Utils.printConsole(LS + ":\n");
                            valorExpresion = t100;
                        }
                    }
                    Utils.printConsole("stack[(int)" + t0 + "]=" + valorExpresion + ";\n");
                    ts.add(aux);
                }
            }
        } catch(Exception e) {

        }
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }
}
