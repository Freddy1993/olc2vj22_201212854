
package Tree;

import Tree.Symbol.*;
import Utils.Utils;

import java.util.HashMap;
import java.util.LinkedList;

public class PrintInstruction implements NodeInst {

    private NodeInst contenido;

    private LinkedList<NodeInst> contenidos;

    public int line = 0;
    public int column = 0;

    public PrintInstruction(NodeInst contenido, int line, int column) {
        this.contenido = contenido;
        this.line = line;
        this.column = column;
    }

    public PrintInstruction(LinkedList<NodeInst> contenido, int line, int column) {
        this.contenidos = contenido;
        this.line = line;
        this.column = column;
    }

    public Object execute(TableSymbol ts,Arbol ar) {
        for(NodeInst instruccion: this.contenidos) {
            Object rImprimir = instruccion.execute(ts,ar);//Variable que almacena el resultado de la expresión que se desea imprimir
            if(rImprimir instanceof Excepcion) {
                return rImprimir;
            }
            if(rImprimir != null){
                if (!(rImprimir instanceof ArrayNode)) {
                    if (rImprimir instanceof Boolean) {
                        if ((Boolean) rImprimir) {
                            rImprimir = "V";
                        } else {
                            rImprimir = "F";
                        }
                    }
                    ar.console.add(rImprimir instanceof Nulo ? "nulo" : String.valueOf(rImprimir));
                } else {
                    LinkedList<String>  valores = new LinkedList<>();
                    ArrayNode arreglo = (ArrayNode) rImprimir;
                    mostrarArregloDinamico(arreglo.getCeldasVecinas(), valores);
                    String itemArreglo = valores.toString();
                    if(valores.size() == 1) {
                        itemArreglo = itemArreglo.substring(1, itemArreglo.length() - 1);
                    }
                    ar.console.add(rImprimir instanceof Nulo ? "nulo" : itemArreglo);
                }
            }
        }
        return null;
    }

    public void mostrarArregloDinamico(LinkedList<ArrayNode> obj, LinkedList<String> valores) {
        String aux = "";
        for (int j = 0; j < obj.size(); j++) {
            if(obj.get(j).getCeldasVecinas().size() > 0) {
                mostrarArregloDinamico(obj.get(j).getCeldasVecinas(), valores);
            } else {
                Object valor = obj.get(j).getValorCeldas();
                if(!(valor instanceof Nulo)){
                    aux = aux + " " + obj.get(j).getValorCeldas().toString();
                }
            }
        }
        if(!aux.isEmpty()) {
            valores.add("[" + aux + "]");
        }
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {

        Object rImprimir = null;
        for(NodeInst Instructions: this.contenidos) {

            Object valorExpresion = Instructions.code3D(ts,ar);
            Tipo tipoExpresion = Instructions.getTipoInstrution();
            if (tipoExpresion == Tipo.ENTERO)
            {
                Utils.printConsole("printf(\"%d\", (int)" + valorExpresion + "); // Imprimir entero\n");
                Utils.printConsole("printf(\"%c\", 10); // Imprimir salto\n");
            }
            else if (tipoExpresion  == Tipo.REAL)
            {
                Utils.printConsole("printf(\"%f\"," + valorExpresion + "); // Imprimir double\n");
                Utils.printConsole("printf(\"%c\", 10); // Imprimir salto\n");
            }
            else if (tipoExpresion  == Tipo.LOGICAL)
            {
                if(valorExpresion instanceof HashMap<?,?>)
                {
                    HashMap<String, String> etiquetas= (HashMap<String, String>) valorExpresion;
                    String LSalida = Utils.generarEtiqueta();
                    Utils.printConsole(etiquetas.get("LV")+":\n");
                    String inicioCadena = Utils.generarCadena3D("1\n");
                    Utils.generarCodigoImprimirCadena(inicioCadena, ts.size());
                    Utils.printConsole("goto "+LSalida+"; // Salida\n ");
                    Utils.printConsole(etiquetas.get("LF")+":\n");
                    inicioCadena = Utils.generarCadena3D("0\n");
                    Utils.generarCodigoImprimirCadena(inicioCadena, ts.size());
                    Utils.printConsole(LSalida+":\n");
                }
                else
                {
                    String t3 = Utils.generateTemp();
                    String L1 = Utils.generarEtiqueta();
                    String L2 = Utils.generarEtiqueta();
                    String L3 = Utils.generarEtiqueta();
                    String t0 = Utils.generateTemp();
                    Utils.printConsole("if ("+valorExpresion+"==1) goto "+L1+"; // Valor Verdadero\n");
                    Utils.printConsole("goto "+L2+"; // Valor falso\n");
                    Utils.printConsole(L1+":\n");
                    String cadenaStringVerdadero = Utils.generarCadena3D("1\n");
                    Utils.printConsole(t3+"="+cadenaStringVerdadero+"; // Inicio cadena\n");
                    Utils.printConsole("goto "+L3+"; // Salida\n");
                    Utils.printConsole(L2+":\n");
                    String cadenaStringFalso =  Utils.generarCadena3D("0\n");
                    Utils.printConsole(t3+"="+cadenaStringFalso+"; // Inicio cadena\n");
                    Utils.printConsole(L3+":\n");
                    String inicioCadena = cadenaStringVerdadero;
                    t0 = Utils.generateTemp();
                    String t1 = Utils.generateTemp();
                    Utils.printConsole(t0+"=P+"+ts.size()+";//Simulacion de cambio de entorno\n");
                    Utils.printConsole(t1+"="+t0+"+1;// Direccion parametro 1\n");
                    Utils.printConsole("stack[(int)"+t1+"]="+t3+";//Paso parametro\n");
                    Utils.printConsole("P=P+"+ts.size()+"; // Cambio de entorno\n");
                    Utils.printConsole("__imprimir__();\n");
                    Utils.printConsole("P=P-"+ts.size()+"; // Retomar entorno\n");
                }
            }
            else if (tipoExpresion == Tipo.CHARACTER)
            {
                Utils.printConsole("printf(\"%c\", (int) " + valorExpresion + "); // Imprimir caracter\n");
                Utils.printConsole("printf(\"%c\", 10); // Imprimir salto\n");
            }
            else if (tipoExpresion == Tipo.VOID) // Manejo de cadenas.
            {
                String t0 = Utils.generateTemp();
                String t1 = Utils.generateTemp();
                Utils.printConsole(t0 + "=P+" + ts.size() + ";//Simulacion de cambio de entorno\n");
                Utils.printConsole(t1 + "=" + t0 + "+1;// Direccion parametro 1\n");
                Utils.printConsole("stack[(int)" + t1 + "]=" + valorExpresion + ";//Paso parametro\n");
                Utils.printConsole("P=P+" + ts.size() + "; // Cambio de entorno\n");
                Utils.printConsole("__imprimir__();\n");
                Utils.printConsole("P=P-" + ts.size() + "; // Retomar entorno\n");
            }
        }
        return null;
    }
}
