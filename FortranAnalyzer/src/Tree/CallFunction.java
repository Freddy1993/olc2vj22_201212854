package Tree;

import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class CallFunction implements NodeInst {
    /**
     * Identificador de la función que se llama
     */
    private final String identificador;
    /**
     * Lista de parámetros de la función que se llama
     */
    private final LinkedList<NodeInst> parametros;

    public int line = 0;
    public int column = 0;
    private Tipo tipo = Tipo.ENTERO;
    /**
     * Constructor de la clase llamada a función
     * @param a Identificador de la función que se está llamando
     * @param b Lista de parámetros que se le están enviando a la función que se llama
     */
    public CallFunction(String a, LinkedList<NodeInst> b, int line, int column) {
        identificador=a;
        parametros=b;
        this.line = line;
        this.column = column;
    }
    /**
     * Método que ejecuta la accion de llamar a una función, es una sobreescritura del
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts Tabla de símbolos del ámbito padre de la sentencia.
     * @return Esta instrucción retorna nulo porque no produce ningun valor al ser
     * ejecutada.
     */
    @Override
    public Object execute(TableSymbol ts,Arbol ar) {

        // para llamar a la función es necesario construir su identificador único
        String id = "_" + identificador + "(";
        for(NodeInst parametro: parametros) {
            // es necesario evaluar los parametros de la función para saber sus tipos
            // y así poder completar el id
            Object resultado = parametro.execute(ts, ar);

            if (resultado instanceof Integer) {
                id += "_" + Symbol.Tipo.ENTERO;
            } else if (resultado instanceof Double) {
                id += "_" + Symbol.Tipo.REAL;
            } else if(resultado instanceof String) {
                id += "_" + Symbol.Tipo.CHARACTER;
            } else if(resultado instanceof Boolean){
                id += "_" + Symbol.Tipo.LOGICAL;
            } else if(resultado instanceof ArrayNode) {
                Primitives obj = (Primitives) parametro;
                id += "_" + obj.tipo;
            }
        }
        id += ")";

        Function f=ar.getFunction(id.toLowerCase());
        if(null!=f){
            f.setValoresParametros(parametros);
            Object rFuncion=f.execute(ts, ar); //Objeto que almacena el resultado de la ejecución del proceso
            if(rFuncion instanceof Excepcion) {
                return rFuncion;
            }

            if(rFuncion instanceof Nulo){
                return rFuncion;
            }else if(f.getTipo()==Symbol.Tipo.VOID && !(rFuncion instanceof Return || rFuncion == null)){
                System.err.println("Una función de tipo Void no puede retornar valores, solamente puede retornar vacío.");
                Excepcion error = new Excepcion("Una función de tipo Void no puede retornar valores, solamente puede retornar vacío.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            } else if (f.getTipo() != Symbol.Tipo.VOID && rFuncion instanceof Return) {
                System.err.println("La funcion necesita un valor de retorno");
                Excepcion error = new Excepcion("La funcion necesita un valor de retorno", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            } else if (f.getTipo() != Symbol.Tipo.VOID && rFuncion == null) {
                System.err.println("Hace falta una sentencia de retorno en la función");
                Excepcion error = new Excepcion("Hace falta una sentencia de retorno en la función", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }else if(f.getTipo()==Symbol.Tipo.ENTERO && !(rFuncion instanceof Integer)){
                System.err.println("Una función de tipo Number no puede retornar un valor que no sea numérico.");
                Excepcion error = new Excepcion("Una función de tipo Number no puede retornar un valor que no sea numérico.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }else if(f.getTipo()==Symbol.Tipo.REAL && !(rFuncion instanceof Double)){
                System.err.println("Una función de tipo Number no puede retornar un valor que no sea numérico.");
                Excepcion error = new Excepcion("Una función de tipo Number no puede retornar un valor que no sea numérico.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }else if(f.getTipo()==Symbol.Tipo.LOGICAL && !(rFuncion instanceof Boolean)){
                System.err.println("Una función de tipo Boolean no puede retornar un valor que no sea verdadero o falso.");
                Excepcion error = new Excepcion("Una función de tipo Boolean no puede retornar un valor que no sea verdadero o falso.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }else if(f.getTipo()==Symbol.Tipo.CHARACTER && !(rFuncion instanceof String)){
                System.err.println("Una función de tipo Cadena no puede retornar un valor que no sea una cadena de caracteres.");
                Excepcion error = new Excepcion("Una función de tipo Cadena no puede retornar un valor que no sea una cadena de caracteres.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }

            if(rFuncion instanceof Return){
                return null;
            }else{
                return rFuncion;
            }
        } else {
            System.err.println("La función " + identificador + " no existe.");
            Excepcion error = new Excepcion("La función " + identificador + " no existe.", line, column);
            ar.excepciones.add(error);
            ar.console.add(error.description);
            return error;
        }
    }
    @Override
    public Tipo getTipoInstrution() {
        return this.tipo;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        try {
            TableSymbol t=new TableSymbol();
            t.addAll(ts);

            Arbol a = ar;

            String id = "_" + identificador + "(";
            for(NodeInst parametro: parametros) {

                Object resultado = parametro.execute(ts, ar);

                if (resultado instanceof Integer) {
                    id += "_" + Symbol.Tipo.ENTERO;
                } else if (resultado instanceof Double) {
                    id += "_" + Symbol.Tipo.REAL;
                } else if(resultado instanceof String) {
                    id += "_" + Symbol.Tipo.CHARACTER;
                } else if(resultado instanceof Boolean){
                    id += "_" + Symbol.Tipo.LOGICAL;
                } else if(resultado instanceof ArrayNode) {
                    Primitives obj = (Primitives) parametro;
                    id += "_" + obj.tipo;
                } else {
                    id += "_" + tipo;
                }
            }
            id += ")";


            String id3d = "_" + identificador ;
            for(NodeInst parametro: parametros) {
                id3d += "_";
            }


            Function f=a.getFunction(id.toLowerCase());
            this.tipo = f.getTipo();
            if(null!=f) {

                String t0 = Utils.generateTemp();
                Utils.printConsole(t0+"=P+"+(ts.size()+1)+";//Simulacion de cambio de entorno\n");


                for (int indice = 0; indice < this.parametros.size(); indice++) {
                    Object parametro = f.getParams().get(indice).execute(t,a);
                    NodeInst expresionActual = this.parametros.get(indice);
                    Object valorInicial = expresionActual.code3D(t,a);

                    TableSymbol tt=new TableSymbol();
                    if (parametro == null) {
                        parametro = f.getParams().get(indice).execute(tt,a);
                    }

                    String t1 = Utils.generateTemp();
                    Utils.printConsole(t1+'='+t0+'+'+(indice+1)+"; //Posicion parametros "+parametro.toString()+"\n");
                    Utils.printConsole("stack[(int)"+t1+"]="+valorInicial+";\n");
                }
                Utils.printConsole("P=P+"+(ts.size()+1)+";//Cambio de entorno\n");
                Utils.printConsole(id3d+"();\n");
                Utils.printConsole("P=P-"+(ts.size()+1)+";//Retomar de entorno\n");
                String t2 = Utils.generateTemp();
                Utils.printConsole(t2+"=stack[(int)"+t0+"];//Valor de retorno\n");
                return t2;
            }

            return null;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}
