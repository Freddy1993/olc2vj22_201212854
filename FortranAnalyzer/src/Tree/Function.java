package Tree;

import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class Function implements NodeInst {
    /**
     * Tipo de la función.
     */
    private Symbol.Tipo tipo;
    /**
     * Identificador de la función.
     */
    private final String identificador;
    /**
     * Parámetros de la función.
     */
    private final LinkedList<Declaracion> parametros;

    private final LinkedList<NodeInst> params;
    /**
     * Valores que se reciben como parámetros para ejecutar la función.
     */
    private LinkedList<NodeInst> valoresParametros;
    /**
     * Lista de las instrucciones que deben ejecutarse si se llama a la función.
     */
    private final LinkedList<NodeInst> instrucciones;

    private NodeInst varResult;

    public int line = 0;
    public int column = 0;
    public Function(String a, String b, LinkedList<Declaracion> c, LinkedList<NodeInst> d, int line, int column) {
        switch (a.toLowerCase()) {
            case "number": tipo=Symbol.Tipo.ENTERO;
                break;
            case "string":  tipo=Symbol.Tipo.CHARACTER;
                break;
            case "boolean": tipo=Symbol.Tipo.LOGICAL;
                break;
            case "void": tipo=Symbol.Tipo.VOID;
                break;
            default:
                tipo=null;
        }
        identificador=b;
        parametros=c;
        instrucciones=d;
        params=null;
        varResult = null;
        this.line = line;
        this.column = column;
    }
    /**
     * Constructor de la clase Function cuando esta no recibe parámetros.
     * @param a Tipo de la función
     * @param b Identificador de la función
     * @param c Lista de instrucciones contenidas por la función
     */
    public Function(String a, String b, LinkedList<NodeInst> c) {
        String reservadaTipo=a.toLowerCase();
        switch (reservadaTipo) {
            case "number": tipo=Symbol.Tipo.ENTERO;
                break;
            case "string":  tipo=Symbol.Tipo.CHARACTER;
                break;
            case "boolean": tipo=Symbol.Tipo.LOGICAL;
                break;
            case "void": tipo=Symbol.Tipo.VOID;
                break;
            default:
                tipo=null;
        }
        identificador=b;
        parametros=new LinkedList<>();
        instrucciones=c;
        params=null;
        varResult=null;
    }

    public Function(String a, String b, LinkedList<NodeInst> c, int line, int column) {
        String reservadaTipo=a.toLowerCase();
        switch (reservadaTipo) {
            case "number": tipo=Symbol.Tipo.ENTERO;
                break;
            case "string":  tipo=Symbol.Tipo.CHARACTER;
                break;
            case "boolean": tipo=Symbol.Tipo.LOGICAL;
                break;
            case "void": tipo=Symbol.Tipo.VOID;
                break;
            default:
                tipo=null;
        }
        identificador=b;
        parametros=new LinkedList<>();
        instrucciones=c;
        params=null;
        varResult=null;
        this.line = line;
        this.column = column;
    }

    public Function(String b, LinkedList<NodeInst> p, LinkedList<NodeInst> c, NodeInst result, int line, int column) {
        tipo = Tipo.VOID;
        identificador=b;
        params = p;
        parametros=new LinkedList<>();
        instrucciones=c;
        varResult=result;
        this.line = line;
        this.column = column;
    }
    /**
     * Método que ejecuta la una función, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia.
     * @return Estra instrucción retorna el valor de retorno de la función o bien
     * una instancia de retorno para el caso de las funciones void.
     */
    @Override
    public Object execute(TableSymbol ts,Arbol ar) {

        Utils.setAmbit(this.identificador);

        TableSymbol tablaLocal = new TableSymbol(); // Creamos una nueva tabla local para la función.
        tablaLocal.addAll(ar.tablaDeSimbolosGlobal); // Agregamos a la tabla local las referencias a las variables globales.
        if (params.size() == valoresParametros.size()) {
            int contador = 0;
            for (NodeInst ins : instrucciones) {
                if (ins instanceof ParamsFunction) {
                    ins.execute(tablaLocal, ar);
                    ParamsFunction params = (ParamsFunction) ins;
                    params.setParametro(true);

                    for (NodeInst instruccion : params.variables) {
                        Identifier iden = (Identifier) instruccion;
                        if (contador <= valoresParametros.size() - 1) {
                            Object valor = valoresParametros.get(contador).execute(tablaLocal, ar);
                            Object obj = tablaLocal.setValor(iden.identifier.toString(), valor);
                            if (obj instanceof Excepcion) {
                                return obj;
                            }
                        }
                    }
                    contador += 1;
                }
            }

            ar.tablaDeSimbolosGlobal = tablaLocal;

            for (NodeInst ins : instrucciones) {
                if (!((ins instanceof ParamsFunction) || (ins instanceof Return))) {
                    Object r;
                    r = ins.execute(tablaLocal, ar);
                    if (r instanceof Break) {
                        System.err.println("La sentencia break no se encuentra dentro de un ciclo");
                        Excepcion error = new Excepcion("La sentencia break no se encuentra dentro de un ciclo", line, column);
                        ar.excepciones.add(error);
                        ar.console.add(error.description);
                        return error;
                    } else if (r instanceof Excepcion) {
                        return r;
                    }
                }
            }

            if (varResult != null) {
                Return result = (Return) varResult;
                Identifier iden = (Identifier) result.valorRetorno;
                if (!tablaLocal.existeVariable(iden.identifier)) {
                    System.err.println("La variable " + iden.identifier + " no esta definida");
                    Excepcion error = new Excepcion("La variable " + iden.identifier + " no esta definida", line, column);
                    ar.excepciones.add(error);
                    ar.console.add(error.description);
                    return error;
                } else {
                    Object r;
                    r = this.varResult.execute(tablaLocal, ar);
                    if (r instanceof Break) {
                        System.err.println("La sentencia break no se encuentra dentro de un ciclo");
                        Excepcion error = new Excepcion("La sentencia break no se encuentra dentro de un ciclo", line, column);
                        ar.excepciones.add(error);
                        ar.console.add(error.description);
                        return error;
                    } else if (r instanceof Excepcion) {
                        return r;
                    }

                    if (r instanceof Double) {
                        this.tipo = Tipo.REAL;
                    } else if (r instanceof Integer) {
                        this.tipo = Tipo.ENTERO;
                    } else if (r instanceof String) {
                        this.tipo = Tipo.CHARACTER;
                    } else if (r instanceof Boolean) {
                        this.tipo = Tipo.LOGICAL;
                    }
                    return r;
                }
            }
        } else {
            System.err.println("La cantidad de parámetros enviada a la función " + identificador + " no es correcta.");
            Excepcion error = new Excepcion("La cantidad de parámetros enviada a la función " + identificador + " no es correcta.", line, column);
            ar.excepciones.add(error);
            ar.console.add(error.description);
            return error;
        }

        return null;
    }
    /**
     * Méotodo que devuelve el identificador de la función
     * @return Identificador de la función
     */
    public String getIdentificador() {

        // se crea un identificador único de las funciones con base en su Id 
        // más el tipo de sus parametros de la forma _id(_tipo..._tipo)
        int contador = 0;
        String id = "_" + identificador + "(";
        if(params.size() != 0) {
            for(NodeInst ins:instrucciones){
                if(contador <= params.size()-1) {
                    if(ins instanceof ParamsFunction) {
                        ParamsFunction params = (ParamsFunction) ins;
                        if(params.tipoEntrada == Tipo.IN) {
                            id += "_" + params.tipo.name();
                        }
                    }
                }
                contador += 1;
            }
        }
        id += ")";

        return id.toLowerCase();
    }
    /**
     * Método que configura el set de parámetros que la función debe recibir
     * @param a Lista de parámetros que se desea asignar a la función.
     */
    public void setValoresParametros(LinkedList<NodeInst> a) {
        valoresParametros=a;
    }
    /**
     * Método que devuelve el tipo de la función
     * @return Tipo de la función
     */
    public LinkedList<Declaracion> getParametros () {
        return this.parametros;
    }

    public LinkedList<NodeInst> getParams () {
        return this.params;
    }
    public Symbol.Tipo getTipo() {
        return tipo;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {

        TableSymbol t=new TableSymbol();
        Arbol a =ar;
        if(params.size()==valoresParametros.size()) {
            int contador = 0;
            for (NodeInst ins : instrucciones) {
                if (ins instanceof ParamsFunction) {
                    ((ParamsFunction) ins).setIs3d(true);
                    ins.execute(t, a);

                    ParamsFunction params = (ParamsFunction) ins;
                    params.setParametro(true);

                    contador += 1;
                }
            }
        }


        String id = "_" + identificador + "_";
        for(NodeInst parametro: parametros) {
            Object resultado = parametro.execute(ts, ar);

            if (resultado instanceof Integer) {
                id += "_" + Symbol.Tipo.ENTERO;
            } else if (resultado instanceof Double) {
                id += "_" + Symbol.Tipo.REAL;
            } else if(resultado instanceof String) {
                id += "_" + Symbol.Tipo.CHARACTER;
            } else if(resultado instanceof Boolean){
                id += "_" + Symbol.Tipo.LOGICAL;
            } else if(resultado instanceof ArrayNode) {
                Primitives obj = (Primitives) parametro;
                id += "_" + obj.tipo;
            }
        }
        id += "_()";

        if (this.params.size() == 0) {
            id = "_" + identificador + "()";
        }

        String etiquetaSalida = Utils.generarEtiqueta();
        Utils.printConsole("\n\nvoid "+id+" {\n");

        for (NodeInst ins: instrucciones) {
            ins.code3D(t,a);
        }

        if (this.tipo != Tipo.VOID) {
            ((Return)varResult).setEtiquetaSalida(etiquetaSalida);
            varResult.code3D(t,ar);
            Utils.printConsole(etiquetaSalida + "://Salida\n");
        }
        Utils.printConsole("return;\n");
        Utils.printConsole("}//Fin \n");

        return null;
    }
}
