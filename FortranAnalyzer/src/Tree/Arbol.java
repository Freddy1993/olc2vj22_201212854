
package Tree;

import Tree.Symbol.*;
import Utils.Utils;
import java.util.LinkedList;

public class Arbol implements NodeInst {
    private final LinkedList<NodeInst> Instructionses;
    public LinkedList<Excepcion> excepciones;
    public LinkedList<Object> console;

    public int line = 0;
    public int column = 0;

    public TableSymbol tablaDeSimbolosGlobal;
    public Arbol(LinkedList<NodeInst> Instructionses) {
        this.Instructionses = Instructionses;
        this.excepciones = new LinkedList<>();
        this.console = new LinkedList<>();
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {

        tablaDeSimbolosGlobal = ts;
        Utils.setAmbit("main");
        for(NodeInst ins:Instructionses){
            if(ins instanceof Function){
                Function f=(Function)ins;
                String id=f.getIdentificador();
                // el identificador único de la función main es _main() ya que no recibe parámetros

                if("_main()".equals(id)){
                    f.setValoresParametros(new LinkedList<>());
                    f.execute(ts, ar);
                    break;
                }
            } else {
                if (ins != null) {
                    ins.execute(ts, ar);
                }
            }
        }
        return null;
    }

    public Function getFunction(String identificador){
        for(NodeInst ins:Instructionses){
            if(ins instanceof Function){

                Function f=(Function)ins;
                String id=f.getIdentificador();

                if(identificador.equals(id)){
                    return f;
                }
            }
        }
        return null;
    }


    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        try {
            for(NodeInst ins:Instructionses)
            {
                if(ins instanceof Function)
                {
                    Function f=(Function)ins;
                    String id=f.getIdentificador();
                    if(!"_main()".equals(id))
                    {
                        //Pasar parametros
                        f.code3D(ts, ar);
                    }
                }

            }
            Utils.printConsole("void main(){\n");

            for(NodeInst ins:Instructionses)
            {
                if(ins instanceof Function)
                {
                    Function f=(Function)ins;
                    String id=f.getIdentificador();
                    if("_main()".equals(id))
                    {

                        //Pasar parametros
                        f.code3D(ts, ar);
                        break;
                    }
                }
                else
                {
                    ins.code3D(ts, ar);
                }
            }
            Utils.printConsole("return;\n}//Fin main\n ");
            Utils.setConsola();
            return null;
        }catch (Exception e) {
            return null;

        }
    }
}
