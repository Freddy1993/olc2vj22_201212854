
package Tree;

import java.util.LinkedList;
import Tree.Symbol.*;
import Utils.Utils;

public class DeclarationArray extends Declaracion implements NodeInst {

    private final LinkedList<NodeInst> dimensiones;
    public int line = 0;
    public int column = 0;

    public DeclarationArray(String a, String b, LinkedList<NodeInst> c, int line, int column) {
        super(a,b,null, line, column);
        dimensiones=c;
        this.line = line;
        this.column = column;
    }

    public DeclarationArray(String a, Tipo b, LinkedList<NodeInst> c, int line, int column) {
        super(a,b,null, line, column);
        dimensiones=c;
        this.line = line;
        this.column = column;
    }

    @Override
    public Object execute(TableSymbol ts,Arbol ar) {
        LinkedList<Integer> tamaniosDimensiones=new LinkedList<>();
        for (NodeInst dim : dimensiones) {
            Object er=dim.execute(ts, ar);
            if(er instanceof Excepcion) {
                return er;
            }
            //Se comprueba que cada indice para declarar un arreglo sea de tipo numerico
            if(!((er instanceof Double) || (er instanceof Integer))){
                System.err.println("Los indices para declarar un arreglo deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.");
                Excepcion error = new Excepcion("Los indices para declarar un arreglo deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.", line, column);
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return null;
            }
            if (er instanceof Integer) {
                tamaniosDimensiones.add(((Integer)er).intValue());
            } else if (er instanceof Double) {
                tamaniosDimensiones.add(((Double)er).intValue());
            }

        }
        ts.add(new Symbol(id,tipo,tamaniosDimensiones));
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {

        Symbol nuevoArreglo = new Symbol(this.id,this.tipo);
        nuevoArreglo.setRol(Symbol.Rol.valueOf("ARRAY"));
        ts.setValor(this.id, nuevoArreglo);

        String t0 = Utils.generateTemp();
        String t1 = Utils.generateTemp();
        Utils.printConsole(t0+"=P+"+nuevoArreglo.getIndice()+";//Direccion arreglo "+nuevoArreglo.getId()+"\n");
        Utils.printConsole("stack[(int)"+t0+"]=H; // Inicio del nuevo arreglo \n");
        Utils.printConsole(t1+"=H;//Inicio arreglo " +nuevoArreglo.getId() + "\n");
        Utils.printConsole("H=H+"+this.dimensiones.size()+"; // Reservando espacio en el heap para el arreglo\n");

        for (int index = 0; index < this.dimensiones.size(); index++) {
            this.codigoElemento3D(ts, ar, this.tipo, this.dimensiones.get(index), index, t1);
        }

        return null;
    }

    public Object codigoElemento3D (TableSymbol ts, Arbol ar, Tipo tipo, NodeInst expressionActual, int indice, String inicio) {

        if (expressionActual instanceof LinkedList) {
            for (int index = 0; index < this.dimensiones.size(); index++) {
                NodeInst expresion = (NodeInst) ((LinkedList<?>) expressionActual).get(index);
                //Tipo tipoExpresion = expresion.getTipoInstrution();

                String t0 = Utils.generateTemp();
                Utils.printConsole(t0+"="+inicio+"+"+indice+"; // Dirección elemento "+indice+"\n");
                Object valorExpresion = expresion.code3D(ts, ar);
                Utils.printConsole("heap[(int)"+t0+"]="+valorExpresion.toString()+";\n");
            }
        } else {
            String t0 = Utils.generateTemp();
            Utils.printConsole(t0+"="+inicio+"+"+indice+"; // Dirección elemento "+indice+"\n");
            Object valorActual = expressionActual.code3D(ts,ar);
            Utils.printConsole("heap[(int)"+t0+"]="+valorActual+";\n");
        }

        return null;
    }
}
