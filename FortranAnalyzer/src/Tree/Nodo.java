package Tree;

import Tree.Symbol.Tipo;

import java.util.LinkedList;

public class Nodo {

    private final LinkedList<Nodo> celdasVecinas;
    private Object valor;
    private Tipo tipo;

    public Nodo() {
        this.celdasVecinas = new LinkedList<>();
        this.valor = new Nulo(); //Inicializando cada celda con objeto Nulo
    }

    public void inicializarNodo(int cantDimensiones, int dimensionActual, LinkedList<Integer> tamaniosDimensiones){
        if(dimensionActual>cantDimensiones){
            return;
        }
        for (int i = 0; i < tamaniosDimensiones.get(dimensionActual-1) ; i++) {
            Nodo arr=new Nodo();
            arr.setTipo(tipo);
            celdasVecinas.add(arr);
            arr.inicializarNodo(cantDimensiones, dimensionActual+1, tamaniosDimensiones);            
        }
    }

    public void setValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, Object val, String id){
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual>=0){
            Nodo arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                // Validar que el valor a insertar sea del mismo tipo del arreglo
                if(val instanceof Double && tipo != Tipo.ENTERO
                        || val instanceof Boolean && tipo != Tipo.LOGICAL
                        || val instanceof String && tipo != Tipo.CHARACTER){
                    System.err.println("Esta intentando insertar un valor de tipo "+val.getClass().getSimpleName()+" al arreglo de tipo "+tipo);
                    return;
                }
                arr.valor=val;
            }else{
                arr.setValor(cantIndices, indiceActual+1, indices, val, id);
            }
        }else{
            System.err.println("La asignación al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.");
        }
    }

    Object getValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, String id) {
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual>=0){
            Nodo arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                Object val = arr.valor;
                return val == null ? new Nulo() : val;
            }else{
                return arr.getValor(cantIndices, indiceActual+1, indices, id);
            }
        }else{
            System.err.println("El acceso al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.");
        }
        return null;
    }
    public Tipo getTipo() {
        return tipo;
    }
    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    public LinkedList<Nodo> getCeldasVecinas() {
        return celdasVecinas;
    }
}
