package Tree;

import Tree.Symbol.*;
import Utils.Utils;

public class Identifier implements NodeInst {
    public String identifier;
    public Tipo tipo;
    public int line = 0;
    public int column = 0;
    public Identifier(String identifier, int line, int column) {
        this.identifier = identifier;
        this.line = line;
        this.column = column;
    }
    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        if (ts.existeVariable(this.identifier)) {
            Object obj = ts.getValor(this.identifier);
            if(obj instanceof Symbol) {
                Symbol variable = (Symbol) obj;
                this.tipo = variable.getTipo();
                return variable.getValor();
            } else if(obj instanceof Excepcion){
                Excepcion error = (Excepcion) obj;
                error.line = this.line;
                error.column = this.column;
                ar.excepciones.add(error);
                ar.console.add(error.description);
                return error;
            }
        } else {
            return this.identifier;
        }

        return null;
    }


    @Override
    public Tipo getTipoInstrution() { return this.tipo; }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        if (ts.existeVariable(this.identifier))
        {
            Symbol variable = (Symbol) ts.getValor(this.identifier);
            this.tipo = variable.getTipo();
            if(variable != null)
            {
                String t0 = Utils.generateTemp();
                String t1 = Utils.generateTemp();

                Utils.printConsole(t0+"=P+"+variable.getIndice()+";//Direccion variable "+this.identifier+"\n");
                Utils.printConsole(t1+"=stack[(int)"+t0+"];\n");
                return t1;
            }
        }
        else
        {
            return null;
        }
        return null;
    }
}
