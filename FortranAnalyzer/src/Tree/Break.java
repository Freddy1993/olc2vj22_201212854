
package Tree;

import Tree.Symbol.*;

public class Break implements NodeInst {

    public int line = 0;
    public int column = 0;

    @Override
    public Object execute(TableSymbol ts, Arbol ar) {
        return this;
    }

    @Override
    public Tipo getTipoInstrution() {
        return null;
    }

    @Override
    public int getLine() {
        return this.line;
    }

    @Override
    public int getColumn() {
        return this.column;
    }

    @Override
    public Object code3D(TableSymbol ts, Arbol ar) {
        return null;
    }
}
