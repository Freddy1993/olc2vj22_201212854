package Utils;

import java.util.LinkedList;

public class Utils
{
    public static int contadorEtiquetas =0;
    public static int contadorTemporales=0;
    public static LinkedList<String> consolaSalida3D = new LinkedList<String>();

    public static LinkedList<String> consola = new LinkedList<String>();
    public static int caracterFinDeCadena = -6298239 ;

    public static String ambito = "";

    public static void initParamas3D()
    {
        consolaSalida3D = new LinkedList<String>();
        consola = new LinkedList();
        contadorTemporales = 0;
        contadorEtiquetas = 0 ;
        ambito = "";
    }

    public static void setAmbit(String a) {
        ambito = a;
    }


    public static String getAmbit() {

        return  ambito;
    }

    public static String generateTemp()
    {
        return "t"+(contadorTemporales++);
    }

    public static String generarEtiqueta()
    {
        return "L"+(++contadorEtiquetas);
    }

    public static void printConsole(Object cadena)
    {
        try
        {
            consolaSalida3D.add(cadena.toString());
        }catch (Exception e)
        {
            System.out.println("Ha ocurrido un error: " + e.getMessage());
        }
    }
    public static String generarCadena3D(String cadena)
    {
        String t0 = generateTemp();
        printConsole(t0+"=H;// Inicio de la nueva cadena\n");

        char[] array = cadena.toCharArray();
        for(char caracter:array)
        {
            printConsole("heap[(int)H]="+ (int)caracter +"; // " + caracter+"\n"); // Caracter actual
            printConsole("H=H+1; // Reservando espacio en stack\n");
        }
        //Agregar caracter de final de cadena
        printConsole("heap[(int)H]="+ obtenerFinCadena() +"; // Fin de cadena. \n"); // Caracter a
        printConsole("H=H+1; // Reservando espacio \n");
        return t0;
    }

    public static String obtenerFinCadena()
    {
        return String.valueOf(caracterFinDeCadena);
    }

    public static String generarProgramaObjeto()
    {
        String codigoFuncionesNativas = Utils.generarNativas();
        String cadenaSalida = "";

        cadenaSalida += "#include <stdio.h> //Importar para el uso de Printf\n";
        cadenaSalida += "#include <math.h> // Libreria matematica\n";
        cadenaSalida += "float heap[100000];\n";
        cadenaSalida += "float stack[100000];\n";
        cadenaSalida += "float H = 0.00;\n";
        cadenaSalida += "float P = 0.00;\n";
        cadenaSalida += "float t0";
        for(int i = 1; i < Utils.contadorTemporales ; i++)
        {
            cadenaSalida += ", t"+ i;
        }
        cadenaSalida += ";// temporales\n\n";
        cadenaSalida += codigoFuncionesNativas;
        //cadenaSalida += "void main(){\n";
        for (String linea: consolaSalida3D)
        {
            cadenaSalida += linea;
        }
        //cadenaSalida += "return;\n}//Fin main\n ";
        return cadenaSalida;
    }

    // Funciones nativas 3D

    public static void generarCodigoImprimirCadena(String inicioCadena, int tamanioEntorno)
    {
        String t0 = generateTemp();
        String t1 = generateTemp();
        printConsole(t0+"=P+"+tamanioEntorno+";//Simulacion de cambio de entorno\n");
        printConsole(t1+"="+t0+"+1;// Direccion parametro 1\n");
        printConsole("stack[(int)"+t1+"]="+inicioCadena+";//Paso parametro\n");
        printConsole("P=P+"+tamanioEntorno+"; // Cambio de entorno\n");
        printConsole("Nativa_Impresion();\n");
        printConsole("P=P-"+tamanioEntorno+"; // Retomar entorno\n");
    }
    public static String generarNativas()
    {
        String codigoFunciones ="";
        codigoFunciones += NativaImpresion();
        codigoFunciones += NativaCompararCadenas();
        return codigoFunciones;
    }

    public static String NativaImpresion()
    {
        String codigoFuncion = "";
        String t0 = generateTemp();
        String t1 = generateTemp();
        String t2 = generateTemp();

        String L0 = generarEtiqueta();
        String L1 = generarEtiqueta();
        String L2 = generarEtiqueta();
        codigoFuncion +="void __imprimir__(){\n";
        codigoFuncion +=t0+"=P+1;// Direccion parametro recibido\n";
        codigoFuncion +=t1+"=stack[(int)"+t0+"];// Direccion de la cadena\n";
        codigoFuncion +=L0+":\n";
        codigoFuncion +=t2+"=heap[(int)"+t1+"]; // Caracter actual\n";
        codigoFuncion +="if ("+t2+"!="+obtenerFinCadena()+") goto "+L1+";\n";
        codigoFuncion +="goto "+L2+";// Fin impresion\n";
        codigoFuncion +=L1+":\n";
        codigoFuncion +="printf(\"%c\", (int)"+t2+"); // Imprimir caracter\n";
        codigoFuncion +=t1+"="+t1+"+1; // siguiente caracter\n";
        codigoFuncion +="goto "+L0+";\n";
        codigoFuncion +=L2+":\n";
        codigoFuncion +="return;\n";
        codigoFuncion +="}\n";
        return codigoFuncion;
    }


    public static String NativaCompararCadenas()
    {
        String t0 = generateTemp();
        String t1 = generateTemp();
        String t2 = generateTemp();
        String t3 = generateTemp();
        String t4 = generateTemp();
        String t5 = generateTemp();
        String t6 = generateTemp();

        String L0 = generarEtiqueta();
        String L1 = generarEtiqueta();
        String L2 = generarEtiqueta();
        String L3 = generarEtiqueta();
        String L4 = generarEtiqueta();
        String L5 = generarEtiqueta();
        String L6 = generarEtiqueta();
        String L7 = generarEtiqueta();

        String codigoFuncion = "";

        codigoFuncion +="\n\n";
        codigoFuncion +="void __compararCadenas__(){\n";
        codigoFuncion +=t0+"=P+1;// Direccion cadena \n";
        codigoFuncion +=t1+"=stack[(int)"+t0+"];// Direccion de la cadena 1\n";
        codigoFuncion +=t2+"=P+2;// Direccion cadena 2\n";
        codigoFuncion +=t3+"=stack[(int)"+t2+"];// Direccion de la cadena 2\n";
        codigoFuncion +=L0+":\n";
        codigoFuncion +=t4+"=heap[(int)"+t1+"];// caracter i-esimo cadena 1\n";
        codigoFuncion +="if("+t4+"=="+obtenerFinCadena()+") goto "+L1+"; //Fin de cadena 1;\n";
        codigoFuncion +="goto "+L2+";\n";
        codigoFuncion +=L2+":\n";
        codigoFuncion +=t5+"=heap[(int)"+t3+"];// caracter i-esimo cadena 2\n";
        codigoFuncion +="if("+t4+"=="+t5+") goto "+L3+"; //Verdadero, actualizar caracter;\n";
        codigoFuncion +="goto "+L6+";// Caracteres diferentes. Falso\n";
        codigoFuncion +=L3+":\n";
        codigoFuncion +=t1+"="+t1+"+1;// caracter cadena 1\n";
        codigoFuncion +=t3+"="+t3+"+1;// caracter cadena 2\n";
        codigoFuncion +="goto "+L0+"; // Compracion\n";
        codigoFuncion +=L1+":\n";
        codigoFuncion +=t5+"=heap[(int)"+t3+"];// caracter i-esimo cadena 2\n";
        codigoFuncion +="if("+t4+"=="+t5+") goto "+L5+"; //Verdadero, \n";
        codigoFuncion +="goto "+L6+";\n";
        codigoFuncion +=L6+":\n";
        codigoFuncion +=t6+"=P+0;//Direccion retorno\n";
        codigoFuncion +="stack[(int)"+t6+"]=0; // Valor falso de retorno\n";
        codigoFuncion +="goto "+L7+";\n";
        codigoFuncion +=L5+":\n";
        codigoFuncion +=t6+"=P+0;//Direccion retorno\n";
        codigoFuncion +="stack[(int)"+t6+"]=1; // Valor verdadero de retorno\n";
        codigoFuncion +=L7+":\n";
        codigoFuncion +="return;\n";
        codigoFuncion +="}\n";
        return codigoFuncion;
    }
    // Imprimir archivos
    public static void setConsola()
    {
        consola.add(generarProgramaObjeto());

    }


}
